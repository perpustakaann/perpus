const log = serverUtils.getLogger('app')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const cookieSer = require('./lib/CookieSer')
const express = require('express')
const favicon = require('serve-favicon')
const morgan = require('morgan')
const path = require('path')
const compression = require('compression')
const jwtDecode = require('jwt-decode')

const app = express()

// libraries
const authService = require('./lib/AuthService')

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')
app.set('x-powered-by', 'CaktiCore')

// resource middleware
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended: true
}))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use(express.static(path.join(__dirname, 'public'), {
  maxAge: 31557600
}))
app.use(compression())

// access logger
app.use(morgan('combined', {
  stream: serverUtils.morganAccessLogStream
}))

const appEnv = serverConfig.serviceEnv === 'prod' ? '' : serverConfig.serviceEnv

// process login and logout
// this should be put first so the request does not
// trapped into the 'catch-all' method below
app.get('/logout', function (req, res) {
  res.clearCookie(serverConfig.server.name + '_' + appEnv)
  res.redirect('/login?message=logout successful&isInfo=true')
})

app.get('/login', function (req, res) {
  if (req.cookies[serverConfig.server.name + '_' + appEnv]) {
    res.redirect('/')
  } else {
    res.render('login', {
      title: `${serverConfig.server.name.toUpperCase()} ${appEnv}`,
      authMessage: req.query.message,
      authMessageInfo: req.query.isInfo === 'true'
    })
  }
})

app.post('/login', function (req, res, next) {
  authService.authenticate(
    req.body.username,
    req.body.password,
    function (resp) {
      const decodedToken = jwtDecode(resp.access_token)

      res.cookie(
        serverConfig.server.name + '_' + appEnv,
        cookieSer.ser({
          token: resp.access_token,
          username: decodedToken.uid,
          fullname: decodedToken.sub,
          id: decodedToken.uid
        }), {
        httpOnly: true,
        secure: serverConfig.server.scheme === 'HTTPS'
      }
      )
      res.redirect('/')
    },
    function (err) {
      log.debug(err)
      res.clearCookie(serverConfig.server.name + '_' + appEnv)
      if (typeof err.error_description === 'undefined') {
        res.redirect('/login?message=Login failed, please try again&isInfo=false')
      }

      res.redirect('/login?message=' + err.error_description + '&isInfo=false')
    }
  )
})

// 'catch-all' method
app.use(function (req, res, next) {
  log.info(`[${req.method}] ${req.originalUrl}`)
  // bypass auth check for static contents
  if (req.path.startsWith('/stylesheets') || req.path.startsWith('/javascripts')) {
    return next()
  }

  // check cookie validity
  if (req.cookies[serverConfig.server.name + '_' + appEnv]) {
    const cookie = cookieSer.dser(req.cookies[serverConfig.server.name + '_' + appEnv])
    if (cookie.token) {
      res.locals.token = cookie.token
      res.locals.userId = cookie.username
      res.locals.user = cookie.fullname
      res.locals.version = serverConfig.version
      res.locals.appEnv = appEnv
      return next()
    } else {
      res.clearCookie(serverConfig.server.name + '_' + appEnv)
      res.redirect('/login?message=User session has expired. Please login again&isInfo=false')
    }
  } else {
    // this handles redirect from /logout
    // do not check cookie if it does not exist
    res.redirect('/login')
  }
})

// root document handler
app.get('/', function (req, res) {
  if (!res.locals.user) {
    res.redirect('/login')
  } else {
    res.render('index', {
      title: `${serverConfig.server.name.toUpperCase()} ${appEnv}`,
      user: res.locals.user,
      version: serverConfig.version,
      appEnv: appEnv,
      fullTitle: serverConfig.server.fullTitle
    })
  }
})

// routers initialization
app.use('/visitor', require('./routes/visitor'))
app.use('/valid_tenant', require('./routes/valid_tenant'))
app.use('/tenant', require('./routes/tenant'))
app.use('/object', require('./routes/object'))
app.use('/objectType', require('./routes/objectType'))
app.use('/map', require('./routes/map'))
app.use('/handler', require('./routes/handler/location'))
app.use('/handler', require('./routes/handler/building'))
app.use('/objectModel', require('./routes/objectModel'))
app.use('/asset', require('./routes/asset'))
app.use('/contactInfo', require('./routes/contactInfo'))
app.use('/ownership', require('./routes/ownership'))
app.use('/referenceObject', require('./routes/referenceObject'))
app.use('/location', require('./routes/location'))
app.use('/building', require('./routes/building'))
app.use('/floor', require('./routes/floor'))
app.use('/room', require('./routes/room'))
app.use('/space', require('./routes/space'))
app.use('/rack', require('./routes/rack'))
app.use('/shelf', require('./routes/shelf'))
app.use('/slot', require('./routes/slot'))
app.use('/powerEquipment', require('./routes/powerEquipment'))
app.use('/powerDistribution', require('./routes/powerDistribution'))
app.use('/powerUnit', require('./routes/powerUnit'))
app.use('/battery', require('./routes/battery'))
app.use('/device', require('./routes/device'))
app.use('/device', require('./routes/deviceModule'))
app.use('/device', require('./routes/port'))
app.use('/device', require('./routes/vlan'))
app.use('/manufacturer', require('./routes/manufacturer'))
app.use('/serviceType', require('./routes/serviceType'))
app.use('/physicaldevice', require('./routes/physicaldevice'))
app.use('/cablesheath', require('./routes/cablesheath'))
app.use('/info', require('./routes/info'))
app.use('/mapHandler', require('./routes/mapHandler'))
app.use('/layout2d', require('./routes/layout2d'))
app.use('/employee', require('./routes/employee'))
app.use('/valid_employee', require('./routes/valid_employee'))
app.use('/buku', require('./routes/buku'))
app.use('/rak', require('./routes/rak'))
app.use('/anggota', require('./routes/anggota'))
app.use('/peminjaman', require('./routes/peminjaman'))
app.use('/petugas', require('./routes/petugas'))
app.use('/pengembalian', require('./routes/pengembalian'))



// catch 404 and forward to error handler
app.use(function (req, res, next) {
  res.statusCode = 404
  next({
    status: 404,
    message: 'Page Not Found: ' + req.url
  })
})

// generic error handling and terminate all unhandled request
app.use(function (err, req, res, next) {
  // construct error object
  const error = err
  if (err.response) {
    error.status = res.statusCode = err.response.status
    error.message = err.response.statusText
    error.description = err.response.data.message ?
      err.response.data.message +
      ': ' +
      (err.response.data.description ? err.response.data.description : JSON.stringify(err.response.data.objects)) :
      err.response.data
  } else if (err.syscall) {
    error.stack = err.syscall + ' ' + err.code + ' ' + err.address + ':' + err.port
  }
  if (!err.status) {
    error.status = res.statusCode = 500
    error.message = error.message || 'Internal Server Error'
  }
  error.path = req.path
  error.method = req.method
  error.data = req.body
  error.isAjax = req.xhr

  // log to output/logfile
  log.error(error)

  // send json or render error page
  if (req.xhr) {
    res.status(err.status).send({
      status: err.status,
      message: err.message,
      description: err.description
    })
  } else {
    res.statusCode = err.status
    res.render('error', {
      user: res.locals.user,
      version: serverConfig.version,
      status: err.status,
      message: err.message + (err.description ? ': ' + err.description : '')
    })
  }

  next()
})

process.on('uncaughtException', function (err) {
  log.error(err)
})

module.exports = app