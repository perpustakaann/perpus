const router = require('express').Router()
const internalService = require('../lib/InternalService')

const dataStructure = [
  { name: 'tenantNum', label: 'Tenant Number', link: 'view', filter: { type: 'text' }, defaultSort: 'asc' },
  { name: 'name', label: 'Name', filter: { type: 'text' } },
  { name: 'floor', label: 'Floor', filter: { type: 'text' } },
  { name: 'height', label: 'Height', filter: { type: 'text' } },
  { name: 'width', label: 'Width', filter: { type: 'text' } },
  { name: 'length', label: 'Length', filter: { type: 'text' } },
  { name: 'employeeId', label: 'Employee Id', filter: { type: 'text' } },
  { name: 'status', label: 'Status', type: 'boolean' },
]

router.get('/data', function (req, res, next) {
  const queries = req.query
  const reqData = {
    limit: 0,
    label: 'Tenant',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {},
    optionalNodes: [
      {
        label: "validasi_visit",
        relationLabel: "HAS_PARENT",
        // directionSensitive: true,
        includeRelationship: true,
        collection: true
      },
    ]
  }

  if (queries.order) {
    queries.order.forEach(function (item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.name] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function (item) {
      if (item.search.value !== '' && item.name) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.name] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function (resData) {
      const data = []

      resData.nodes.forEach(function (item) {

        data.push({
          _id: item.node._id ? item.node._id : null,
          tenantNum: item.node.tenantNum ? item.node.tenantNum : null,
          name: item.node.name ? item.node.name : null,
          floor: item.node.floor ? item.node.floor : null,
          height: item.node.height ? item.node.height : null,
          width: item.node.width ? item.node.width : null,
          length: item.node.length ? item.node.length : null,
          employeeId: item.node.employeeId ? item.node.employeeId : null,
          status: item.node.status ? item.node.status : null,
          links: {
            view: { url: '/tenant/' + item.node._id, method: 'GET' },
          }
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }

      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function (err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }

      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})

router.get('/', function (req, res, next) {
  res.render('tenant/list', {
    title: `${serverConfig.server.name.toUpperCase()}`,
    user: res.locals.user,
    dataStructure: dataStructure,
    links: {
      data: { url: '/tenant/data', method: 'GET' }
    }
  })
})

router.post('/', function (req, res, next) {
  const reqData = {
    label: 'Tenant',
    properties: {
      tenantNum: req.body.tenantNum,
      name: req.body.name,
      floor: req.body.floor,
      height: req.body.height,
      width: req.body.width,
      length: req.body.length,
      employeeId: req.body.employeeId,
      status: req.body.status
    }
  }

  internalService.createNode(reqData, res, next, function (resData) {

    if (req.body.valID) {
      const relationData = {
        startNodeId: resData.node._id,
        endNodeId: req.body.valID,
        type: 'HAS_PARENT',
      }

      internalService.createRelationship(relationData, res, next, function () {
        res.status(200).send({
          links: {
            view: { url: '/tenant/' + resData.node._id, method: 'GET' }
          }
        })
      })
    } else {
      res.status(200).send({
        links: {
          view: { url: '/tenant/' + resData.node._id, method: 'GET' }
        }
      })
    }
  })
})

router.put('/:_id', function (req, res, next) {
  var _id = req.params._id
  var data = {
    label: 'Tenant',
    id: parseInt(_id),
    properties: req.body
  }

  internalService.updateNode(data, res, next, function () {
    res.status(200).send({
      links: {
        index: { url: '/tenant/' + _id, method: 'GET' }
      }
    })
  })
})

router.get('/:_id', function (req, res, next) {
  var id = req.params._id
  internalService.getNode(id, 'TRUE', res, next, function (resData) {

    const data = []
    var item = resData

    data.push({
      tenantNum: item.node.tenantNum ? item.node.tenantNum : null,
      name: item.node.name ? item.node.name : null,
      floor: item.node.floor ? item.node.floor : null,
      height: item.node.height ? item.node.height : null,
      width: item.node.width ? item.node.width : null,
      length: item.node.length ? item.node.length : null,
      employeeId: item.node.employeeId ? item.node.employeeId : null,
      status: item.node.status ? item.node.status : null,
      links: {
        delete: { url: '/tenant/' + item.node._id, method: 'DELETE' },
        edit: { url: '/tenant/' + item.node._id, method: 'PUT' }
      }
    })

    res.render('tenant/details', {
      title: `${serverConfig.server.name.toUpperCase()}`,
      user: res.locals.user,
      data: data[0]
    })
  }
  )
})

router.delete('/:_id', function (req, res, next) {
  const _id = req.params._id

  internalService.deleteNode(_id, res, next, function () {
    res.status(200).send({
      links: {
        index: {
          url: '/tenant/',
          method: 'GET'
        }
      }
    })
  })
})

module.exports = router
