const express = require('express');
const router = express.Router();
const Security = require('../lib/Security');
const auth = Security.auth;
const Backbone = require('../lib/BackboneService');
const sendRequest = Backbone.sendRequest;
const dataStructure = [
    // { name: "indicator", label : "Indicator", filter: { type: 'text' }},
    { name: "name", label: "Name", link: "view", filter: { type: 'text' } },
    { name: "specification", label: "Specification / Type"},
    { name: "networkRole", label: "Network Role", filter: { type: 'text' }, type:'network-role' },
    { name: "networkLocation", label: "Network Location", filter: { type: 'text' } },
    { name: "areacode", label: "Area Code", filter: { type: 'text' } },
    { name: "source", label: "Source", filter: { type: 'text' } },
    { name: "status", label: "Status", filter: { type: 'text' },type: 'status' }
]

router.get('/data', function (req, res, next) {
    const queries = req.query;
    var rowsx =  parseInt(queries.length);
    var pagex = (parseInt(queries.start) / rowsx) + 1;
    const requestData = {
        label: "PhysicalDevice",
        filter: [],
        rows: rowsx,
        page: pagex
    };

    if(queries.columns) {
        queries.columns.forEach(function(item, index){
            if (item.search.value !== '' && item.name) {
                var obj = {}; 
                obj['prop'] = item.name;
                obj['value'] = item.search.value;
                requestData.filter.push(obj);
            }
        });
    }

    sendRequest('admin', '/refferenceObjects/pageQuery', requestData,  'PUT', res, function (responseData) {
        const data = [];
        var totalRows = 0;

        if(responseData.length >0){
            responseData[0].nodes.forEach(function(item, index){
                var spec = item.specification ? item.specification : null;
                var type = item.type ? item.type : null;
                var netrole = item.networkRole ? item.networkRole : null;
                data.push({
                  indicator : item.indicator ? item.indicator : null,
                  id: item.id ? item.id : null,
                  name: item.name ? item.name : null,
                  specification: spec+" / "+type,
                  networkRole:netrole,
                  source: item.source ? item.source : null,
                  areacode: item.area_code ? item.area_code : null,
                  networkLocation: item.networkLocation ? item.networkLocation : null,
                  status: item.status ? item.status : null,
                  links: {
                    view: { url: "/physicaldevice/id/" + item.id, method: "GET" }
                  }
                });
            });
            totalRows = responseData[0].pager.totalRows;
        }
        const result = {
            draw: queries.draw,
            data: data,
            recordsFiltered: totalRows,
            recordsTotal: totalRows
        };

        res.send(result);
        
    }, function(code, message) {
        const result = {
            draw: queries.draw,
            data: [],
            recordsFiltered: 0,
            recordsTotal: 0,
        }

        if(code !== 404) {
            result.error = message;
        }

        res.status(200).send(result);
    });
});

router.get('/', function (req, res, next) {
    res.render('physicaldevice/list', {
        dataStructure: dataStructure,
        links: {
            data: { url: "/physicaldevice/data", method: "GET" }
        }
    });
});

router.post('/', function (req, res, next) {
    auth(res, function(resp){
        const requestData ={
            name: req.body.name,
            label: req.body.name,
            key: req.body.name,
            type: "FRAME",
            status: "INSERVICE",
            manufacturer: req.body.manufacturer,
            legacy_id: "",
            networkLocation: req.body.networkLocation,
            specification: req.body.specification,
            description: req.body.description,
            source: "Backbone-fe",
            createdBy: resp
        };
        sendRequest('admin', '/physicalDevice/create', requestData,  'POST', res, function (responseData) {
            res.status(200).send({
            links: {
                view: { url: "/physicaldevice/id/" + responseData.low, method: "GET" }
            }
            });
        });
    });
});

router.get('/:key/:value', function (req, res, next) {
    var val = req.params.value;
    var params = "id="+val;
    if(req.params.key=="name"){
        params = "name="+Buffer.from(val, 'base64').toString('ascii');
    }
    sendRequest('admin','/physicalDevice/find?'+ params, null, 'GET', res, function (responseData) {
        const data = {
            id: responseData[0].physical.id ? responseData[0].physical.id : null,
            name: responseData[0].physical.name ? responseData[0].physical.name : null,
            key: responseData[0].physical.key ? responseData[0].physical.key : null,
            label: responseData[0].physical.label ? responseData[0].physical.label : null,
            specification: responseData[0].physical.specification ? responseData[0].physical.specification : null,
            source: responseData[0].physical.source ? responseData[0].physical.source : null,
            type: responseData[0].physical.type ? responseData[0].physical.type : null,
            areacode: responseData[0].physical.areacode ? responseData[0].physical.areacode : null,
            networkLocation: responseData[0].physical.networkLocation ? responseData[0].physical.networkLocation : null,
            status: responseData[0].physical.status ? responseData[0].physical.status : null,
            legacy_id: responseData[0].physical.legacyId ? responseData[0].physical.legacyId : null,
            createdBy: responseData[0].physical.createdBy ? responseData[0].physical.createdBy : null,
            createdAt: responseData[0].physical.createdAt ? responseData[0].physical.createdAt : null,
            modifiedAt: responseData[0].physical.modifiedAt ? responseData[0].physical.modifiedAt : null,
            projectId: responseData[0].physical.projectId ? responseData[0].physical.projectId : null,
            contractId: responseData[0].physical.contractId ? responseData[0].physical.contractId : null,
            modifiedAt: responseData[0].physical.modifiedAt ? responseData[0].physical.modifiedAt : null,
            modifiedBy: responseData[0].physical.modifiedBy ? responseData[0].physical.modifiedBy : null,
            module:[],
            has_pipe:[],
            mapName: Buffer.from(responseData[0].physical.name).toString('base64')
        };

        if(responseData[0].modules[0].module.id){
            responseData[0].modules.forEach(function(item, index){
                data.module.push({
                    name: item.module.name?item.module.name: null,
                    networkRole: item.module.networkRole?item.module.networkRole: null,
                    status: item.module.status?item.module.status: null,
                    device: responseData[0].physical.name?responseData[0].physical.name: null,
                    links: {
                        view: {url: "/physicaldevice/" + val + "/module/" + Buffer.from(item.module.name).toString('base64'), method: "GET"}
                    }
                });
            });   
        }

        res.render('physicaldevice/details', {
            data: data,
            links: {
                view: { url: "/physicaldevice/id/" + data.id, method: "GET" },
                edit: { url: "/physicaldevice/" + data.id, method: "PUT" },
                delete: {url: "/physicaldevice/" + data.id + "/" + data.status, method: "DELETE"}
            }
        });
    });
});

router.get('/:id/module/:module', function (req, res, next) {
    const id = req.params.id;
    const module = req.params.module;
    var decodemodule = Buffer.from(module, 'base64').toString();
    sendRequest('admin','/physicalDevice/find?id='+ id, null, 'GET', res, function (responseData) {
        const data = {
            id:responseData[0].physical.id,
            name: responseData[0].physical.name,
            module: [],
            port:[],
        };
        if(responseData[0].modules){
            responseData[0].modules.forEach(function(item, index){
                if(item.module.name == decodemodule){
                    data.module.push({
                        name: item.module.name,
                        networkRole: item.module.networkRole,
                        status: item.module.status, 
                    });
                    
                    item.ports.forEach(function(item2, index){
                        if(item.module.networkRole == "SPLITTER"){
                            if(item2.connection[0]){
                                var UP = null;
                                var DOWN = null;
                                item2.connection.forEach(function(item3, index){
                                    if(item3.direction == "UP"){
                                        UP = item3.to ;
                                    }else if(new RegExp("(DOWN|JUMPER)").test(item3.direction)){
                                        DOWN = item3.to ;
                                    }
                                });
                                data.port.push({
                                    iUP: UP,
                                    iDown: DOWN,
                                    name: item2.name,
                                    key: item2.key, 
                                    status: item2.status,
                                })
                            }else{
                                data.port.push({
                                    name: item2.name,
                                    key: item2.key, 
                                    status: item2.status,
                                })
                            }
                        }else if(new RegExp("(FRAME_UNIT|RADIO|RADIO_ANTENNA|ANTENNA)").test(item.module.networkRole)){
                            if(item2.connection[0]){
                                var UP = null;
                                var DOWN = null;
                                item2.connection.forEach(function(item3, index){
                                    if(new RegExp("(UP|VIRTUAL|LINK|JUMPER)").test(item3.direction)){
                                        UP = item3.to ;
                                    }else if(item3.direction == "DOWN"){
                                        DOWN = item3.to ;
                                    }
                                });
                                data.port.push({
                                    iUP: UP,
                                    iDown: DOWN,
                                    name: item2.name,
                                    key: item2.key, 
                                    status: item2.status,
                                })
                            }else{
                                data.port.push({
                                    name: item2.name,
                                    key: item2.key, 
                                    status: item2.status,
                                })
                            }
                        }else{
                            data.port.push({
                                name: item2.name,
                                key: item2.key, 
                                status: item2.status,
                            })
                        }
                    });
                }
            });
        }
        
        res.render('physicaldevice/detailports', {
            data: data,
            links: {
                view: { url: "/physicaldevice/" + id + "/module/" + module, method: "GET" },
                edit: { url: "/physicaldevice/" + Buffer.from(data.name).toString('base64') + "/module/" + module + "/"+ data.id, method: "PUT" },
                delete: {url: "/physicaldevice/" + id + "/module/" + module, method: "DELETE"}
            }
        });
    });
});

router.put('/:name', function (req, res, next) {
    auth(res, function(resp){
        const name = req.params.name;
        const requestData   = [];
        var obj             = {};
        obj['id']           = name;
        obj['modifiedBy']   = resp;
        var keynames        = Object.keys(req.body);
        var x1              = req.body;
        var x2              = JSON.stringify(x1);
        var x3              = x2.split(":");
        var x4              = x3[1].split('"');
        obj[keynames]       = x4[1];
        requestData.push(obj);
        sendRequest('admin', '/physicaldevice/update', requestData[0],  'PUT', res, function (responseData) {
            res.status(200).send();
        });
    });
});

router.post('/module', function (req, res, next) {
    auth(res, function(resp){
        const requestData ={
            device: req.body.odp,
            module: req.body.module,
            templateModule: req.body.template,
            createdBy: resp
        };
        sendRequest('admin', '/physicalDevice/createModulePorts', requestData,  'POST', res, function (responseData) {
            console.log(responseData);
            res.status(200).send({
                links: {
                    view: { url: "/physicaldevice/id/" + req.body.phyid, method: "GET" }
                }
            });
        });
    });
});

router.put('/:name/module/:module/:id', function (req, res, next) {
    auth(res, function(resp){
        const name = req.params.name;
        var decode = Buffer.from(name, 'base64').toString();
        const module = req.params.module;
        var decodeModule = Buffer.from(module, 'base64').toString();
        const requestData   = [];
        var obj             = {};
        obj['module']       = decodeModule;
        obj['device']       = decode;
        obj['modifiedBy']   = resp;
        var keynames        = Object.keys(req.body);
        var x1              = req.body;
        var x2              = JSON.stringify(x1);
        var x3              = x2.split(":");
        var x4              = x3[1].split('"');
        obj[keynames]       = x4[1];
        requestData.push(obj);
        sendRequest('admin', '/physicalDevice/updateModulePorts', requestData[0],  'PUT', res, function (responseData) {
            res.status(200).send();
        });
    });
});

router.put('/', function (req, res, next) {
    auth(res, function(resp){
        const requestData ={
            portKey: req.body.id,
            status: req.body.status,
        };
        sendRequest('admin', '/physicalDevice/updatePort', requestData,  'PUT', res, function (responseData) {
            res.status(200).send();
        });
    });
});

router.put('/:device/:module', function (req, res, next) {
    auth(res, function(resp){
        const device = Buffer.from(req.params.device, 'base64').toString();
        const module = Buffer.from(req.params.module, 'base64').toString();

        const requestData = {
            device : device,
            module : module
        };
        sendRequest('admin', '/physicalDevice/deleteModulePorts' ,requestData, 'PUT', res, function (responseData){
            res.status(200).send();
        });
    });
});
router.delete('/:id/:status', function (req, res, next) {
    auth(res, function(resp){
        const id = req.params.id;

        const requestData ={
            id: id
        };
        if(req.params.status=="PENDINGDELETE"){
            sendRequest('admin', '/physicalDevice/delete', requestData,  'PUT', res, function (responseData) {
                res.status(200).send({
                    id: id,
                    links: {
                        index: { url: "/physicaldevice/", method: "GET" }
                    }
                });
            });
        }else{
            const result = {}
            result.error = "Status must PENDINGDELETE";
            res.status(400).send(result);
        }
    });
});
module.exports = router;