const router = require('express').Router()
const internalService = require('../lib/PrefmanService')

router.get('/:name', function(req, res, next) {
  internalService.referenceObject(
    req.params.name,
    res,
    next,
    function(resData) {
      var data = []

      if(resData.length > 0){
        data = resData
      }

      if (data) res.status(200).send({data:data})
      else res.send()
    }
  )
})

router.get('/prefman/boilerPlate', function(req, res, next) {
  console.log(req.query)
  internalService.boilerPlate(
    req.query,
    res,
    next,
    function(resData) {
      // console.log("Iyeu datana",resData)
      res.send(resData)
      // var data = []
      // if(resData.length > 0){
      //   data = resData
      // }
      // if (data) res.status(200).send({data:data})
      // else res.send()
    }
  )
})


module.exports = router