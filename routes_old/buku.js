// 1	id_buku
// 2	kode_buku	
// 3	judul_buku
// 4	penulis_buku	
// 5	penerbit_buku	
// 6	tahun_penerbit	
// 7	stok
const router = require('express').Router()
const internalService = require('../lib/InternalService')

const dataStructure = [
  {name: 'id_buku', label: 'Book ID', link: 'view', filter: {type: 'text'}, defaultSort: 'asc'},
  {name: 'book_code', label: 'Book Code', filter: {type: 'text'}},
  {name: 'judul', label: 'Judul', link: 'view', filter: {type: 'text'}},
  {name: 'penulis', label: 'Penulis', filter: {type: 'text'}},
  {name: 'penerbit', label: 'Penerbit', filter: {type: 'text'}},
  {name: 'terbit', label: 'Tahun Terbit', filter: {type: 'text'}},
  {name: 'status', label: 'Status', type: 'boolean'},
  {name: 'createdAt', label: 'Created At', type: 'date'},
  {name: 'createdBy', label: 'Created By', filter: {type: 'text'}}
]

router.get('/data', function(req, res, next) {
  const queries = req.query
  const reqData = {
    limit: 0,
    label: 'Buku',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {},
    optionalNodes: [
      {
        label: "Rak",
        relationLabel: "HAS_PARENT",
        // directionSensitive: true,
        includeRelationship: true,
        collection: true
      },
    ]
  }

  if (queries.order) {
    queries.order.forEach(function(item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.book_code] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function(item) {
      if (item.search.value !== '' && item.book_code) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.book_code] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function(resData) {
      const data = []

      resData.nodes.forEach(function(item) {

        data.push({
          _id: item.node._id ? item.node._id : null,
        //   objectName: item.objectOpt[0].name,
          id_buku: item.node.id_buku ? item.node.id_buku : null,
          book_code: item.node.book_code ? item.node.book_code : null,
          judul: item.node.judul ? item.node.judul : null,
          penulis: item.node.penulis ? item.node.penulis : null,
          penerbit: item.node.penerbit ? item.node.penerbit : null,
          terbit: item.node.terbit ? item.node.terbit : null,
          status: item.node.status ? item.node.status: null,
          createdAt: item.node.createdAt ? item.node.createdAt : null,
          createdBy: item.node.createdBy ? item.node.createdBy : null,
          updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
          updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
          links: {
            view: {url: '/buku/' + item.node._id, method: 'GET'},
            // viewObject: {url: '/object/' + item.objectOpt[0]._id, method: 'GET'},
          }
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }

      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function(err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }

      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})

router.get('/', function(req, res, next) {
  res.render('buku/list', {
    title: `${serverConfig.server.name.toUpperCase()}`,
    user: res.locals.user,
    dataStructure: dataStructure,
    links: {
      data: {url: '/buku/data', method: 'GET'}
    }
  })
})

router.post('/', function(req, res, next) {
  const reqData = {
    label: 'Buku',
    properties: {
      id_buku: req.body.id_buku,
      book_code: req.body.book_code,
      judul: req.body.judul,
      penulis: req.body.penulis,
      penerbit: req.body.penerbit,
      terbit: req.body.terbit,
      status: 'ACTIVE'
    }
  }

  internalService.createNode(reqData, res, next, function(resData) {

    if(req.body.rakId){
      const relationData = {
        startNodeId: resData.node._id,
        endNodeId: req.body.rakId,
        type: 'HAS_PARENT',
      }
  
      internalService.createRelationship(relationData, res, next, function(){
        res.status(200).send({
          links: {
            view: {url: '/buku/' + resData.node._id, method: 'GET'}
          }
        })
      })
    }
    else if(req.body.pinjamId){
      const relationData = {
        startNodeId: resData.node._id,
        endNodeId: req.body.pinjamId,
        type: 'HAS_PARENT',
      }
  
      internalService.createRelationship(relationData, res, next, function(){
        res.status(200).send({
          links: {
            view: {url: '/buku/' + resData.node._id, method: 'GET'}
          }
        })
      })
    }else{
      res.status(200).send({
        links: {
          view: {url: '/buku/' + resData.node._id, method: 'GET'}
        }
      })
    }
  })
})

router.put('/:_id', function(req, res, next) {
  var _id = req.params._id
  var data = {
    label: 'Buku',
    id: parseInt(_id),
    properties: req.body
  }

  internalService.updateNode(data, res, next, function(){
    res.status(200).send({
      links: {
        index: {url: '/buku/' + _id, method: 'GET'}
      }
    })
  })
})

router.get('/:_id', function(req, res, next) {
  var id = req.params._id
  internalService.getNode(id, 'TRUE', res, next, function(resData) {

    const data = []
    var item = resData

    data.push({
      id_buku: item.node.id_buku ? item.node.id_buku : null,
      book_code: item.node.book_code ? item.node.book_code : null,
      judul: item.node.judul ? item.node.judul : null,
      penulis: item.node.penulis ? item.node.penulis : null,
      penerbit: item.node.penerbit ? item.node.penerbit : null,
      terbit: item.node.terbit ? item.node.terbit : null,
      status: item.node.status ? item.node.status : null,
      createdAt: item.node.createdAt ? item.node.createdAt : null,
      createdBy: item.node.createdBy ? item.node.createdBy : null,
      updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
      updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
      links: {
        delete: {url: '/buku/' + item.node._id, method: 'DELETE'},
        edit: {url: '/buku/' + item.node._id, method: 'PUT'}
      }
    })
      
    res.render('buku/details', {
      title: `${serverConfig.server.name.toUpperCase()}`,
      user: res.locals.user,
      data: data[0]
    })    
  }
  )
})

router.delete('/:_id', function(req, res, next) {
  const _id = req.params._id

  internalService.deleteNode(_id, res, next, function() {
    res.status(200).send({
      links: {
        index: {
          url: '/buku/',
          method: 'GET'
        }
      }
    })
  })
})

module.exports = router
