const router = require('express').Router()
const internalService = require('../lib/InternalService')

const dataStructure = [
  {name: 'name', label: 'Name', link: 'view', filter: {type: 'text'}, defaultSort: 'asc'},
  {name: 'type', label: 'Type', filter: {type: 'text'}},
  {name: 'label', label: 'Label', filter: {type: 'text'}},
  {name: 'locatedAt', label: 'Located at', filter:{type: 'text'}},
  {name: 'purpose', label: 'Purpose', filter:{type: 'text'}},
  {name: 'status', label: 'Status', filter:{type: 'text'}},
  {name: 'createdAt', label: 'Created At', filter:{type: 'text'}},
  {name: 'createdBy', label: 'Created By', filter: {type: 'text'}}
]

router.get('/data', function(req, res, next) {
  const queries = req.query
  const reqData = {
    limit: 0,
    label: 'PowerEquipment',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {}
  }

  if (queries.order) {
    queries.order.forEach(function(item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.name] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function(item) {
      if (item.search.value !== '' && item.name) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.name] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function(resData) {
      const data = []

      resData.nodes.forEach(function(item) {
        data.push({
          _id: item.node._id ? item.node._id : null,
          name: item.node.name ? item.node.name : null,
          type: item.node.type ? item.node.type : null,
          ipAddress: item.node.ipAddress ? item.node.ipAddress : null,
          label: item.node.label ? item.node.label : null,
          uid: item.node.uid ? item.node.uid : null,
          height: item.node.height ? item.node.height : null,
          width: item.node.width ? item.node.width : null,
          length: item.node.length ? item.node.length : null,
          serialNumber: item.node.serialNumber ? item.node.serialNumber : null,
          manufacturer: item.node.manufacturer ? item.node.manufacturer : null,
          model: item.node.model ? item.node.model : null,
          inputCurrentType: item.node.inputCurrentType ? item.node.inputCurrentType: null,
          inputVoltage: item.node.inputVoltage ? item.node.inputVoltage: null,
          inputAmpere: item.node.inputAmpere ? item.node.inputAmpere: null,
          outputCurrentType: item.node.outputCurrentType ? item.node.outputCurrentType: null,
          outputVoltage: item.node.outputVoltage ? item.node.outputVoltage: null,
          outputAmpere: item.node.outputAmpere ? item.node.outputAmpere: null,
          status: item.node.status ? item.node.status: null,
          purpose: item.node.purpose ? item.node.purpose: null,
          createdAt: item.node.createdAt ? item.node.createdAt : null,
          createdBy: item.node.createdBy ? item.node.createdBy : null,
          updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
          updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
          links: {
            view: {url: '/powerEquipment/' + item.node._id, method: 'GET'}
          },
          floor:[]
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }

      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function(err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }

      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})

router.get('/', function(req, res, next) {
  res.render('powerEquipment/list', { title: `${serverConfig.server.name.toUpperCase()}`,
    user: res.locals.user,
    dataStructure: dataStructure,
    links: {
      data: {url: '/powerEquipment/data', method: 'GET'}
    }
  })
})

router.post('/', function(req, res, next) {
  let params = req.body
  params['createdBy'] = res.locals.user
  params['spaceId'] = parseInt(req.body.spaceId)
  params['ipOn'] = req.get('host')
  let request = {
    collection : 'IDC-FE' ,
    type : 'QUERY',
    name : 'CREATE_POWER_EQUIPMENT',
    body : {
      params : params
    }
  }
  internalService.predefinedQuery(request, res, next, function(resData) {
    res.status(200).send({
      links: {
        view: {url: '/powerEquipment/' + resData[0]._id, method: 'GET'}
      }
    })
  })
})

router.put('/:_id', function(req, res, next) {
  var _id = req.params._id
  var data = {
    label: 'PowerEquipment',
    id: parseInt(_id),
    properties: req.body
  }

  internalService.updateNode(data, res, next, function(){
    res.status(200).send({
      links: {
        index: {url: '/powerEquipment/' + _id, method: 'GET'}
      }
    })
  })
})

router.get('/:_id', function(req, res, next) {
  var id = req.params._id
  internalService.getNode(id, 'TRUE', res, next, function(item) {

    const data = []
    
    data.push({
      _id: item.node._id ? item.node._id : null,
      name: item.node.name ? item.node.name : null,
      type: item.node.type ? item.node.type : null,
      ipAddress: item.node.ipAddress ? item.node.ipAddress : null,
      label: item.node.label ? item.node.label : null,
      uid: item.node.uid ? item.node.uid : null,
      height: item.node.height ? item.node.height : null,
      width: item.node.width ? item.node.width : null,
      length: item.node.length ? item.node.length : null,
      serialNumber: item.node.serialNumber ? item.node.serialNumber : null,
      manufacturer: item.node.manufacturer ? item.node.manufacturer : null,
      model: item.node.model ? item.node.model : null,
      inputCurrentType: item.node.inputCurrentType ? item.node.inputCurrentType: null,
      inputVoltage: item.node.inputVoltage ? item.node.inputVoltage: null,
      inputAmpere: item.node.inputAmpere ? item.node.inputAmpere: null,
      outputCurrentType: item.node.outputCurrentType ? item.node.outputCurrentType: null,
      outputVoltage: item.node.outputVoltage ? item.node.outputVoltage: null,
      outputAmpere: item.node.outputAmpere ? item.node.outputAmpere: null,
      status: item.node.status ? item.node.status: null,
      purpose: item.node.purpose ? item.node.purpose: null,
      createdAt: item.node.createdAt ? item.node.createdAt : null,
      createdBy: item.node.createdBy ? item.node.createdBy : null,
      updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
      updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
      links: {
        delete: {url: '/powerEquipment/' + item.node._id, method: 'DELETE'},
        edit: {url: '/powerEquipment/' + item.node._id, method: 'PUT'}
      },
      shelfData: []
    })

    res.render('powerEquipment/details', {
      title: `${serverConfig.server.name.toUpperCase()}`,
      user: res.locals.user,
      data: data[0]
    })    
  }
  )
})

router.delete('/:_id', function(req, res, next) {
  const _id = req.params._id

  internalService.deleteNode(_id, res, next, function() {
    res.status(200).send({
      links: {
        index: {
          url: '/powerEquipment/',
          method: 'GET'
        }
      }
    })
  })
})

module.exports = router