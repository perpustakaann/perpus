const router = require('express').Router()
const internalService = require('../lib/InternalService')
const internalServicePrefman = require('../lib/PrefmanService')


const dataStructure = [
  {name: 'name', label: 'Name', link: 'view', filter: {type: 'text'}, defaultSort: 'asc'},
  {name: 'type', label: 'Type', filter: {type: 'text'}},
  {name: 'voltage', label: 'Type', filter: {type: 'text'}},
  {name: 'ampere', label: 'Type', filter: {type: 'text'}},
  {name: 'sequence', label: 'Type', filter: {type: 'text'}},
  {name: 'purpose', label: 'Purpose', filter: {type: 'text'}},
  {name: 'createdAt', label: 'Created At', filter:{type: 'text'}},
  {name: 'createdBy', label: 'Created By', filter: {type: 'text'}}
]

router.get('/data', function(req, res, next) {
  const queries = req.query
  const reqData = {
    limit: 0,
    label: 'PowerUnit',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {}
  }

  if (queries.order) {
    queries.order.forEach(function(item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.name] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function(item) {
      if (item.search.value !== '' && item.name) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.name] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function(resData) {
      const data = []

      resData.nodes.forEach(function(item) {
        data.push({
          _id: item.node._id ? item.node._id : null,
          name: item.node.name ? item.node.name : null,
          type: item.node.type ? item.node.type : null,
          uid: item.node.uid ? item.node.uid : null,
          sequence:item.node.sequence ? item.node.sequence : null,
          voltage:item.node.voltage ? item.node.voltage : null,
          ampere:item.node.ampere ? item.node.ampere : null,
          purpose: item.node.purpose ? item.node.purpose : null,
          status: item.node.status ? item.node.status: null,
          createdAt: item.node.createdAt ? item.node.createdAt : null,
          createdBy: item.node.createdBy ? item.node.createdBy : null,
          updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
          updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
          links: {
            view: {url: '/powerUnit/' + item.node._id, method: 'GET'}
          }
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }

      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function(err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }

      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})

router.get('/', function(req, res, next) {
  res.render('powerUnit/list', { title: `${serverConfig.server.name.toUpperCase()}`,
    user: res.locals.user,
    dataStructure: dataStructure,
    links: {
      data: {url: '/powerUnit/data', method: 'GET'}
    }
  })
})

router.post('/', function(req, res, next) {
  console.log(req.body)
  const data = req.body.data
  const category = req.body.category
  const type = req.body.type
  data.forEach((i) => {
    const label = i.label
    const getValueType = label.split('(')
    const paramGetElectric = {
      name : "PowerUnitLine",
      params : {
        _type : getValueType[0]
      }
    }
    internalServicePrefman.boilerPlate(
      paramGetElectric,
      res,
      next,
      function(resData) {
        let lineInfo = resData
        const paramGetSpect = {
          name : "PowerUnitSpec",
          params : {
            _type : getValueType[0]
          }
        }
        internalServicePrefman.boilerPlate(
          paramGetSpect,
          res,
          next,
          function(resDataSpect) {
            const spectInfo = resDataSpect
            const propertiesPowerUnit = {
              type: getValueType[0], // `Power Unit Type` -- diambil dari Reference Value
              category: category, // `Power Unit category` -- diambil dari Reference Value
              name: i.name, //'`Power Unit Name` -- free text 
              label: i.label, //'`Power Unit Name Alias` -- free text    
              leftRightPosition: i.x, //'`Power Unit position from left to right ` -- free text 
              topDownPosition:i.y, //'`Power Unit position from top to down` -- free text 
              serialNumber: '', // `Serial number dari Power Unit 
              manufacturer:'', // `pabrikan dari Power Unit
              model:'', //model Power Unit
              currentType: 'AC', //jenis input (AC | DC) Power Unit
              voltage:380, //besaran voltage  (Volt) Power Unit
              ampere:10, //besaran arus (Amp) Power Unit  
              status:'ACTIVE', //status Power Unit 
              purpose:'IDC', //peruntukan dari suatu Power Unit -- diambil dari Reference Value
              createdAt: new Date(), // tanggal create data
              createdBy: res.locals.user // user yang melakukan input data
            }
            if(spectInfo.length > 0){
              spectInfo.forEach((i) => {
                propertiesPowerUnit[i.name] = i.value
              })
            }

            // const propertiesElectricLine = {}


            
          }
        )
      }
    )
  })
  
  // let params = req.body
  // params['createdBy'] = res.locals.user
  // params['pduId'] = parseInt(req.body.pduId)
  // params['ipOn'] = req.get('host')
  // let request = {
  //   collection : 'IDC-FE' ,
  //   type : 'QUERY',
  //   name : 'CREATE_POWER_UNIT',
  //   body : {
  //     params : params
  //   }
  // }
  // internalService.predefinedQuery(request, res, next, function(resData) {
  //   res.status(200).send({
  //     links: {
  //       view: {url: '/powerUnit/' + resData[0]._id, method: 'GET'}
  //     }
  //   })
  // })
})

router.put('/:_id', function(req, res, next) {
  var _id = req.params._id
  var data = {
    label: 'PowerUnit',
    id: parseInt(_id),
    properties: req.body
  }

  internalService.updateNode(data, res, next, function(){
    res.status(200).send({
      links: {
        index: {url: '/powerUnit/' + _id, method: 'GET'}
      }
    })
  })
})

router.get('/:_id', function(req, res, next) {
  var id = req.params._id
  internalService.getNode(id, 'TRUE', res, next, function(resData) {

    const data = []
    data.push({
      _id: resData.node._id ? resData.node._id : null,
      name: resData.node.name ? resData.node.name : null,
      type: resData.node.type ? resData.node.type : null,
      uid: resData.node.uid ? resData.node.uid : null,
      serialNumber: resData.node.serialNumber ? resData.node.serialNumber : null,
      manufacturer: resData.node.manufacturer ? resData.node.manufacturer : null,
      model: resData.node.model ? resData.node.model : null,
      voltage: resData.node.voltage ? resData.node.voltage : null,
      label: resData.node.label ? resData.node.label : null,
      ampere: resData.node.ampere ? resData.node.ampere : null,
      currentType: resData.node.currentType ? resData.node.currentType : null,
      purpose: resData.node.purpose ? resData.node.purpose : null,
      status: resData.node.status ? resData.node.status : null,
      createdAt: resData.node.createdAt ? resData.node.createdAt : null,
      createdBy: resData.node.createdBy ? resData.node.createdBy : null,
      updatedAt: resData.node.updatedAt ? resData.node.updatedAt : null,
      updatedBy: resData.node.updatedBy ? resData.node.updatedBy : null,
      links: {
        delete: {url: '/powerUnit/' + resData.node._id, method: 'DELETE'},
        edit: {url: '/powerUnit/' + resData.node._id, method: 'PUT'}
      }
    })
    res.render('powerUnit/details', {
      title: `${serverConfig.server.name.toUpperCase()}`,
      user: res.locals.user,
      data: data[0]
    })    
  }
  )
})

router.delete('/:_id', function(req, res, next) {
  const _id = req.params._id
  let params = {}
  params['powerUnitId'] = parseInt(_id)
  let request = {
    collection : 'IDC-FE' ,
    type : 'QUERY',
    name : 'DELETE_POWER_UNIT',
    body : {
      params : params
    }
  }
  internalService.predefinedQuery(request, res, next, function() {
    res.status(200).send({
      links: {
        index: {
          url: '/powerUnit/',
          method: 'GET'
        }
      }
    })
  })
})

module.exports = router
