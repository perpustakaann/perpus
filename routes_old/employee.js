const router = require('express').Router()
const internalService = require('../lib/InternalService')

const dataStructure = [
  {name: 'employeeNum', label: 'Employee Number', filter: {type: 'text'}, defaultSort: 'asc'},
  {name: 'nik', label: 'Nik', filter: {type: 'text'}},
  {name: 'name', label: 'Name', link: 'view', filter: {type: 'text'}, defaultSort: 'asc'},
  {name: 'gender', label: 'Gender', filter: {type: 'text'}},
  {name: 'bod', label: 'Bod', filter: {type: 'date'}},
  {name: 'email', label: 'Email', filter: {type: 'text'}},
  {name: 'telp', label: 'Phone',filter: {type: 'text'}},
  {name: 'division', label: 'Division',filter: {type: 'text'}},
  {name: 'job_title', label: 'Job title',filter: {type: 'text'}},
  {name: 'status', label: 'Status', type: 'boolean'},
  {name: 'createdAt', label: 'Created At', type: 'text'},
  {name: 'createdBy', label: 'Created By', filter: {type: 'text'}}
]

router.get('/data', function(req, res, next) {
  const queries = req.query
  const reqData = {
    limit: 0,
    label: 'Employee',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {},
    optionalNodes: [
      {
        label: "valid_employee",
        relationLabel: "HAS_PARENT",
        // directionSensitive: true,
        includeRelationship: true,
        collection: true
      },
    ]
  }

  if (queries.order) {
    queries.order.forEach(function(item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.name] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function(item) {
      if (item.search.value !== '' && item.name) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.name] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function(resData) {
      const data = []

      resData.nodes.forEach(function(item) {

        data.push({
          _id: item.node._id ? item.node._id : null,
        //   objectName: item.objectOpt[0].name,
          employeeNum: item.node.employeeNum ? item.node.employeeNum : null,
          nik: item.node.nik ? item.node.nik : null,
          name: item.node.name ? item.node.name : null,
          gender: item.node.gender ? item.node.gender : null,
          bod: item.node.bod ? item.node.bod : null,
          email: item.node.email ? item.node.email : null,
          telp: item.node.telp ? item.node.telp : null,
          division: item.node.division ? item.node.division : null,
          job_title: item.node.job_title ? item.node.job_title : null,
          status: item.node.status ? item.node.status: null,
          createdAt: item.node.createdAt ? item.node.createdAt : null,
          createdBy: item.node.createdBy ? item.node.createdBy : null,
          updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
          updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
          links: {
            view: {url: '/employee/' + item.node._id, method: 'GET'},
            // viewObject: {url: '/object/' + item.objectOpt[0]._id, method: 'GET'},
          }
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }

      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function(err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }

      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})

router.get('/', function(req, res, next) {
  res.render('employee/list', {
    title: `${serverConfig.server.name.toUpperCase()}`,
    user: res.locals.user,
    dataStructure: dataStructure,
    links: {
      data: {url: '/employee/data', method: 'GET'}
    }
  })
})

router.post('/', function(req, res, next) {
  const reqData = {
    label: 'Employee',
    properties: {
      employeeNum: req.body.employeeNum,
      nik: req.body.nik,
      name: req.body.name,
      gender: req.body.gender,
      bod: req.body.bod,
      email: req.body.email,
      telp: req.body.telp,
      division: req.body.division,
      job_title: req.body.job_title,
      status: 'REGISTER'
    }
  }

  internalService.createNode(reqData, res, next, function(resData) {

    if(req.body.valId){
      const relationData = {
        startNodeId: resData.node._id,
        endNodeId: req.body.valId,
        type: 'HAS_PARENT',
      }
  
      internalService.createRelationship(relationData, res, next, function(){
        res.status(200).send({
          links: {
            view: {url: '/employee/' + resData.node._id, method: 'GET'}
          }
        })
      })
    }else{
      res.status(200).send({
        links: {
          view: {url: '/employee/' + resData.node._id, method: 'GET'}
        }
      })
    }
  })
})

router.put('/:_id', function(req, res, next) {
  var _id = req.params._id
  var data = {
    label: 'Employee',
    id: parseInt(_id),
    properties: req.body
  }

  internalService.updateNode(data, res, next, function(){
    res.status(200).send({
      links: {
        index: {url: '/employee/' + _id, method: 'GET'}
      }
    })
  })
})

router.get('/:_id', function(req, res, next) {
  var id = req.params._id
  internalService.getNode(id, 'TRUE', res, next, function(resData) {

    const data = []
    var item = resData

    data.push({
      employeeNum: item.node.employeeNum ? item.node.employeeNum : null,
      nik: item.node.nik ? item.node.nik : null,
      name: item.node.name ? item.node.name : null,
      gender: item.node.gender ? item.node.gender : null,
      bod: item.node.bod ? item.node.bod : null,
      email: item.node.email ? item.node.email : null,
      telp: item.node.telp ? item.node.telp : null,
      division: item.node.division ? item.node.division : null,
      job_title: item.node.job_title ? item.node.job_title : null,
      status: item.node.status ? item.node.status: null,
      createdAt: item.node.createdAt ? item.node.createdAt : null,
      createdBy: item.node.createdBy ? item.node.createdBy : null,
      updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
      updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
      links: {
        delete: {url: '/employee/' + item.node._id, method: 'DELETE'},
        edit: {url: '/employee/' + item.node._id, method: 'PUT'}
      }
    })
      
    res.render('employee/details', {
      title: `${serverConfig.server.name.toUpperCase()}`,
      user: res.locals.user,
      data: data[0]
    })    
  }
  )
})

router.delete('/:_id', function(req, res, next) {
  const _id = req.params._id

  internalService.deleteNode(_id, res, next, function() {
    res.status(200).send({
      links: {
        index: {
          url: '/employee/',
          method: 'GET'
        }
      }
    })
  })
})

module.exports = router
