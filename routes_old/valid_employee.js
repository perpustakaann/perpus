const router = require('express').Router()
const internalService = require('../lib/InternalService')

const dataStructure = [
  {name: 'valid_employee', label: 'Validasi employee', link: 'view', filter: {type: 'text'}, defaultSort: 'asc'},
  {name: 'verifi_Employeename', label: 'Employee Name', filter: {type: 'text'}},
  {name: 'tenant', label: 'Tenant', filter: {type: 'text'}},
  {name: 'createdAt', label: 'Created At', type: 'date'},
  {name: 'createdBy', label: 'Created By', filter: {type: 'text'}}
]

router.get('/data', function(req, res, next) {
  const queries = req.query
  const reqData = {
    limit: 0,
    label: 'Valid_employee',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {}
  }

  if (queries.order) {
    queries.order.forEach(function(item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.valid_employee] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function(item) {
      if (item.search.value !== '' && item.valid_employee) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.valid_employee] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function(resData) {
      const data = []

      resData.nodes.forEach(function(item) {
        data.push({
          _id: item.node._id ? item.node._id : null,
          valid_employee: item.node.valid_employee ? item.node.valid_employee : null,
          verifi_Employeename: item.node.verifi_Employeename ? item.node.verifi_Employeename : null,
          tenant: item.node.tenant ? item.node.tenant : null,
          createdAt: item.node.createdAt ? item.node.createdAt : null,
          createdBy: item.node.createdBy ? item.node.createdBy : null,
          updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
          updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
          links: {
            view: {url: '/valid_employee/' + item.node._id, method: 'GET'}
          }
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }

      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function(err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }

      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})

router.get('/', function(req, res, next) {
  res.render('valid_employee/list', {
    title: `${serverConfig.server.name.toUpperCase()}`,
    user: res.locals.user,
    dataStructure: dataStructure,
    links: {
      data: {url: '/valid_employee/data', method: 'GET'}
    }
  })
})

router.post('/', function(req, res, next) {
  const reqData = {
    label: 'Valid_employee',
    properties: {
      valid_employee: req.body.valid_employee,
      verifi_Employeename: req.body.verifi_Employeename,
      tenant: req.body.tenant
    }
  }

  internalService.createNode(reqData, res, next, function(resData) {
    res.status(200).send({
      links: {
        view: {url: '/valid_employee/' + resData.node._id, method: 'GET'}
      }
    })
  })
})

router.put('/:_id', function(req, res, next) {
  var _id = req.params._id
  var data = {
    label: 'Valid_employee',
    id: parseInt(_id),
    properties: req.body
  }

  internalService.updateNode(data, res, next, function(){
    res.status(200).send({
      links: {
        index: {url: '/valid_employee/' + _id, method: 'GET'}
      }
    })
  })
})

router.get('/:_id', function(req, res, next) {
  var id = req.params._id
  internalService.getNode(id, 'TRUE', res, next, function(resData) {

    const data = []
    
    data.push({
      _id: resData.node._id ? resData.node._id : null,
      valid_employee: resData.node.valid_employee ? resData.node.valid_employee : null,
      verifi_Employeename: resData.node.verifi_Employeename ? resData.node.verifi_Employeename : null,
      tenant: resData.node.tenant ? resData.node.tenant : null,
      createdAt: resData.node.createdAt ? resData.node.createdAt : null,
      createdBy: resData.node.createdBy ? resData.node.createdBy : null,
      updatedAt: resData.node.updatedAt ? resData.node.updatedAt : null,
      updatedBy: resData.node.updatedBy ? resData.node.updatedBy : null,
      links: {
        delete: {url: '/valid_employee/' + resData.node._id, method: 'DELETE'},
        edit: {url: '/valid_employee/' + resData.node._id, method: 'PUT'},
        addEmployee: {url: '/valid_employee/addEmployee/', method: 'POST'},
      },
      employee: [],
    })

    resData.relationships.forEach(function(itemRelationship){

      if(itemRelationship.relationship != null){
        

        if(itemRelationship.relationship._type == "HAS_PARENT"){

          if(itemRelationship.node._labels.includes("Employee")){
            data[0].employee.push({
              _id: itemRelationship.node._id ? itemRelationship.node._id : null,
              employeeNum: itemRelationship.node.employeeNum ? itemRelationship.node.employeeNum : null,
              nik: itemRelationship.node.nik ? itemRelationship.node.nik : null,
              name: itemRelationship.node.name ? itemRelationship.node.name : null,
              gender: itemRelationship.node.gender ? itemRelationship.node.gender : null,
              bod: itemRelationship.node.bod ? itemRelationship.node.bod : null,
              email: itemRelationship.node.email ? itemRelationship.node.email : null,
              telp: itemRelationship.node.telp ? itemRelationship.node.telp : null,
              division: itemRelationship.node.division ? itemRelationship.node.division : null,
              job_title: itemRelationship.node.job_title ? itemRelationship.node.job_title : null,
              status: itemRelationship.node.status ? (itemRelationship.node.status == 'REGISTER') : null,
              createdAt: itemRelationship.node.createdAt ? itemRelationship.node.createdAt : null,
              createdBy: itemRelationship.node.createdBy ? itemRelationship.node.createdBy : null,
              updatedAt: itemRelationship.node.updatedAt ? itemRelationship.node.updatedAt : null,
              updatedBy: itemRelationship.node.updatedBy ? itemRelationship.node.updatedBy : null,
              links: {
                view: {url: '/employee/' + itemRelationship.node._id, method: 'DELETE'},
                delete: {url: '/valid_employee/employee/' + resData.node._id + '/' + itemRelationship.node._id, method: 'DELETE'},
              }
            })
          }

        }
        
      }

    })

    res.render('valid_employee/details', {
      title: `${serverConfig.server.name.toUpperCase()}`,
      user: res.locals.user,
      data: data[0]
    })    
  }
  )
})

router.delete('/:_id', function(req, res, next) {
  const _id = req.params._id

  internalService.deleteNode(_id, res, next, function() {
    res.status(200).send({
      links: {
        index: {
          url: '/valid_employee/',
          method: 'GET'
        }
      }
    })
  })
})

module.exports = router
