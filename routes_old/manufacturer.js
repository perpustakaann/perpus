const express = require('express');
const router = express.Router();

const security = require('../lib/Security');
const internalService = require('../lib/ISPService');

router.get('/data', function (clientRequest, clientResponse, next) {
  // security.protect('DEVICE', clientRequest, clientResponse, next, function () {
    const queries = clientRequest.query;
    const requestData = {
      limit: 0,
      label: "Manufacturer",
    };

    internalService.listNodes(requestData, clientResponse, next, function (responseData) {
      const data = [];
      responseData.nodes.forEach(function (item, label) {
        data.push({
          id: item.node._id,
          name: item.node.name,
        });
      });
      clientResponse.status(200).send(data);
    });
  // });
});

router.post('/sto', function (clientRequest, clientResponse, next) {
  // security.protect('MANUFACTURE', clientRequest, clientResponse, next, function () {
    const relationData = {
      startNodeId: clientRequest.body.stoId,
      endNodeId: clientRequest.body.manufactureId,
      type: "SERVED_BY",
      properties: {
        technology: clientRequest.body.technology,
      }
    };

    internalService.createRelationship(relationData, clientResponse, next, function () {
      clientResponse.status(200).send({
        links: {
          view: {url: "/area/sto/" + clientRequest.body.stoId, method: "GET"}
        }
      });
    });
  // });
});

router.put('/sto', function (clientRequest, clientResponse, next) {
  // security.protect('MANUFACTURE', clientRequest, clientResponse, next, function () {
    const relationData = {
      startNode: clientRequest.body.stoId,
      endNode: clientRequest.body.manufactureId,
      type: "SERVED_BY",
      id: clientRequest.body.relationId,
      properties: {
        technology: clientRequest.body.technology,
      }
    };

    internalService.updateRelationship(relationData, clientResponse, next, function () {
      clientResponse.status(200).send({
        links: {
          view: {url: "/area/sto/" + clientRequest.body.stoId, method: "GET"}
        }
      });
    });
  // });
});

router.delete('/sto/:stoId/:relationId', function (clientRequest, clientResponse, next) {
  const relationshipId = clientRequest.params.relationId;
  const stoId = clientRequest.params.stoId;

  // security.protect('MANUFACTURE', clientRequest, clientResponse, next, function () {
    internalService.deleteRelationship(relationshipId, clientResponse, next, function () {
      clientResponse.status(200).send({
        links: {
          index: {
            url: "/area/sto/" + stoId,
            method: "GET"
          }
        }
      });
    });
  // });
});

module.exports = router;
