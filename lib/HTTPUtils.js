// const axios = require('axios')
const fs = require('fs')
const services = require('./WebService')
const token = fs.readFileSync('keys/token')
const log = serverUtils.getLogger('lib.HTTPUtils')

const aaa = services.aaa
const spatial = services.spatial
const nodered = services.nodered

exports.getSTOByBBox = function(bounds) {
  let path =
    `polygons?encode_geom=${!!bounds.encodeGeom}&zoom_level=${bounds.zoomLevel}` +
    `&north=${bounds.north}&east=${bounds.east}&south=${bounds.south}&west=${bounds.west}`
  let headers = {'accept-encoding': 'gzip, deflate'}
  log.info(`[GET] ${serverConfig.service.spatial.baseURL}${path}`)
  return spatial.get(path, {headers: headers})
}

exports.getAllSTO = function() {
  let path = 'allSTO'
  let headers = {'accept-encoding': 'gzip, deflate'}
  log.info(`[GET] ${serverConfig.service.spatial.baseURL}${path}`)
  return spatial.get(path, {headers: headers})
}

exports.getSTOById = function(id) {
  let path = `byId?id=${id}`
  let headers = {'accept-encoding': 'gzip, deflate'}
  log.info(`[GET] ${serverConfig.service.spatial.baseURL}${path}`)
  return spatial.get(path, {headers: headers})
}

exports.getNearest = function(lat, lng) {
  let path = `nearest?lat=${lat}&lng=${lng}`
  let headers = {'accept-encoding': 'gzip, deflate'}
  log.info(`[GET] ${serverConfig.service.spatial.baseURL}${path}`)
  return spatial.get(path, {headers: headers})
}

exports.updateNodeGeom = function(user, data) {
  let path = 'geometry'
  let headers = {'X-Requestor': user}
  let method = 'PUT'
  log.info(`[${method}] ${serverConfig.service.spatial.baseURL}${path} - data `, data)
  return new Promise((resolve, reject) => {
    spatial({
      url: path,
      method: method,
      data: data,
      headers: headers
    }).then(resp => resolve(handleSuccess(resp)), err => reject(parseErr(err)))
  })
}

exports.postWLANDisc = function(data) {
  return new Promise((resolve, reject) => {
    nodered({
      url: '/wlan-disc',
      method: 'POST',
      data: data
    }).then(resp => resolve(handleSuccess(resp)), err => reject(parseErr(err)))
  })
}

exports.postJoviceVLANDisc = function(data) {
  console.log(data)
  return new Promise((resolve, reject) => {
    nodered({
      url: '/jovice-disc/sub-interface',
      method: 'POST',
      data: data
    }).then(resp => resolve(handleSuccess(resp)), err => reject(parseErr(err)))
  })
}

exports.postJovicePortDisc = function(data) {
  return new Promise((resolve, reject) => {
    nodered({
      url: '/jovice-disc/port',
      method: 'POST',
      data: data
    }).then(resp => resolve(handleSuccess(resp)), err => reject(parseErr(err)))
  })
}

exports.auth = function(username, password) {
  return new Promise((resolve, reject) => {
    aaa
      .post(
        '/auth/authenticate',
        {
          username: username,
          password: password
        },
        {headers: {Cookie: 'access_token=' + token + '; Secure; HttpOnly;'}}
      )
      .then(
        resp => {
          let apiResp = {
            status: resp.status,
            message: resp.statusText,
            data: resp.data,
            headers: resp.headers,
            error: false
          }
          resolve(apiResp)
        },
        err => {
          let apiResp = err.response.data
          apiResp.status = err.response.status
          apiResp.statusText = err.response.statusText
          reject(apiResp)
        }
      )
  })
}

exports.aaaGetNodeList = function(label, data) {
  return aaa
    .post('/common/listNode?label=' + label, data, {
      headers: {Cookie: 'access_token=' + token + '; Secure; HttpOnly;'}
    })
    .then(function(resp) {
      let apiResp = {
        status: resp.status,
        message: resp.statusText,
        data: resp.data,
        headers: resp.headers,
        error: false
      }
      return apiResp
    })
    .catch(function(err) {
      let apiResp = {
        status: err.response.status,
        message: err.response.statusText,
        description: '[' + err.response.data.error + '] ' + err.response.data.error_description,
        error: true
      }
      return apiResp
    })
}

exports.lambdaGetServiceOrderInfo = function(id) {
  return services.lambda
    .get('/getServiceOrderInfo/' + id)
    .then(function(resp) {
      return resp.data
    })
    .catch(function(err) {
      return err
    })
}

exports.lambdaListServiceOrder = function(serviceName, startDate, endDate) {
  return services.lambda
    .get('/listServiceOrder?serviceName=' + serviceName + '&startDate=' + startDate + '&endDate=' + endDate)
    .then(function(resp) {
      return resp.data
    })
    .catch(function(err) {
      return err
    })
}

exports.lambdaUpdateServiceOrder = function(data) {
  return services.lambda
    .put('/updateServiceOrder', data)
    .then(function(resp) {
      return resp.data
    })
    .catch(function(err) {
      return err
    })
}

function parseErr(err) {
  if (err.response) {
    return {
      status: err.response.status,
      message: err.response.statusText,
      description: err.response.data.error,
      error: true
    }
  } else {
    return {
      status: 500,
      message: err.errno + ':' + err.code,
      description: err.message,
      error: true
    }
  }
}

function handleSuccess(resp) {
  return {
    status: resp.status,
    message: resp.statusText,
    data: resp.data,
    headers: resp.headers,
    error: false
  }
}
