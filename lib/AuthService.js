const services = require('./WebService')
const config = require('./Config')
const idm = services.idm
const log = serverUtils.getLogger('lib.AuthService')

exports.authenticate = function (username, password, callback, fallout) {
  log.info(`[POST] /v1/auth/token { "grant_type": "password", "username": "${username}", "password": "${password}" }`)
  idm
    .post(`/v1/auth/token?aud=${config.auth.audience}`, `grant_type=password&username=${username}&password=${password}`, {
      headers: {
        Authorization: 'Basic ' + Buffer.from(`${config.auth.clientId}:${config.auth.clientSecret}`).toString('base64'),
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    })
    .then(function (resp) {
      callback(resp.data)
    })
    .catch(function (err) {
      fallout(err)
    })
}

exports.authorize = function (authToken, resource, action, next, callback, fallout) {
  const req = `token=${authToken}&token_type_hint=access_token&scope=${config.auth.audience},${resource},${action}`
  const logInfo = {
    audience : config.auth.audience,
    action : action
  } 
  log.info(`[POST] ${idm.defaults.baseURL}/v1/auth/introspect ${JSON.stringify(logInfo)}`)
  idm.post(`/v1/auth/introspect?aud=${config.auth.audience}`, req, {
      headers: {
        Authorization: 'Basic ' + Buffer.from(`${config.auth.clientId}:${config.auth.clientSecret}`).toString('base64'),
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).then(function (resp) {
      if (!resp.data.active) {
        throw {
          status: 403,
          message: 'Access denied',
          description: action.name + ' ' + resource.name + (resource.networkRole ? ' ' + resource.networkRole : '')
        }
      }
      if (callback)
        callback()
    })
    .catch(function (err) {
      if (fallout) fallout(err)
      else next(err)
    })
}

// exports.userinfo = function(authToken, next, callback, fallout) {
//   log.info(`[GET] ${idm.defaults.baseURL}v1/oauth2/userinfo`)
//   idm
//     .get('/v1/oauth2/userinfo', {
//       headers: {Authorization: 'Bearer ' + authToken}
//     })
//     .then(function(resp) {
//       callback(resp.data)
//     })
//     .catch(function(err) {
//       fallout(err)
//     })
// }