const internalService = require('./InternalService')
const authService = require('./AuthService')
const log = serverUtils.getLogger('lib.Security')

// READ = GET, LIST
// MODIFY = CREATE, UPDATE, DELETE
// GET = get single object
// LIST = get multiple objects
// CREATE = create one object
// UPDATE = update one object
// DELETE = delete one object
function getAuthSpec(context, resourceAttribute) {
  let resource = {name: context.scope}
  let action = {name: context.method}

  log.info(
    JSON.stringify({
      requestAttribute: {
        module: context.scope,
        path: context.path,
        method: context.method,
        resourceAttribute: resourceAttribute
      }
    })
  )

  switch (context.scope) {
    case 'AREA':
      if (context.method === 'GET') {
        action.name = 'READ'
      } else {
        action.name = 'MODIFY'
      }
      break
    case 'DEVICE':
      if (context.method === 'GET') {
        action.name = 'READ'
      } else {
        resource.networkRole = resourceAttribute.networkRole.toUpperCase() // AN, ME, PE
        action.name = 'MODIFY'
        if (['WAC', 'WAG', 'AP'].includes(resource.networkRole)) resource.networkRole = 'AN'
      }
      break
    case 'MANUFACTURE':
      if (context.method === 'GET') {
        action.name = 'READ'
      } else {
        action.name = 'MODIFY'
      }
      break
    case 'ORDER_ACTIVATION':
      if (context.method === 'GET') {
        action.name = 'READ'
      } else {
        action.name = 'MODIFY'
      }
      break
    case 'PORT_PURPOSE':
      if (context.method === 'GET') {
        action.name = 'READ'
      } else {
        action.name = 'MODIFY'
      }
      break
    case 'SERVICE':
      if (context.method === 'GET') {
        action.name = 'READ'
      } else {
        action.name = 'MODIFY'
      }
      break
    case 'SERVICE_TYPE':
      if (context.method === 'GET') {
        action.name = 'READ'
      } else {
        action.name = 'MODIFY'
      }
      break
    case 'SUB_INTERFACE':
      if (context.method === 'GET') {
        action.name = 'READ'
      } else {
        action.name = 'MODIFY'
        resource.networkRole = resourceAttribute.networkRole.toUpperCase() // AN, ME, PE
      }
      break
    case 'VLAN_GROUP':
      if (context.method === 'GET') {
        action.name = 'READ'
      } else {
        action.name = 'MODIFY'
      }
      break
    case 'VLAN_POOL':
      if (context.method === 'GET') {
        action.name = 'READ'
      } else {
        action.name = 'MODIFY'
      }
      break
    case 'VRF':
      if (context.method === 'GET') {
        action.name = 'READ'
      } else {
        action.name = 'MODIFY'
      }
      break
    case 'CUSTOMER':
      if (context.method === 'GET') {
        action.name = 'READ'
      } else {
        action.name = 'MODIFY'
      }
      break
    case 'RT_RULES':
      if (context.method === 'GET') {
        action.name = 'READ'
      } else {
        action.name = 'MODIFY'
      }
      break
    case 'RD_RULES':
      if (context.method === 'GET') {
        action.name = 'READ'
      } else {
        action.name = 'MODIFY'
      }
      break
    case 'SSID':
      if (context.method === 'GET') {
        action.name = 'READ'
      } else {
        action.name = 'MODIFY'
      }
      break
    default:
      resource.name = '[' + context.scope + ']' + context.path
      log.error(`undefined resource ${resource.name}${action.name}`)
  }

  const authSpec = {
    resource: resource,
    action: action
  }

  log.debug(`{authSpec: ${JSON.stringify(authSpec)}}`)

  return authSpec
}

module.exports.check = function(req, res, next) {
  const authSpec = getAuthSpec(req.context, {})
  authService.authorize(res.locals.user.token, authSpec.resource, authSpec.action, next, next)
}

module.exports.protect = function(module, req, res, next, callback, fallout) {
  const authSpec = getAuthSpec(
    {
      scope: module,
      path: req.path,
      method: req.method
    },
    {}
  )
  authService.authorize(res.locals.user.token, authSpec.resource, authSpec.action, next, callback, fallout)
}

module.exports.protectWithAttribute = function(module, resourceAttr, clientRequest, clientResponse, next, callback) {
  const authSpec = getAuthSpec(
    {
      scope: module,
      path: clientRequest.path,
      method: clientRequest.method
    },
    resourceAttr
  )
  authService.authorize(clientResponse.locals.user.token, authSpec.resource, authSpec.action, next, callback)
}

module.exports.protectWithFetchDevice = function(nodeId, module, clientRequest, clientResponse, next, callback) {
  internalService.getNode(nodeId, false, clientResponse, next, function(responseData) {
    const authSpec = getAuthSpec(
      {
        scope: module,
        path: clientRequest.path,
        method: clientRequest.method
      },
      responseData.node
    )
    authService.authorize(clientResponse.locals.user.token, authSpec.resource, authSpec.action, next, callback)
  })
}

module.exports.protectWithFetchDeviceByName = function(
  deviceName,
  module,
  clientRequest,
  clientResponse,
  next,
  callback
) {
  const requestData = {
    limit: 1,
    label: 'Resource',
    propertyFilter: {
      name: deviceName
    }
  }

  internalService.listNodes(
    requestData,
    clientResponse,
    next,
    function(responseData) {
      const authSpec = getAuthSpec(
        {
          scope: module,
          path: clientRequest.path,
          method: clientRequest.method
        },
        responseData.nodes[0].node
      )
      authService.authorize(clientResponse.locals.user.token, authSpec.resource, authSpec.action, next, callback)
    },
    function(err) {
      next({
        status: 403,
        message: 'Access denied',
        description: module + ' ' + clientRequest.method
      })
    }
  )
}
