const express = require('express')
const router = express.Router()
// const log = serverUtils.getLogger('lib/Render')
const internalService = require('../lib/InternalService')

router.get(
  '/',
  function(req, res, next) {
    //   clientResponse.render('common/list', {
    //     user: clientResponse.locals.user,
    //     dataStructure: dataStructure,
    //     title: 'Location',
    //     links: {
    //       data: { url: '/rdRules/data?length=0', method: 'GET' },
    //       add: { url: '/rdRules', method: 'POST' },
    //       edit: { url: '/rdRules', method: 'PUT' },
    //       editCurrentValue: { url: '/rdRules/value', method: 'PUT' }
    //     }
    //   })
    if (!(req.context && req.context.model && req.context.model.label)) {
      next({status: 500, message: 'Label is mandatory for list query', isError: true})
      // return
    }

    let label = req.context.model.label
    if (!req.context.props) req.context.props = [{name: 'name', label: 'Name', filter: {type: 'text'}}]
    let bigData = !!(req.context && req.context.model && req.context.model.bigData)

    const queries = {
      limit: 10,
      label: label,
      skip: 0,
      propertyFilter: {}
    }
    if (!bigData) queries.propertySort = {name: 'asc'}
    Object.assign(queries, req.context.queries)

    if (!bigData)
      if (queries.order) {
        queries.order.forEach(function(item) {
          const field = req.context.props[item.column]
          if (field) {
            queries.propertySort[field.name] = item.dir
          }
        })
      } else queries.propertySort['name'] = 'asc'

    if (queries.columns) {
      queries.columns.forEach(function(item) {
        if (item.search.value !== '' && item.name) {
          queries.propertyFilter[item.name] =
            '(?i)' +
            item.search.value
              .replace(/ /g, '*')
              .replace(/\(/g, '\\(')
              .replace(/\)/g, '\\)') +
            '*'
        }
      })
    }

    internalService.listNodes(
      queries,
      res,
      next,
      function(resData) {
        let data = []

        resData.nodes.forEach(function(item) {
          const entry = {
            id: item.node._id,
            name: item.node.name,
            createdAt: item.node.createdAt ? new Date(item.node.createdAt).toUTCString() : 'N/A',
            createdBy: item.node.createdBy + (item.node.source ? ' (' + item.node.source + ')' : ''),
            links: {
              view: {url: '/ssid/' + item.node._id, method: 'GET'}
            }
          }

          data.push(entry)
        })

        let result = {
          draw: queries.draw,
          data: data,
          recordsFiltered: resData.pager.total,
          recordsTotal: resData.pager.total
        }

        next(result)
      },
      function(err) {
        if (err.response && err.response.status !== 404) {
          err.isError = true
          next(err)
        } else {
          if (err.constructor.name === 'Error') {
            next({status: 500, message: err.message, stack: err.stack, code: err.code, isError: true})
          } else {
            err.isError = true
            next(err)
          }
        }
      }
    )
  },
  executeRenderList
)

function executeRenderList(data, req, res, next) {
  if (data.isError) next(data)
  else {
    let context = Object.assign(
      {
        user: res.locals.user,
        props: req.context.props,
        title: req.context.model.label,
        id: req.context.model.label.toLowerCase()
      },
      data
    )
    res.render('common/list', {
      context: context,
      showData: true,
      dataToShow: context
    })
  }
}

module.exports = router
