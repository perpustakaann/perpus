const querystring = require('querystring')
const services = require('./WebService')
const uimax = services.isp
const log = serverUtils.getLogger('lib.InternalService')

exports.listNodes = function(data, res, next, callback, fallout) {
  log.info(`[POST] ${uimax.defaults.baseURL}/internal/node/list - data: ${JSON.stringify(data)}`)
  return uimax
    .post('/internal/node/list', data, {headers: {'X-Requestor': res.locals.userId}})
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (err.response)
        log.error('[ERROR] /internal/node/list - [%s:%s] %s\ndata = %o', err.response.status, err.response.statusText, err.message, data)
      else 
        log.error('[ERROR] /internal/node/list - [%s]\ndata = %o', err.message, data)
      if (fallout) {
        fallout(err)
      } else {
        if (err.response.status === 404) {
          callback({nodes: []})
        } else {
          next(err)}
      }
    })
}

exports.createNode = function(data, res, next, callback, fallout) {
  log.debug('[POST] /internal/node')
  log.debug(data)
  return uimax
    .post('/internal/node', data, {headers: {'X-Requestor': res.locals.userId}})
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.cypher = function(data, res, next, callback, fallout) {
  log.info('[POST] /internal/cypher')
  log.info(data)
  return uimax
    .post('/internal/cypher', data, {headers: {'X-Requestor': res.locals.userId}})
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.getNode = function(nodeId, includeRelation, res, next, callback, fallout) {
  log.debug(`[GET] /internal/node?id=${nodeId}&includeRelations=${includeRelation}`)
  return uimax
    .get(`/internal/node?id=${nodeId}&includeRelations=${includeRelation}`, {
      headers: {'X-Requestor': res.locals.userId}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.getNodesByLabelPath = function(nodeId, labels, res, next, callback, fallout) {
  log.debug(`[GET] /internal/nodesByLabelPath?id=${nodeId}&labels=${labels}`)
  return uimax
    .get(`/internal/nodesByLabelPath?id=${nodeId}&labels=${labels}`, {
      headers: {'X-Requestor': res.locals.userId}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.getNodeSelectedRelationship = function(nodeId, relationshipTypes, res, next, callback, fallout) {
  log.debug(`[GET] /internal/node?id=${nodeId}&includeRelations=true&relationshipTypes=${relationshipTypes}`)
  return uimax
    .get(`/internal/node?id=${nodeId}&includeRelations=true&relationshipTypes=${relationshipTypes}`, {
      headers: {'X-Requestor': res.locals.userId}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.updateNode = function(data, res, next, callback, fallout) {
  log.debug('[PUT] /internal/node')
  log.debug(data)
  return uimax
    .put('/internal/node', data, {headers: {'X-Requestor': res.locals.userId}})
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.deleteNode = function(nodeId, res, next, callback, fallout) {
  log.debug(`[DELETE] /internal/node?id=${nodeId}`)
  return uimax
    .delete(`/internal/node?id=${nodeId}`, {}, {headers: {'X-Requestor': res.locals.userId}})
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.deleteNodeWithEdges = function(data, res, next, callback, fallout) {
  log.debug('[DELETE] /internal/nodeWithRelations')
  log.debug(data)
  return uimax
    .delete('/internal/nodeWithRelations', {data: data, headers: {'X-Requestor': res.locals.userId}})
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.createRelationship = function(data, res, next, callback, fallout) {
  log.debug('[POST] /internal/relationship')
  log.debug(data)
  return uimax
    .post('/internal/relationship', data, {headers: {'X-Requestor': res.locals.userId}})
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.deleteRelationship = function(relationshipId, res, next, callback, fallout) {
  log.debug(`[DELETE] /internal/relationship?id=${relationshipId}`)
  return uimax
    .delete(`/internal/relationship?id=${relationshipId}`, {}, {headers: {'X-Requestor': res.locals.userId}})
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.deleteNodeProperties = function(nodeId, properties, res, next, callback, fallout) {
  let url = `/internal/node/property?id=${nodeId}`
  log.debug(`[DELETE] ${url}`)
  
  return uimax
    .delete(url, {
      data: properties,
      headers: {'X-Requestor': res.locals.userId}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.moveRelationship = function(data, res, next, callback, fallout) {
  let url = `/internal/relationship/move?id=${data.id}`
  if (data.endNodeId) url += `&endNodeId=${data.endNodeId}`
  if (data.startNodeId) url += `&startNodeId=${data.startNodeId}`

  log.debug(`[PUT] ${url}`)

  return uimax
    .put(url, data, {headers: {'X-Requestor': res.locals.userId}})
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.updateRelationship = function(data, res, next, callback, fallout) {
  log.debug('[PUT] /internal/relationship')
  log.debug(data)
  return uimax
    .put('/internal/relationship', data, {headers: {'X-Requestor': res.locals.userId}})
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.getResourceType = function(res, next, callback, fallout) {
  log.debug('[GET] /internal/resource/type')
  return uimax
    .get('/internal/resource/type', {headers: {'X-Requestor': res.locals.userId}})
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.checkCapacity = function(deviceName, portName, res, next, callback, fallout) {
  log.debug(
    `[GET] /device/checkCapacity?deviceName=${encodeURIComponent(deviceName)}&portName=${encodeURIComponent(portName)}`
  )
  return uimax
    .get(
      `/device/checkCapacity?deviceName=${encodeURIComponent(deviceName)}&portName=${encodeURIComponent(portName)}`,
      {headers: {'X-Requestor': res.locals.userId}}
    )
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.setSDP = function(data, res, next, callback, fallout) {
  log.debug('[PUT] /internal/resource/defaultSDP')
  log.debug(data)
  return uimax
    .put('/internal/resource/defaultSDP', data, {headers: {'X-Requestor': res.locals.userId}})
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.getCompleteConnection = function(data, res, next, callback, fallout) {
  log.debug(`[GET] /internal/connections/complete?${querystring.stringify(data)}`)
  return uimax
    .get(`/internal/connections/complete?${querystring.stringify(data)}`, {
      headers: {'X-Requestor': res.locals.userId}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.getHalfConnection = function(data, res, next, callback, fallout) {
  log.debug(`[GET] /internal/connections/half?${querystring.stringify(data)}`)
  return uimax
    .get(`/internal/connections/half?${querystring.stringify(data)}`, {
      headers: {'X-Requestor': res.locals.userId}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.getAnWacConnection = function(data, res, next, callback, fallout) {
  log.debug(`[GET] /internal/connections/wifi?${querystring.stringify(data)}`)
  return uimax
    .get(`/internal/connections/wifi?${querystring.stringify(data)}`, {
      headers: {'X-Requestor': res.locals.userId}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.deletePort = function(nodeId, res, next, callback, fallout) {
  log.debug(`[DELETE] /internal/port?id=${nodeId}`)
  return uimax
    .delete(`/internal/port?id=${nodeId}`, {}, {headers: {'X-Requestor': res.locals.userId}})
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.safeDeleteNode = function(nodeId, relationLabel, res, next, callback, fallout) {
  log.debug(`[DELETE] /internal/safeDeleteNode?id=${nodeId}&relationLabel=${relationLabel}`)
  return uimax
    .delete(`/internal/safeDeleteNode?id=${nodeId}&relationLabel=${relationLabel}`, {
      headers: {'X-Requestor': res.locals.userId}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.stoByPhoneNumber = function(phoneNumber, res, next, callback, fallout) {
  log.debug(`[GET] /internal/stoByPhoneNumber/${phoneNumber}`)
  return uimax
    .get(`/internal/stoByPhoneNumber/${phoneNumber}`, {headers: {'X-Requestor': res.locals.userId}})
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.vrfByDeviceId = function(deviceId, res, next, callback, fallout) {
  log.debug(`[GET] /internal/vrf/byDeviceId/${deviceId}`)
  return uimax
    .get(`/internal/vrf/byDeviceId/${deviceId}`, {headers: {'X-Requestor': res.locals.userId}})
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.setVrfOwner = function(vrfId, ownerId, res, next, callback, fallout) {
  log.debug(`[POST] /internal/vrf/${vrfId}/owner`)
  return uimax
    .post(`/internal/vrf/${vrfId}/owner`, {ownerId: ownerId}, {headers: {'X-Requestor': res.locals.userId}})
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.unsetVrfOwner = function(vrfId, res, next, callback, fallout) {
  log.debug(`[DELETE] /internal/vrf/${vrfId}/owner`)
  return uimax
    .delete(`/internal/vrf/${vrfId}/owner`, {headers: {'X-Requestor': res.locals.userId}})
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.createRt = function(data, res, next, callback, fallout) {
  log.debug('[POST] /internal/vrf/createRt')
  log.debug(data)
  return uimax
    .post('/internal/vrf/createRt', data, {headers: {'X-Requestor': res.locals.userId}})
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.deleteVrfDeviceAssoc = function(vrfName, deviceName, res, next, callback, fallout) {
  log.debug(`[DELETE] /internal/vrf/association?vrfName=${vrfName}&deviceName=${deviceName}`)
  return uimax
    .delete(`/internal/vrf/association?vrfName=${vrfName}&deviceName=${deviceName}`, {
      headers: {'X-Requestor': res.locals.userId}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.deleteVrfSubInterfaceAssoc = function(vrfName, subInterface, res, next, callback, fallout) {
  log.debug(`[DELETE] /internal/vrf/association?vrfName=${vrfName}&subInterface=${subInterface}`)
  return uimax
    .delete(`/internal/vrf/association?vrfName=${vrfName}&subInterface=${subInterface}`, {
      headers: {'X-Requestor': res.locals.userId}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}
