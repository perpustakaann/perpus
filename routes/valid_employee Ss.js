const router = require('express').Router()
const internalService = require('../lib/InternalService')

const dataStructure = [{
  name: 'valid_employee',
  label: 'validasi employee',
  link: 'view',
  filter: {
    type: 'text'
  },
  defaultSort: 'asc'
},
{
  name: 'verifi_Employeename',
  label: 'employee name',
  filter: {
    type: 'text'
  }
},
{
  name: 'tenant',
  label: 'Tenant',
  filter: {
    type: 'text'
  }
},
{
  name: 'createdAt',
  label: 'Created At',
  type: 'date'
},
{
  name: 'createdBy',
  label: 'Created By',
  filter: {
    type: 'text'
  }
}
]

router.get('/data', function (req, res, next) {
  const queries = req.query
  const reqData = {
    limit: 0,
    label: "Valid_employee",
    skip: queries.start,
    propertyFilter: {},
    propertySort: {}
  }

  if (queries.order) {
    queries.order.forEach(function (item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.name] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function (item) {
      if (item.search.value !== '' && item.name) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.name] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function (resData) {
      const data = []

      resData.nodes.forEach(function (item) {
        data.push({
          _id: item.node._id ? item.node._id : null,
          valid_employee: item.node.valid_employee ? item.node.valid_employee : null,
          verifi_Employeename: item.node.verifi_Employeename ? item.node.verifi_Employeename : null,
          tenant: item.node.tenant ? item.node.tenant : null,
          createdAt: item.node.createdAt ? item.node.createdAt : null,
          createdBy: item.node.createdBy ? item.node.createdBy : null,
          updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
          updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
          links: {
            view: {
              url: '/valid_employee/' + item.node._id,
              method: 'GET'
            }
          }
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }

      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function (err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }

      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})

router.get('/', function (req, res, next) {
  res.render('valid_employee/list', {
    title: `${serverConfig.server.name.toUpperCase()}`,
    user: res.locals.user,
    dataStructure: dataStructure,
    links: {
      data: {
        url: '/valid_employee/data',
        method: 'GET'
      }
    }
  })
})

router.post('/', function (req, res, next) {
  const reqData = {
    label: 'Valid_employee',
    properties: {
      employeeId: req.body.employeeId,
      verifi_Employeename: req.body.verifi_Employeename,
      tenant: req.body.tenant,
      status: 'ACTIVE'
    }
  }

  internalService.createNode(reqData, res, next, function (resData) {

    if (req.body.employeeId) {
      const relationData = {
        startNodeId: resData.node._id,
        endNodeId: req.body.employeeId,
        type: 'HAS_PARENT',
      }

      internalService.createRelationship(relationData, res, next, function () {
        res.status(200).send({
          links: {
            view: { url: '/valid_employee/' + resData.node._id, method: 'GET' }
          }
        })
      })
    } else {
      res.status(200).send({
        links: {
          view: { url: '/valid_employee/' + resData.node._id, method: 'GET' }
        }
      })
    }
  })
})


router.put('/:_id', function (req, res, next) {
  var _id = req.params._id
  var data = {
    label: 'Valid_employee',
    id: parseInt(_id),
    properties: req.body
  }

  internalService.updateNode(data, res, next, function () {
    res.status(200).send({
      links: {
        index: {
          url: '/valid_employee/' + _id,
          method: 'GET'
        }
      }
    })
  })
})

router.get('/:_id', function (req, res, next) {
  var id = req.params._id
  internalService.getNode(id, 'TRUE', res, next, function (resData) {

    const data = []

    var item = resData
    data.push({
      _id: item.node._id ? item.node._id : null,
      valid_employee: item.node.valid_employee ? item.node.valid_employee : null,
      verifi_Employeename: item.node.verifi_Employeename ? item.node.verifi_Employeename : null,
      tenant: item.node.tenant ? item.node.tenant : null,
      createdAt: item.node.createdAt ? item.node.createdAt : null,
      createdBy: item.node.createdBy ? item.node.createdBy : null,
      updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
      updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
      links: {
        delete: { url: '/valid_employee/detachDelete/' + item.node._id, method: 'DELETE' },
        edit: { url: '/valid_employee/' + item.node._id, method: 'PUT' },
        addEmployee: { url: '/valid_employee/addEmployee/', method: 'POST' },
      },
      employee: []
    })


    resData.relationships.forEach(function (itemRel) {
      //Check jika memiliki relasi
      if (itemRel.relationship != null) {
        //Ambil relasi dengan nama "HAS_PARENT"
        if (itemRel.relationship._type == "HAS_PARENT") {
          //Ambil node dengan label "Guest"
          if (itemRel.node._labels.includes("Employee")) {
            data[0].employee.push({
              _id: itemRel.node._id ? itemRel.node._id : null,
              //   objectName: item.objectOpt[0].name,
              employeeNum: itemRel.node.employeeNum ? itemRel.node.employeeNum : null,
              nik: itemRel.node.nik ? itemRel.node.nik : null,
              name: itemRel.node.name ? itemRel.node.name : null,
              gender: itemRel.node.gender ? itemRel.node.gender : null,
              bod: itemRel.node.bod ? itemRel.node.bod : null,
              email: itemRel.node.email ? itemRel.node.email : null,
              telp: itemRel.node.telp ? itemRel.node.telp : null,
              division: itemRel.node.division ? itemRel.node.division : null,
              job_title: itemRel.node.job_title ? itemRel.node.job_title : null,
              status: itemRel.node.status ? itemRel.node.status : null,
              createdAt: itemRel.node.createdAt ? itemRel.node.createdAt : null,
              createdBy: itemRel.node.createdBy ? itemRel.node.createdBy : null,
              updatedAt: itemRel.node.updatedAt ? itemRel.node.updatedAt : null,
              updatedBy: itemRel.node.updatedBy ? itemRel.node.updatedBy : null,
              links: {
                view: { url: '/employee/' + itemRel.node._id, method: 'DELETE' },
                delete: { url: '/valid_employee/employee/' + itemRel.node._id + '/' + itemRel.node._id, method: 'DELETE' },
              }
            })
          }
        }
      }
    })

    res.render('valid_employee/details', {
      user: res.locals.user,
      data: data[0]
    })
  }
  )
})

router.delete('/detachDelete/:_id', function (req, res, next) {
  const _id = req.params._id

  internalService.detachDelete(_id, res, next, function () {
    res.status(200).send({
      links: {
        index: {
          url: '/valid_employee/',
          method: 'GET'
        }
      }
    })
  })
})





// map handler




module.exports = router