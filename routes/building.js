const router = require('express').Router()
const internalService = require('../lib/InternalService')

const dataStructure = [
  {name: 'name', label: 'Name', link: 'view', filter: {type: 'text'}, defaultSort: 'asc'},
  {name: 'type', label: 'Type', filter: {type: 'text'}},
  {name: 'uid', label: 'Building ID', filter: {type: 'text'}},
  {name: 'floor', label: 'Floor', filter:{type: 'text'}},
  {name: 'height', label: 'Height (m)', filter:{type: 'text'}},
  {name: 'width', label: 'Width (m)', filter:{type: 'text'}},
  {name: 'length', label: 'Length (m)', filter:{type: 'text'}},
  {name: 'squareMeter', label: 'Square Meter (m²)', filter:{type: 'text'}},
  {name: 'builtDate', label: 'Built Date', type: 'date'},
  {name: 'createdAt', label: 'Created At', type: 'date'},
  {name: 'createdBy', label: 'Created By', filter: {type: 'text'}}
]

router.get('/data', function(req, res, next) {
  const queries = req.query
  const reqData = {
    limit: 0,
    label: 'Building',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {}
  }

  if (queries.order) {
    queries.order.forEach(function(item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.name] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function(item) {
      if (item.search.value !== '' && item.name) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.name] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function(resData) {
      const data = []

      resData.nodes.forEach(function(item) {
        data.push({
          _id: item.node._id ? item.node._id : null,
          name: item.node.name ? item.node.name : null,
          type: item.node.type ? item.node.type : null,
          uid: item.node.uid ? item.node.uid : null,
          height: item.node.height ? item.node.height : null,
          width: item.node.width ? item.node.width : null,
          length: item.node.length ? item.node.length : null,
          floor: item.node.floor ? item.node.floor : null,
          squareMeter: item.node.squareMeter ? item.node.squareMeter : null,
          builtDate: item.node.builtDate ? item.node.builtDate : null,
          status: item.node.status ? item.node.status: null,
          createdAt: item.node.createdAt ? item.node.createdAt : null,
          createdBy: item.node.createdBy ? item.node.createdBy : null,
          updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
          updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
          links: {
            view: {url: '/building/' + item.node._id, method: 'GET'}
          }
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }

      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function(err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }

      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})

router.get('/', function(req, res, next) {
  res.render('building/list', { title: `${serverConfig.server.name.toUpperCase()}`,
    user: res.locals.user,
    dataStructure: dataStructure,
    links: {
      data: {url: '/building/data', method: 'GET'}
    }
  })
})

router.post('/', function(req, res, next) {
  const reqData = {
    label: 'Building',
    properties: {
      name: req.body.name,
      type: req.body.type,
      uid: req.body.uid,
      height: req.body.height,
      width: req.body.width,
      length: req.body.length,
      squareMeter: req.body.squareMeter,
      floor: req.body.floor,
      builtDate: req.body.builtDate,
      status: 'ACTIVE'
    }
  }
  internalService.createNode(reqData, res, next, function(resData) {
    const paramsData = {
      startNodeId : resData.node._id,
      endNodeId : req.body.locationId,
      type : "LOCATED_AT"
    }
    internalService.createRelationship(paramsData, res, next, function(resData2){
      res.status(200).send({
        links: {
          view: {url: '/building/' + resData.node._id, method: 'GET'}
        }
      })
    })
  })
})

router.get('/generateFloor', function(req, res, next) {
  let request = {
    collection : 'IDC-FE' ,
    type : 'QUERY',
    name : 'GENERATE_FLOOR',
    body : {
      params : {
        buildingId : parseInt(req.query.buildingId),
        floorcount : parseInt(req.query.floorCount),
        createdBy : res.locals.user
      }
    }
  }
  internalService.predefinedQuery(request, res, next, function(resData) {
    res.status(200).send()
  })
})

router.put('/:_id', function(req, res, next) {
  var _id = req.params._id
  var data = {
    label: 'Building',
    id: parseInt(_id),
    properties: req.body
  }

  internalService.updateNode(data, res, next, function(){
    res.status(200).send({
      links: {
        index: {url: '/building/' + _id, method: 'GET'}
      }
    })
  })
})

router.get('/:_id', function(req, res, next) {
  var id = req.params._id

  internalService.getNodesByLabelPath(id , 'Building', '', 'Floor,Room,Rack', res, next, function (resData) {
    let result = {
      floor: [],
      room: [],
      rack: [],
      space: []
    }
    const records = resData
    if (records.length > 0) {
      records.forEach((i) => {
        result = i['000']
        result['links'] = {
          delete: {
            url: '/building/' + i['000']._id,
            method: 'DELETE'
          },
          edit: {
            url: '/building/' + i['000']._id,
            method: 'PUT'
          }
        }

        if (i['002']) {
          i['002']['links'] = {
            view: {
              url: '/floor/' + i['002']._id + '?parentId=' + id,
              method: 'GET'
            },
            delete: {
              url: '/floor/' + i['002']._id + '?parentId=' + id,
              method: 'DELETE'
            },
          }
          i['002']['size'] = `${i['004'].height}/${i['004'].width}`
          // Floor
          if (result.floor.length > 0) {
            result.floor.forEach((b) => {
              if (b._id !== i['002']._id) {
                result.floor.push(i['002'])
              }
            })
          } else {
            result.floor.push(i['002'])
          }
        }

        // Room
        if (i['004']) {
          i['004']['links'] = {
            view: {
              url: '/room/' + i['004']._id + '?parentId=' + id,
              method: 'GET'
            },
            delete: {
              url: '/room/' + i['004']._id + '?parentId=' + id,
              method: 'DELETE'
            },
          }
          i['004']['size'] = `${i['004'].height}/${i['004'].width}/${i['004'].length}`
          if (result.room.length > 0) {
            result.room.forEach((b) => {
              if (b._id !== i['004']._id) {
                result.room.push(i['004'])
              }
            })
          } else {
            result.room.push(i['004'])
          }
        }
        // Rack
        i['006']['links'] = {
          view: {url: '/rack/' + i['006']._id + '?parentId=' + id, method: 'GET'},
          delete: {url: '/rack/' + i['006']._id + '?parentId=' + id, method: 'DELETE'},
        }
        i['006']['size'] = `${i['006'].height}/${i['006'].width}/${i['006'].length}`
        // if(result.rack.length > 0){
        //   result.rack.forEach((b) => {
        //     if(b._id !== i['008']._id){
        //       result.rack.push(i['008'])
        //     }
        //   })
        // }else{
          result.rack.push(i['006'])
        // }
      })
      res.render('building/details', {
        title: `${serverConfig.server.name.toUpperCase()}`,
        user: res.locals.user,
        data: result
      })
    }
  }) 
  // internalService.getNode(id, 'TRUE', res, next, function(resData) {
  //   const data = []
  //   data.push({
  //     _id: resData.node._id ? resData.node._id : null,
  //     name: resData.node.name ? resData.node.name : null,
  //     type: resData.node.type ? resData.node.type : null,
  //     uid: resData.node.uid ? resData.node.uid : null,
  //     height: resData.node.height ? resData.node.height : null,
  //     width: resData.node.width ? resData.node.width : null,
  //     length: resData.node.length ? resData.node.length : null,
  //     area: resData.node.area ? resData.node.area : null,
  //     floor: resData.node.floor ? resData.node.floor : null,
  //     builtDate: resData.node.builtDate ? resData.node.builtDate : null,
  //     status: resData.node.status ? resData.node.status : null,
  //     createdAt: resData.node.createdAt ? resData.node.createdAt : null,
  //     createdBy: resData.node.createdBy ? resData.node.createdBy : null,
  //     updatedAt: resData.node.updatedAt ? resData.node.updatedAt : null,
  //     updatedBy: resData.node.updatedBy ? resData.node.updatedBy : null,
  //     links: {
  //       delete: {url: '/building/' + resData.node._id, method: 'DELETE'},
  //       edit: {url: '/building/' + resData.node._id, method: 'PUT'}
  //     },
  //     floorData: []
  //   })
    
  //   if(resData.relationships[0].relationship !== null){
  //     resData.relationships.forEach(function(item){
  //       if(item.node._labels.includes('Floor')){
  //         data[0].floorData.push({
  //           _id: item.node._id ? item.node._id : null,
  //           type: item.node.type ? item.node.type : null,
  //           name: item.node.name ? item.node.name : null,
  //           label: item.node.label ? item.node.label : null,
  //           level: item.node.level ? item.node.level   : null,
  //           uid: item.node.uid ? item.node.uid : null,
  //           height: item.node.height ? item.node.height : null,
  //           width: item.node.width ? item.node.width : null,
  //           length: item.node.length ? item.node.length : null,
  //           createdAt: item.node.createdAt ? item.node.createdAt : null,
  //           createdBy: item.node.createdBy ? item.node.createdBy : null,
  //           createdOn: item.node.createdOn ? item.node.createdOn : null,
  //           links: {
  //             view: {url: '/floor/' + item.node._id + '?parentId=' + resData.node._id, method: 'GET'},
  //             delete: {url: '/floor/'+ item.node._id + '?parentId=' + resData.node._id, method: 'DELETE'},
  //           }
  //         })
  //       }
  //     })
  //   }

  //   res.render('building/details', {
  //     title: `${serverConfig.server.name.toUpperCase()}`,
  //     user: res.locals.user,
  //     data: data[0]
  //   })    
  // }
  // )
})

router.delete('/:_id', function(req, res, next) {
  const _id = req.params._id

  internalService.deleteNode(_id, res, next, function() {
    res.status(200).send({
      links: {
        index: {
          url: '/building/',
          method: 'GET'
        }
      }
    })
  })
})

router.get('/:id/floor', function(req, res, next) {
  const id = req.params.id
  console.log("ID : "+id)
  const queries = req.query
  const reqData = {
    limit: 0,
    label: 'Floor',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {},
    relatedNodes: [
      {
        label: "Building",
        relationLabel: "PART_OF",
        // directionSensitive: true,
        includeRelationship: true,
        collection: true,
        propertyFilter: {
          _id:id
        }
      },
    ]
  }

  if (queries.order) {
    queries.order.forEach(function(item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.name] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function(item) {
      if (item.search.value !== '' && item.name) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.name] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function(resData) {
      const data = []

      resData.nodes.forEach(function(item) {
        data.push({
            _id: item.node._id ? item.node._id : null,
            name: item.node.name ? item.node.name : null
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }

      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function(err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }

      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})


module.exports = router
