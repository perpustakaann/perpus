// 1	id_rak
// 2	nama_rak
// 3	lokasi_rak
// 4	id_buku
const router = require('express').Router()
const internalService = require('../lib/InternalService')

const dataStructure = [
  {name: 'bookId', label: 'Id Books', link: 'view', filter: {type: 'text'}, defaultSort: 'asc'},
  {name: 'name', label: 'Nama Rak', link: 'view', filter: {type: 'text'}, defaultSort: 'asc'},
  {name: 'location', label: 'location Rak ', filter: {type: 'text'}},
  {name: 'createdAt', label: 'Created At', type: 'date'},
  {name: 'createdBy', label: 'Created By', filter: {type: 'text'}}
]

router.get('/data', function(req, res, next) {
  const queries = req.query
  const reqData = {
    limit: 0,
    label: 'Rak',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {}
  }

  if (queries.order) {
    queries.order.forEach(function(item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.name] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function(item) {
      if (item.search.value !== '' && item.name) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.name] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function(resData) {
      const data = []

      resData.nodes.forEach(function(item) {
        data.push({
          _id: item.node._id ? item.node._id : null,
          bookId: item.node.bookId ? item.node.bookId : null,
          name: item.node.name ? item.node.name : null,
          location: item.node.location ? item.node.location : null,
          createdAt: item.node.createdAt ? item.node.createdAt : null,
          createdBy: item.node.createdBy ? item.node.createdBy : null,
          updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
          updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
          links: {
            view: {url: '/rak/' + item.node._id, method: 'GET'}
          }
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }

      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function(err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }

      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})

router.get('/', function(req, res, next) {
  res.render('rak/list', {
    title: `${serverConfig.server.name.toUpperCase()}`,
    user: res.locals.user,
    dataStructure: dataStructure,
    links: {
      data: {url: '/rak/data', method: 'GET'}
    }
  })
})

router.post('/', function(req, res, next) {
  const reqData = {
    label: 'Rak',
    properties: {
      bookId: req.body.bookId,
      name: req.body.name,
      location: req.body.location
    }
  }

  internalService.createNode(reqData, res, next, function (resData) {

    if (req.body.bookId) {
      const relationData = {
        startNodeId: resData.node._id,
        endNodeId: req.body.bookId,
        type: 'HAS_PARENT',
      }

      internalService.createRelationship(relationData, res, next, function () {
        res.status(200).send({
          links: {
            view: { url: '/rak/' + resData.node._id, method: 'GET' }
          }
        })
      })
    } else {
      res.status(200).send({
        links: {
          view: { url: '/rak/' + resData.node._id, method: 'GET' }
        }
      })
    }
  })
})

router.put('/:_id', function(req, res, next) {
  var _id = req.params._id
  var data = {
    label: 'Rak',
    id: parseInt(_id),
    properties: req.body
  }

  internalService.updateNode(data, res, next, function(){
    res.status(200).send({
      links: {
        index: {url: '/rak/' + _id, method: 'GET'}
      }
    })
  })
})

router.get('/:_id', function(req, res, next) {
  var id = req.params._id
  internalService.getNode(id, 'TRUE', res, next, function(resData) {

    const data = []
    
    data.push({
      _id: resData.node._id ? resData.node._id : null,
      bookId: resData.node.bookId ? resData.node.bookId : null,
      name: resData.node.name ? resData.node.name : null,
      location: resData.node.location ? resData.node.location : null,
      createdAt: resData.node.createdAt ? resData.node.createdAt : null,
      createdBy: resData.node.createdBy ? resData.node.createdBy : null,
      updatedAt: resData.node.updatedAt ? resData.node.updatedAt : null,
      updatedBy: resData.node.updatedBy ? resData.node.updatedBy : null,
      links: {
        delete: {url: '/rak/' + resData.node._id, method: 'DELETE'},
        edit: {url: '/rak/' + resData.node._id, method: 'PUT'},
        addBuku: {url: '/rak/addBuku/', method: 'POST'},
      },
      buku: [],
    //   objectModel: [],
    //   objectRole: [],
    })

    resData.relationships.forEach(function(itemRelationship){
      if(itemRelationship.relationship != null){
        if(itemRelationship.relationship._type == "HAS_PARENT"){
          if(itemRelationship.node._labels.includes("Buku")){
            data[0].buku.push({
              _id: itemRelationship.node._id ? itemRelationship.node._id : null,
              id_buku: itemRelationship.node.id_buku ? itemRelationship.node.id_buku : null,
              book_code: itemRelationship.node.book_code ? itemRelationship.node.book_code : null,
              judul: itemRelationship.node.judul ? itemRelationship.node.judul : null,
              penulis: itemRelationship.node.penulis ? itemRelationship.node.penulis : null,
              penerbit: itemRelationship.node.penerbit ? itemRelationship.node.penerbit : null,
              terbit: itemRelationship.node.terbit ? itemRelationship.node.terbit : null,
              status: itemRelationship.node.status ? (itemRelationship.node.status == 'ACTIVE') : null,
              createdAt: itemRelationship.node.createdAt ? itemRelationship.node.createdAt : null,
              createdBy: itemRelationship.node.createdBy ? itemRelationship.node.createdBy : null,
              updatedAt: itemRelationship.node.updatedAt ? itemRelationship.node.updatedAt : null,
              updatedBy: itemRelationship.node.updatedBy ? itemRelationship.node.updatedBy : null,
              links: {
                view: {url: '/buku/' + itemRelationship.node._id, method: 'DELETE'},
                delete: {url: '/rak/buku/' + resData.node._id + '/' + itemRelationship.node._id, method: 'DELETE'},
              }
            })
          }

        }
        
      }

    })

    res.render('rak/details', {
      title: `${serverConfig.server.name.toUpperCase()}`,
      user: res.locals.user,
      data: data[0]
    })
  }
  )
})

router.delete('/:_id', function(req, res, next) {
  const _id = req.params._id

  internalService.deleteNode(_id, res, next, function() {
    res.status(200).send({
      links: {
        index: {
          url: '/rak/',
          method: 'GET'
        }
      }
    })
  })
})

module.exports = router
