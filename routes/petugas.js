const router = require('express').Router()
const internalService = require('../lib/InternalService')

const dataStructure = [
  { name: 'kd_petugas', label: 'Code Petugas', link: 'view', filter: { type: 'text' }, defaultSort: 'asc' },
  { name: 'nama_petugas', label: 'Nama Petugas', link: 'view', filter: { type: 'text' }, defaultSort: 'asc' },
  { name: 'jabatan_petugas', label: 'Jabatan Petugas', filter: { type: 'text' } },
  { name: 'no_telp_petugas', label: 'No. Telepon', filter: { type: 'text' } },
  { name: 'alamat_petugas', label: 'Alamat', filter: { type: 'text' } },
  { name: 'status', label: 'Status', type: 'boolean' },
  { name: 'createdAt', label: 'Created At', type: 'date' },
  { name: 'createdBy', label: 'Created By', filter: { type: 'text' } }
]

router.get('/data', function (req, res, next) {
  const queries = req.query
  const reqData = {
    limit: 0,
    label: 'Petugas',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {}
  }

  if (queries.order) {
    queries.order.forEach(function (item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.kd_petugas] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function (item) {
      if (item.search.value !== '' && item.kd_petugas) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.kd_petugas] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function (resData) {
      const data = []

      resData.nodes.forEach(function (item) {
        data.push({
          _id: item.node._id ? item.node._id : null,
          kd_petugas: item.node.kd_petugas ? item.node.kd_petugas : null,
          nama_petugas: item.node.nama_petugas ? item.node.nama_petugas : null,
          jabatan_petugas: item.node.jabatan_petugas ? item.node.jabatan_petugas : null,
          no_telp_petugas: item.node.no_telp_petugas ? item.node.no_telp_petugas : null,
          alamat_petugas: item.node.alamat_petugas ? item.node.alamat_petugas : null,
          status: item.node.status ? item.node.status : null,
          createdAt: item.node.createdAt ? item.node.createdAt : null,
          createdBy: item.node.createdBy ? item.node.createdBy : null,
          updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
          updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
          links: {
            view: { url: '/petugas/' + item.node._id, method: 'GET' }
          }
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }

      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function (err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }

      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})

router.get('/', function (req, res, next) {
  res.render('petugas/list', {
    user: res.locals.user,
    dataStructure: dataStructure,
    links: {
      data: { url: '/petugas/data', method: 'GET' }
    }
  })
})

router.post('/', function (req, res, next) {
  const reqData = {
    label: 'Petugas',
    properties: {
      kd_petugas: req.body.kd_petugas,
      nama_petugas: req.body.nama_petugas,
      jabatan_petugas: req.body.jabatan_petugas,
      no_telp_petugas: req.body.no_telp_petugas,
      alamat_petugas: req.body.alamat_petugas,
      status: 'ACTIVE'
    }
  }

  internalService.createNode(reqData, res, next, function (resData) {
    if (req.body.nugasId) {
      const relationData = {
        startNodeId: resData.node._id,
        endNodeId: req.body.nugasId,
        type: 'HAS_PARENT'
      }

      internalService.createRelationship(relationData, res, next, function () {
        res.status(200).send({
          links: {
            view: { url: '/petugas/' + resData.node._id, method: 'GET' }
          }
        })
      })
    } else {
      res.status(200).send({
        links: {
          view: { url: '/petugas/' + resData.node._id, method: 'GET' }
        }
      })
    }
  })
})
router.put('/:_id', function (req, res, next) {
  var _id = req.params._id
  var data = {
    label: 'Petugas',
    id: parseInt(_id),
    properties: req.body
  }

  internalService.updateNode(data, res, next, function () {
    res.status(200).send({
      links: {
        index: { url: '/petugas/' + _id, method: 'GET' }
      }
    })
  })
})

router.get('/:_id', function (req, res, next) {
  var id = req.params._id
  internalService.getNode(id, 'TRUE', res, next, function (resData) {
    const data = []
    var item = resData

    data.push({
      kd_petugas: item.node.kd_petugas ? item.node.kd_petugas : null,
      nama_petugas: item.node.nama_petugas ? item.node.nama_petugas : null,
      jabatan_petugas: item.node.jabatan_petugas ? item.node.jabatan_petugas : null,
      no_telp_petugas: item.node.no_telp_petugas ? item.node.no_telp_petugas : null,
      alamat_petugas: item.node.alamat_petugas ? item.node.alamat_petugas : null,
      status: item.node.status ? item.node.status : null,
      createdAt: item.node.createdAt ? item.node.createdAt : null,
      createdBy: item.node.createdBy ? item.node.createdBy : null,
      updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
      updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
      links: {
        delete: { url: '/petugas/deleteNode/' + item.node._id, method: 'DELETE' },
        edit: { url: '/petugas/' + item.node._id, method: 'PUT' }
      }
    })

    res.render('petugas/details', {
      user: res.locals.user,
      data: data[0]
    })
  }
  )
})

router.delete('/deleteNode/:_id', function (req, res, next) {
  const _id = req.params._id

  internalService.detachDelete(_id, res, next, function () {
    res.status(200).send({
      links: {
        index: {
          url: '/petugas/',
          method: 'GET'
        }
      }
    })
  })
})

module.exports = router
