// 1	id_peminjaman
// 2	tanggal_pinjam	        (date automatic)
// 3	tanggal_kembali	        (date)
// 4	id_buku
// 5	id_anggota
// 6	id_petugas

const router = require('express').Router()
const internalService = require('../lib/InternalService')

const dataStructure = [
  { name: 'bukuId', label: 'Id Books', link: 'view', filter: { type: 'text' }, defaultSort: 'asc' },
  { name: 'petugasId', label: 'Id Petugas', link: 'view', filter: { type: 'text' }, defaultSort: 'asc' },
  { name: 'memId', label: 'Id Anggota', link: 'view', filter: { type: 'text' }, defaultSort: 'asc' },
  { name: 'id_peminjaman', label: 'ID Pinjam ', link: 'view', filter: { type: 'text' }, defaultSort: 'asc' },
  { name: 'tgl_pinjam', label: 'Tanggal Pinjam', filter: { type: 'text' } },
  { name: 'tgl_kembali', label: 'Tanggal Kembali', filter: { type: 'text' } },
  { name: 'createdAt', label: 'Created At', type: 'date' },
  { name: 'createdBy', label: 'Created By', filter: { type: 'text' } }
]

router.get('/data', function (req, res, next) {
  const queries = req.query
  const reqData = {
    limit: 0,
    label: 'Peminjaman',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {}
  }

  if (queries.order) {
    queries.order.forEach(function (item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.id_peminjaman] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function (item) {
      if (item.search.value !== '' && item.id_peminjaman) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.id_peminjaman] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function (resData) {
      const data = []

      resData.nodes.forEach(function (item) {
        data.push({
          _id: item.node._id ? item.node._id : null,
          bukuId: item.node.bukuId ? item.node.bukuId : null,
          petugasId: item.node.petugasId ? item.node.petugasId : null,
          memId: item.node.memId ? item.node.memId : null,
          id_peminjaman: item.node.id_peminjaman ? item.node.id_peminjaman : null,
          tgl_pinjam: item.node.tgl_pinjam ? item.node.tgl_pinjam : null,
          tgl_kembali: item.node.tgl_kembali ? item.node.tgl_kembali : null,
          createdAt: item.node.createdAt ? item.node.createdAt : null,
          createdBy: item.node.createdBy ? item.node.createdBy : null,
          updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
          updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
          links: {
            view: { url: '/peminjaman/' + item.node._id, method: 'GET' }
          }
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }

      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function (err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }

      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})

router.get('/', function (req, res, next) {
  res.render('peminjaman/list', {
    title: `${serverConfig.server.name.toUpperCase()}`,
    user: res.locals.user,
    dataStructure: dataStructure,
    links: {
      data: { url: '/peminjaman/data', method: 'GET' }
    }
  })
})

router.post('/', function (req, res, next) {
  const reqData = {
    label: 'Peminjaman',
    properties: {
      bukuId: req.body.bukuId,
      petugasId: req.body.petugasId,
      memId: req.body.memId,
      id_peminjaman: req.body.id_peminjaman,
      tgl_pinjam: req.body.tgl_pinjam,
      tgl_kembali: req.body.tgl_kembali
    }
  }

  internalService.createNode(reqData, res, next, function (resData) {
    if (req.body.bukuId) {
      const relationData = {
        startNodeId: resData.node._id,
        endNodeId: req.body.bukuId,
        type: 'HAS_PARENT'
      }

      internalService.createRelationship(relationData, res, next, function () {
        res.status(200).send({
          links: {
            view: { url: '/peminjaman/' + resData.node._id, method: 'GET' }
          }
        })
      })
    } else if (req.body.memId) {
      const relationData = {
        startNodeId: resData.node._id,
        endNodeId: req.body.memId,
        type: 'HAS_PARENT'
      }

      internalService.createRelationship(relationData, res, next, function () {
        res.status(200).send({
          links: {
            view: { url: '/peminjaman/' + resData.node._id, method: 'GET' }
          }
        })
      })
    } else if (req.body.petugasId) {
      const relationData = {
        startNodeId: resData.node._id,
        endNodeId: req.body.petugasId,
        type: 'HAS_PARENT'
      }

      internalService.createRelationship(relationData, res, next, function () {
        res.status(200).send({
          links: {
            view: { url: '/peminjaman/' + resData.node._id, method: 'GET' }
          }
        })
      })
    } else {
      res.status(200).send({
        links: {
          view: { url: '/peminjaman/' + resData.node._id, method: 'GET' }
        }
      })
    }
  })
})

router.put('/:_id', function (req, res, next) {
  var _id = req.params._id
  var data = {
    label: 'Peminjaman',
    id: parseInt(_id),
    properties: req.body
  }

  internalService.updateNode(data, res, next, function () {
    res.status(200).send({
      links: {
        index: { url: '/peminjaman/' + _id, method: 'GET' }
      }
    })
  })
})

router.get('/:_id', function (req, res, next) {
  var id = req.params._id
  internalService.getNode(id, 'TRUE', res, next, function (resData) {
    const data = []

    data.push({
      _id: resData.node._id ? resData.node._id : null,
      bukuId: resData.node.bukuId ? resData.node.bukuId : null,
      petugasId: resData.node.petugasId ? resData.node.petugasId : null,
      memId: resData.node.memId ? resData.node.memId : null,
      id_peminjaman: resData.node.id_peminjaman ? resData.node.id_peminjaman : null,
      tgl_pinjam: resData.node.tgl_pinjam ? resData.node.tgl_pinjam : null,
      tgl_kembali: resData.node.tgl_kembali ? resData.node.tgl_kembali : null,
      createdAt: resData.node.createdAt ? resData.node.createdAt : null,
      createdBy: resData.node.createdBy ? resData.node.createdBy : null,
      updatedAt: resData.node.updatedAt ? resData.node.updatedAt : null,
      updatedBy: resData.node.updatedBy ? resData.node.updatedBy : null,
      links: {
        delete: { url: '/peminjaman/' + resData.node._id, method: 'DELETE' },
        edit: { url: '/peminjaman/' + resData.node._id, method: 'PUT' },
        addBuku: { url: '/peminjaman/addBuku/', method: 'POST' },
        addPetugas: { url: '/peminjaman/addPetugas/', method: 'POST' },
        addAnggota: { url: '/peminjaman/addAnggota/', method: 'POST' }
        // addPetugas: { url: '/peminjaman/addPetugas/', method: 'POST' }
      },
      buku: [],
      anggota: [],
      petugas: []
    })

    resData.relationships.forEach(function (itemRelationship) {
      if (itemRelationship.relationship != null) {
        if (itemRelationship.relationship._type == 'HAS_PARENT') {
          if (itemRelationship.node._labels.includes('Buku')) {
            data[0].buku.push({
              _id: itemRelationship.node._id ? itemRelationship.node._id : null,
              id_buku: itemRelationship.node.id_buku ? itemRelationship.node.id_buku : null,
              book_code: itemRelationship.node.book_code ? itemRelationship.node.book_code : null,
              judul: itemRelationship.node.judul ? itemRelationship.node.judul : null,
              penulis: itemRelationship.node.penulis ? itemRelationship.node.penulis : null,
              penerbit: itemRelationship.node.penerbit ? itemRelationship.node.penerbit : null,
              terbit: itemRelationship.node.terbit ? itemRelationship.node.terbit : null,
              status: itemRelationship.node.status ? (itemRelationship.node.status == 'ACTIVE') : null,
              createdAt: itemRelationship.node.createdAt ? itemRelationship.node.createdAt : null,
              createdBy: itemRelationship.node.createdBy ? itemRelationship.node.createdBy : null,
              updatedAt: itemRelationship.node.updatedAt ? itemRelationship.node.updatedAt : null,
              updatedBy: itemRelationship.node.updatedBy ? itemRelationship.node.updatedBy : null,
              links: {
                view: { url: '/buku/' + itemRelationship.node._id, method: 'DELETE' },
                delete: { url: '/peminjaman/buku/' + resData.node._id + '/' + itemRelationship.node._id, method: 'DELETE' }
              }
            })
          }
        }
      }
    })
    resData.relationships.forEach(function (itemRelationship) {
      if (itemRelationship.relationship != null) {
        if (itemRelationship.relationship._type == 'HAS_PARENT') {
          if (itemRelationship.node._labels.includes('Anggota')) {
            data[0].anggota.push({
              _id: itemRelationship.node._id ? itemRelationship.node._id : null,
              kd_anggota: itemRelationship.node.kd_anggota ? itemRelationship.node.kd_anggota : null,
              name: itemRelationship.node.name ? itemRelationship.node.name : null,
              gender: itemRelationship.node.gender ? itemRelationship.node.gender : null,
              bod: itemRelationship.node.bod ? itemRelationship.node.bod : null,
              address: itemRelationship.node.address ? itemRelationship.node.address : null,
              email: itemRelationship.node.email ? itemRelationship.node.email : null,
              telp: itemRelationship.node.telp ? itemRelationship.node.telp : null,
              status: itemRelationship.node.status ? (itemRelationship.node.status == 'ACTIVE') : null,
              createdAt: itemRelationship.node.createdAt ? itemRelationship.node.createdAt : null,
              createdBy: itemRelationship.node.createdBy ? itemRelationship.node.createdBy : null,
              updatedAt: itemRelationship.node.updatedAt ? itemRelationship.node.updatedAt : null,
              updatedBy: itemRelationship.node.updatedBy ? itemRelationship.node.updatedBy : null,
              links: {
                view: { url: '/anggota/' + itemRelationship.node._id, method: 'DELETE' },
                delete: { url: '/peminjaman/anggota/' + resData.node._id + '/' + itemRelationship.node._id, method: 'DELETE' }
              }
            })
          }
        }
      }
    })
    resData.relationships.forEach(function (itemRelationship) {
      if (itemRelationship.relationship != null) {
        if (itemRelationship.relationship._type == 'HAS_PARENT') {
          if (itemRelationship.node._labels.includes('Petugas')) {
            data[0].petugas.push({
              _id: itemRelationship.node._id ? itemRelationship.node._id : null,
              kd_petugas: itemRelationship.node.kd_petugas ? itemRelationship.node.kd_petugas : null,
              nama_petugas: itemRelationship.node.nama_petugas ? itemRelationship.node.nama_petugas : null,
              jabatan_petugas: itemRelationship.node.jabatan_petugas ? itemRelationship.node.jabatan_petugas : null,
              no_telp_petugas: itemRelationship.node.no_telp_petugas ? itemRelationship.node.no_telp_petugas : null,
              alamat_petugas: itemRelationship.node.alamat_petugas ? itemRelationship.node.alamat_petugas : null,
              status: itemRelationship.node.status ? (itemRelationship.node.status == 'ACTIVE') : null,
              createdAt: itemRelationship.node.createdAt ? itemRelationship.node.createdAt : null,
              createdBy: itemRelationship.node.createdBy ? itemRelationship.node.createdBy : null,
              updatedAt: itemRelationship.node.updatedAt ? itemRelationship.node.updatedAt : null,
              updatedBy: itemRelationship.node.updatedBy ? itemRelationship.node.updatedBy : null,
              links: {
                view: { url: '/petugas/' + itemRelationship.node._id, method: 'DELETE' },
                delete: { url: '/peminjaman/petugas/' + resData.node._id + '/' + itemRelationship.node._id, method: 'DELETE' }
              }
            })
          }
        }
      }
    })
    res.render('peminjaman/details', {
      title: `${serverConfig.server.name.toUpperCase()}`,
      user: res.locals.user,
      data: data[0]
    })
  }
  )
})
router.delete('/:_id', function (req, res, next) {
  const _id = req.params._id

  internalService.deleteNode(_id, res, next, function () {
    res.status(200).send({
      links: {
        index: {
          url: '/peminjaman/',
          method: 'GET'
        }
      }
    })
  })
})

module.exports = router
