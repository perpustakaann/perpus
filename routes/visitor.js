const router = require('express').Router()
const internalService = require('../lib/InternalService')

const dataStructure = [
  { name: 'visitnum', label: 'Visitor Number', filter: { type: 'text' }, defaultSort: 'asc' },
  { name: 'name', label: 'Name', link: 'view', filter: { type: 'text' }, defaultSort: 'asc' },
  { name: 'gender', label: 'Gender', filter: { type: 'text' } },
  { name: 'address', label: 'Address', filter: { type: 'text' } },
  { name: 'email', label: 'Email', filter: { type: 'text' } },
  { name: 'telp', label: 'Phone', filter: { type: 'text' } },
  { name: 'institution', label: 'Institution', filter: { type: 'text' } },
  { name: 'jobtitle', label: 'Job Title', filter: { type: 'text' } },
  { name: 'meet_with', label: 'Meet With', filter: { type: 'text' } },
  { name: 'floor', label: 'Floor', filter: { type: 'text' } },
  { name: 'purpose', label: 'Purpose', filter: { type: 'text' } },
  { name: 'status', label: 'Status', type: 'boolean' },
  { name: 'createdAt', label: 'Created At', type: 'date' },
  { name: 'createdBy', label: 'Created By', filter: { type: 'text' } }
]

router.get('/data', function (req, res, next) {
  const queries = req.query
  const reqData = {
    limit: 0,
    label: 'Visitor',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {},
    optionalNodes: [
      {
        label: "validasi_visit",
        relationLabel: "HAS_PARENT",
        // directionSensitive: true,
        includeRelationship: true,
        collection: true
      },
    ]
  }

  if (queries.order) {
    queries.order.forEach(function (item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.name] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function (item) {
      if (item.search.value !== '' && item.name) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.name] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function (resData) {
      const data = []

      resData.nodes.forEach(function (item) {

        data.push({
          _id: item.node._id ? item.node._id : null,
          //   objectName: item.objectOpt[0].name,
          visitnum: item.node.visitnum ? item.node.visitnum : null,
          name: item.node.name ? item.node.name : null,
          gender: item.node.gender ? item.node.gender : null,
          address: item.node.address ? item.node.address : null,
          email: item.node.email ? item.node.email : null,
          telp: item.node.telp ? item.node.telp : null,
          institution: item.node.institution ? item.node.institution : null,
          jobtitle: item.node.jobtitle ? item.node.jobtitle : null,
          meet_with: item.node.meet_with ? item.node.meet_with : null,
          floor: item.node.floor ? item.node.floor : null,
          purpose: item.node.purpose ? item.node.purpose : null,
          status: item.node.status ? item.node.status : null,
          createdAt: item.node.createdAt ? item.node.createdAt : null,
          createdBy: item.node.createdBy ? item.node.createdBy : null,
          updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
          updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
          links: {
            view: { url: '/visitor/' + item.node._id, method: 'GET' },
            // viewObject: {url: '/object/' + item.objectOpt[0]._id, method: 'GET'},
          }
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }

      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function (err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }

      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})

router.get('/', function (req, res, next) {
  res.render('visitor/list', {
    title: `${serverConfig.server.name.toUpperCase()}`,
    user: res.locals.user,
    dataStructure: dataStructure,
    links: {
      data: { url: '/visitor/data', method: 'GET' }
    }
  })
})

router.post('/', function (req, res, next) {
  const reqData = {
    label: 'Visitor',
    properties: {
      visitnum: req.body.visitnum,
      name: req.body.name,
      gender: req.body.gender,
      address: req.body.address,
      email: req.body.email,
      telp: req.body.telp,
      institution: req.body.institution,
      jobtitle: req.body.jobtitle,
      meet_with: req.body.meet_with,
      floor: req.body.floor,
      purpose: req.body.purpose,
      status: 'REGISTER'
    }
  }

  internalService.createNode(reqData, res, next, function (resData) {

    if (req.body.valID) {
      const relationData = {
        startNodeId: resData.node._id,
        endNodeId: req.body.valID,
        type: 'HAS_PARENT',
      }

      internalService.createRelationship(relationData, res, next, function () {
        res.status(200).send({
          links: {
            view: { url: '/visitor/' + resData.node._id, method: 'GET' }
          }
        })
      })
    } else {
      res.status(200).send({
        links: {
          view: { url: '/visitor/' + resData.node._id, method: 'GET' }
        }
      })
    }
  })
})

router.put('/:_id', function (req, res, next) {
  var _id = req.params._id
  var data = {
    label: 'Visitor',
    id: parseInt(_id),
    properties: req.body
  }

  internalService.updateNode(data, res, next, function () {
    res.status(200).send({
      links: {
        index: { url: '/visitor/' + _id, method: 'GET' }
      }
    })
  })
})

router.get('/:_id', function (req, res, next) {
  var id = req.params._id
  internalService.getNode(id, 'TRUE', res, next, function (resData) {

    const data = []
    var item = resData

    data.push({
      visitnum: item.node.visitnum ? item.node.visitnum : null,
      name: item.node.name ? item.node.name : null,
      gender: item.node.gender ? item.node.gender : null,
      address: item.node.address ? item.node.address : null,
      email: item.node.email ? item.node.email : null,
      telp: item.node.telp ? item.node.telp : null,
      institution: item.node.institution ? item.node.institution : null,
      jobtitle: item.node.jobtitle ? item.node.jobtitle : null,
      meet_with: item.node.meet_with ? item.node.meet_with : null,
      floor: item.node.floor ? item.node.floor : null,
      purpose: item.node.purpose ? item.node.purpose : null,
      status: item.node.status ? item.node.status : null,
      createdAt: item.node.createdAt ? item.node.createdAt : null,
      createdBy: item.node.createdBy ? item.node.createdBy : null,
      updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
      updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
      links: {
        delete: { url: '/visitor/' + item.node._id, method: 'DELETE' },
        edit: { url: '/visitor/' + item.node._id, method: 'PUT' }
      }
    })

    res.render('visitor/details', {
      title: `${serverConfig.server.name.toUpperCase()}`,
      user: res.locals.user,
      data: data[0]
    })
  }
  )
})

router.delete('/:_id', function (req, res, next) {
  const _id = req.params._id

  internalService.deleteNode(_id, res, next, function () {
    res.status(200).send({
      links: {
        index: {
          url: '/visitor/',
          method: 'GET'
        }
      }
    })
  })
})

module.exports = router
