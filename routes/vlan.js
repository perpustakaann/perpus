const express = require('express')
const router = express.Router()

const security = require('../lib/Security')
const internalService = require('../lib/InternalService')
const utils = require('../lib/Utils')

let _updateVLAN = (deviceId, vlanId, clientRequest, clientResponse, next) => {
  security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    if (Object.keys(clientRequest.body).includes('mtu')) clientRequest.body.mtu = parseInt(clientRequest.body.mtu)

    if (Object.keys(clientRequest.body).includes('number'))
      clientRequest.body.number = parseInt(clientRequest.body.number)

    if (Object.keys(clientRequest.body).includes('netmask'))
      clientRequest.body.netmask = parseInt(clientRequest.body.netmask)

    if (Object.keys(clientRequest.body).includes('translation'))
      clientRequest.body.translation = parseInt(clientRequest.body.translation)

    const requestData = {
      label: 'VLAN',
      id: vlanId,
      properties: clientRequest.body
    }

    internalService.updateNode(requestData, clientResponse, next, function() {
      clientResponse.status(204).send()
    })
  })
}

//
// WAC VLAN (via Module)
//

// GET list of NA VLAN
router.get('/:deviceId/naModuleVLAN', function(clientRequest, clientResponse, next) {
  const deviceId = clientRequest.params.deviceId

  security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    const queries = clientRequest.query
    const requestData = {
      limit: 0,
      label: 'Module',
      propertyFilter: {
        name: 'NA'
      },
      relatedNodes: [
        {
          includeRelationship: false,
          label: 'Resource',
          propertyFilter: {
            _id: deviceId
          }
        }
      ],
      optionalNodes: [
        {
          includeRelationship: false,
          label: 'VLAN'
        }
      ],
      propertySort: {
        name: 'asc'
      }
    }

    internalService.listNodes(
      requestData,
      clientResponse,
      next,
      function(responseData) {
        const data = []
        const result = {
          data: data
        }

        responseData.nodes.forEach(function(item) {
          let module = item.node
          let device = item.resource
          const entry = {
            id: item.vlanOpt._id,
            name: item.vlanOpt.name,
            encapsulationType: item.vlanOpt.encapsulationType,
            intervlanName: item.vlanOpt.intervlanName,
            mode: item.vlanOpt.mode,
            mtu: item.vlanOpt.mtu,
            netmask: item.vlanOpt.netmask,
            networkId: item.vlanOpt.networkId,
            number: item.vlanOpt.number,
            purpose: item.vlanOpt.purpose,
            status: item.vlanOpt.status,
            translation: item.vlanOpt.translation,
            type: item.vlanOpt.type,
            vcid: item.vlanOpt.vcid,
            createdAt: utils.formatDate(item.vlanOpt.createdAt),
            createdBy: item.vlanOpt.createdBy + (item.vlanOpt.source ? ' (' + item.vlanOpt.source + ')' : ''),
            links: {
              view: {
                url: `/device/${device._id}/module/${module._id}/vlan/${item.vlanOpt._id}`,
                method: 'GET'
              }
            }
          }

          data.push(entry)
        })

        clientResponse.send(result)
      },
      function(err) {
        const result = {
          draw: queries.draw,
          data: [],
          recordsFiltered: 0,
          recordsTotal: 0
        }

        if (err.response && err.response.status !== 404) {
          result.error = err.data
        }

        clientResponse.status(200).send(result)
      }
    )
  })
})

// GET list of VLAN
router.get('/:deviceId/module/:moduleId/vlan', function(clientRequest, clientResponse, next) {
  const deviceId = clientRequest.params.deviceId
  const moduleId = clientRequest.params.moduleId

  security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    const queries = clientRequest.query
    const requestData = {
      limit: 0,
      label: 'VLAN',
      relatedNodes: [
        {
          includeRelationship: false,
          label: 'Module',
          propertyFilter: {
            _id: moduleId
          }
        }
      ],
      optionalNodes: [
        {
          includeRelationship: false,
          label: 'Manufacturer'
        },
        {
          includeRelationship: true,
          label: 'ServiceType'
        }
      ],
      propertySort: {
        name: 'asc'
      }
    }

    internalService.listNodes(
      requestData,
      clientResponse,
      next,
      function(responseData) {
        const data = []
        const result = {
          data: data
        }

        responseData.nodes.forEach(function(item) {
          const entry = {
            id: item.node._id,
            name: item.node.name,
            encapsulationType: item.node.encapsulationType,
            intervlanName: item.node.intervlanName,
            mode: item.node.mode,
            mtu: item.node.mtu,
            netmask: item.node.netmask,
            networkId: item.node.networkId,
            number: item.node.number,
            purpose: item.node.purpose,
            status: item.node.status,
            translation: item.node.translation,
            type: item.node.type,
            vcid: item.node.vcid,
            createdAt: utils.formatDate(item.node.createdAt),
            createdBy: item.node.createdBy + (item.node.source ? ' (' + item.node.source + ')' : ''),
            links: {
              view: {
                url: '/device/' + deviceId + '/module/' + moduleId + '/vlan/' + item.node._id,
                method: 'GET'
              }
            }
          }

          data.push(entry)
        })

        clientResponse.send(result)
      },
      function(err) {
        const result = {
          draw: queries.draw,
          data: [],
          recordsFiltered: 0,
          recordsTotal: 0
        }

        if (err.response && err.response.status !== 404) {
          result.error = err.data
        }

        clientResponse.status(200).send(result)
      }
    )
  })
})

// GET list of interface VLAN data (name + id map)
router.get('/:deviceId/interfaceVLANData', function(clientRequest, clientResponse, next) {
  const deviceId = clientRequest.params.deviceId
  const moduleId = clientRequest.params.moduleId

  security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    const queries = clientRequest.query
    const requestData = {
      limit: 0,
      label: 'Module',
      propertyFilter: {
        name: 'NA'
      },
      relatedNodes: [
        {
          includeRelationship: false,
          label: 'Resource',
          propertyFilter: {
            _id: deviceId
          }
        }
      ],
      optionalNodes: [
        {
          includeRelationship: false,
          label: 'VLAN'
        }
      ]
    }

    internalService.listNodes(requestData, clientResponse, next, function(responseData) {
      const data = []

      responseData.nodes.forEach(function(item) {
        data.push({
          id: item.vlanOpt._id,
          name: item.vlanOpt.name
        })
      })
      data.sort(function(a, b) {
        return parseInt(a.name) > parseInt(b.name)
      })

      clientResponse.send(data)
    })
  })
})

// GET VLAN details
router.get('/:deviceId/module/:moduleId/vlan/:vlanId', function(clientRequest, clientResponse, next) {
  const deviceId = clientRequest.params.deviceId
  const moduleId = clientRequest.params.moduleId
  const vlanId = clientRequest.params.vlanId

  security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    internalService.getNodesByLabelPath(vlanId, 'Module,Resource', clientResponse, next, function(responseData) {
      const vlan = responseData[0]['000']
      const module = responseData[0]['002']
      module.links = {
        view: {url: `/device/${deviceId}/module/${moduleId}`, method: 'GET'}
      }
      const device = responseData[0]['004']
      device.links = {
        view: {url: `/device/${deviceId}`, method: 'GET'}
      }

      internalService.getNode(vlanId, true, clientResponse, next, function(responseData) {
        const vlan = responseData.node
        const manr = responseData.manufacturerOpt
        const sert = responseData.servicetypeOpt

        const data = {
          id: vlan._id,
          name: vlan.name,
          encapsulationType: vlan.encapsulationType,
          intervlanName: vlan.intervlanName,
          mode: vlan.mode,
          mtu: vlan.mtu,
          netmask: vlan.netmask,
          networkId: vlan.networkId,
          number: vlan.number,
          purpose: vlan.purpose,
          status: vlan.status,
          translation: vlan.translation,
          type: vlan.type,
          createdAt: utils.formatDate(vlan.createdAt),
          createdBy: vlan.createdBy + (vlan.source ? ' (' + vlan.source + ')' : ''),
          manufacture: manr ? manr.name : null,
          services: [],
          others: [],
          links: {
            delete: {url: `/device/${deviceId}/module/${moduleId}/vlan/${vlanId}`, method: 'DELETE'},
            edit: {url: `/device/${deviceId}/module/${moduleId}/vlan/${vlanId}`, method: 'PUT'},
            device: {url: `/device/${deviceId}`, method: 'GET'},
            module: {url: `/device/${deviceId}/module/${moduleId}`, method: 'GET'},
            linkedVlan: {url: `/device/${deviceId}/module/${moduleId}/vlan/${vlanId}/linkedVlan`, method: 'GET'},
            linkedWAGVLAN: {url: `/device/${deviceId}/module/${moduleId}/vlan/${vlanId}/linkedWAGVLAN`, method: 'GET'}
          },
          device: device,
          module: module
        }

        responseData.relationships.forEach(function(item) {
          if (!item.relationship) {
            return
          }

          if (item.relationship._type === 'SERVING') {
            data.services.push({
              sid: item.node.sid,
              reservationId: item.node.reservationId,
              ssid: item.node.ssid,
              links: {
                view: {url: '/serviceOrder/' + item.node._id, method: 'GET'}
              }
            })
          } else if (item.relationship._type === 'LINKED_TO') {
          } else if (item.relationship._type === 'LINKED_FROM') {
          } else if (item.relationship._type === 'CONFIGURED_FOR') {
          } else if (item.relationship._type === 'HAS_VLAN') {
          } else {
            data.others.push({
              relationshipPayload: item.relationship,
              nodePayload: item.node
            })
          }
        })

        clientResponse.render('vlan/details', {
          user: clientResponse.locals.user,
          data: data
        })
      })
    })
  })
})

// DELETE VLAN
router.delete('/:deviceId/module/:moduleId/vlan/:vlanId', function(clientRequest, clientResponse, next) {
  const vlanId = clientRequest.params.vlanId
  const deviceId = clientRequest.params.deviceId
  const moduleId = clientRequest.params.moduleId

  security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    const data = {
      nodeId: vlanId,
      otherNodeIds: [moduleId]
    }

    internalService.deleteNodeWithEdges(data, clientResponse, next, function(responseData) {
      clientResponse.status(200).send({
        id: responseData._id,
        links: {
          index: {
            url: '/device/' + deviceId + '/module/' + moduleId,
            method: 'GET'
          }
        }
      })
    })
  })
})

// GET VLAN base on vlan ID only
router.get('/moduleVLAN/:vlanId', function(clientRequest, clientResponse, next) {
  const vlanId = clientRequest.params.vlanId

  // security.protectWithFetchDevice(deviceId, "DEVICE", clientRequest, clientResponse, next, function () {
  internalService.getNode(vlanId, true, clientResponse, next, function(responseData) {
    let rels = responseData.relationships
    let module = {}

    rels.forEach(rel => {
      if (rel.relationship._type === 'HAS_VLAN') module = rel.node
    })

    if (Object.keys(module).length > 0)
      internalService.getNode(module._id, true, clientResponse, next, function(responseData) {
        let moduleRels = responseData.relationships
        let device = {}

        moduleRels.forEach(moduleRel => {
          if (moduleRel.relationship._type === 'HAS_MEMBER') device = moduleRel.node
        })

        if (Object.keys(device).length > 0)
          clientResponse.redirect('/device/' + device._id + '/module/' + module._id + '/vlan/' + vlanId)
      })
  })
})

// DELETE linked VLAN
router.delete('/:deviceId/module/:moduleId/vlan/:vlanId/linkedVlan/:relId', function(
  clientRequest,
  clientResponse,
  next
) {
  const deviceId = clientRequest.params.deviceId
  const vlanId = clientRequest.params.vlanId
  const moduleId = clientRequest.params.moduleId
  const relId = clientRequest.params.relId

  security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    internalService.deleteRelationship(relId, clientResponse, next, function(r) {
      clientResponse.status(202).send({
        links: {
          index: {url: `/device/${deviceId}/module/${moduleId}/vlan/${vlanId}`, method: 'GET'}
        }
      })
    })
  })
})

// GET linked VLANs
router.get('/:deviceId/module/:moduleId/vlan/:vlanId/linkedVlan', function(clientRequest, clientResponse, next) {
  const deviceId = clientRequest.params.deviceId
  const moduleId = clientRequest.params.moduleId
  const vlanId = clientRequest.params.vlanId

  security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    const queries = clientRequest.query
    const requestData = {
      limit: 0,
      label: 'VLAN',
      relatedNodes: [
        {
          includeRelationship: false,
          label: 'Resource',
          propertyFilter: {
            networkRole: 'AN'
          },
          propertySort: {
            name: 'asc'
          }
        },
        {
          includeRelationship: true,
          label: 'VLAN',
          propertyFilter: {
            _id: vlanId
          }
        }
      ]
    }

    internalService.listNodes(
      requestData,
      clientResponse,
      next,
      function(responseData) {
        const data = []
        const result = {
          data: data
        }

        responseData.nodes.forEach(function(item) {
          const entry = {
            id: item.node._id,
            device: item.resource.name,
            name: item.node.name,
            links: {
              view: {url: `/device/${item.resource._id}/vlan/${item.node._id}`, method: 'GET'},
              viewDevice: {url: `/device/${item.resource._id}`, method: 'GET'},
              delete: {
                url: `/device/${deviceId}/module/${moduleId}/vlan/${vlanId}/linkedVlan/${item.vlanRel._id}`,
                method: 'DELETE'
              }
            }
          }

          data.push(entry)
        })

        clientResponse.send(result)
      },
      function(err) {
        const result = {
          draw: queries.draw,
          data: [],
          recordsFiltered: 0,
          recordsTotal: 0
        }

        if (err.response && err.response.status !== 404) {
          result.error = err.data
        }

        clientResponse.status(200).send(result)
      }
    )
  })
})

// GET linked WAG VLANs
router.get('/:deviceId/module/:moduleId/vlan/:vlanId/linkedWAGVLAN', function(clientRequest, clientResponse, next) {
  const deviceId = clientRequest.params.deviceId
  const moduleId = clientRequest.params.moduleId
  const vlanId = clientRequest.params.vlanId

  security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    internalService.getNodesByLabelPath(
      vlanId,
      'VLAN,Port,Module,Resource',
      clientResponse,
      next,
      function(responseData) {
        const data = []
        const result = {
          data: data
        }

        responseData.forEach(function(item) {
          let wagVLANLink = item['001']
          let wagVLAN = item['002']
          let wagPort = item['004']
          let wagModule = item['006']
          let wagDevice = item['008']
          const entry = {
            id: wagVLAN._id,
            port: wagPort.name,
            module: wagModule.name,
            device: wagDevice.name,
            name: wagVLAN.name,
            links: {
              view: {
                url: `/device/${wagDevice._id}/module/${wagModule._id}/port/${wagPort._id}/vlan/${wagVLAN._id}`,
                method: 'GET'
              },
              viewPort: {url: `/device/${wagDevice._id}/module/${wagModule._id}/port/${wagPort._id}`, method: 'GET'},
              viewModule: {url: `/device/${wagDevice._id}/module/${wagModule._id}`, method: 'GET'},
              viewDevice: {url: `/device/${wagDevice._id}`, method: 'GET'},
              delete: {
                url: `/device/${deviceId}/module/${moduleId}/vlan/${vlanId}/linkedVlan/${wagVLANLink._id}`,
                method: 'DELETE'
              }
            }
          }

          data.push(entry)
        })

        clientResponse.send(result)
      },
      function(err) {
        const result = {
          draw: queries.draw,
          data: [],
          recordsFiltered: 0,
          recordsTotal: 0
        }

        if (err.response && err.response.status !== 404) {
          result.error = err.data
        }

        clientResponse.status(200).send(result)
      }
    )
  })
})

// ADD new linked VLAN
router.post('/:deviceId/module/:moduleId/vlan/:vlanId/linkedVlan', function(clientRequest, clientResponse, next) {
  const deviceId = clientRequest.body.deviceId
  const vlanId = clientRequest.body.vlanId
  const moduleId = clientRequest.body.moduleId
  const targetDeviceId = clientRequest.body.targetDeviceId
  const targetVLANId = clientRequest.body.targetVLANId

  security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    const relationData = {
      startNodeId: targetVLANId,
      endNodeId: vlanId,
      type: 'LINKED_TO'
    }

    internalService.createRelationship(relationData, clientResponse, next, function() {
      clientResponse.status(200).send({
        links: {
          view: {url: `/device/${deviceId}/module/${moduleId}/vlan/${vlanId}`, method: 'GET'}
        }
      })
    })
  })
})

// ADD new linked WAG VLAN
router.post('/:deviceId/module/:moduleId/vlan/:vlanId/linkedWAGVlan', function(clientRequest, clientResponse, next) {
  const deviceId = clientRequest.body.deviceId
  const vlanId = clientRequest.body.vlanId
  const moduleId = clientRequest.body.moduleId
  const targetDeviceId = clientRequest.body.targetDeviceId
  const targetVLANId = clientRequest.body.targetVLANId

  security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    const relationData = {
      startNodeId: vlanId,
      endNodeId: targetVLANId,
      type: 'LINKED_FROM'
    }

    internalService.createRelationship(relationData, clientResponse, next, function() {
      clientResponse.status(200).send({
        links: {
          view: {url: `/device/${deviceId}/module/${moduleId}/vlan/${vlanId}`, method: 'GET'}
        }
      })
    })
  })
})

// ADD new VLAN
router.post('/:deviceId/module/:moduleId/vlan', function(clientRequest, clientResponse, next) {
  const deviceId = clientRequest.params.deviceId
  const moduleId = clientRequest.params.moduleId
  const vlanType = clientRequest.query.type

  const vlanName = clientRequest.body.name
  const netmask = parseInt(clientRequest.body.netmask)
  const mtu = parseInt(clientRequest.body.mtu)
  const number = parseInt(clientRequest.body.number)
  const translation = parseInt(clientRequest.body.translation)
  const purpose = clientRequest.body.purpose
  const intervlanName = clientRequest.body.intervlanName
  const status = clientRequest.body.status
  const encapsulationType = clientRequest.body.encapsulationType
  const mode = clientRequest.body.mode
  const networkId = clientRequest.body.networkId
  const type = clientRequest.body.type

  security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    const requestData = {
      label: 'VLAN' + (vlanType && vlanType === 'interface' ? ':InterfaceVLAN' : ''),
      properties: {
        key: vlanName + '@' + moduleId + '@' + deviceId,
        name: vlanName,
        purpose: purpose,
        intervlanName: intervlanName,
        status: status,
        encapsulationType: encapsulationType,
        mode: mode,
        mtu: mtu,
        netmask: netmask,
        networkId: networkId,
        number: number,
        translation: translation,
        type: type
      }
    }

    internalService.createNode(requestData, clientResponse, next, function(responseData) {
      const relationData = {
        startNodeId: moduleId,
        endNodeId: responseData.node._id,
        type: 'HAS_VLAN'
      }

      internalService.createRelationship(relationData, clientResponse, next, function(responseData2) {
        clientResponse.status(200).send({
          links: {
            view: {url: `/device/${deviceId}/module/${moduleId}`, method: 'GET'}
          }
        })
      })
    })
  })
})

// UPDATE VLAN
router.put('/:deviceId/module/:moduleId/vlan/:vlanId', function(clientRequest, clientResponse, next) {
  _updateVLAN(clientRequest.params.deviceId, clientRequest.params.vlanId, clientRequest, clientResponse, next)
})

//
// AN VLAN routers
//

// GET list of AN VLAN
router.get('/:deviceId/vlan', function(clientRequest, clientResponse, next) {
  const deviceId = clientRequest.params.deviceId

  security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    const queries = clientRequest.query
    const requestData = {
      limit: 0,
      label: 'VLAN',
      relatedNodes: [
        {
          includeRelationship: false,
          label: 'Resource',
          propertyFilter: {
            _id: deviceId
          }
        }
      ],
      optionalNodes: [
        {
          includeRelationship: false,
          label: 'Manufacturer'
        },
        {
          includeRelationship: true,
          label: 'ServiceType'
        }
      ],
      propertySort: {
        name: 'asc'
      }
    }

    internalService.listNodes(
      requestData,
      clientResponse,
      next,
      function(responseData) {
        const data = []
        const result = {
          data: data
        }

        responseData.nodes.forEach(function(item) {
          const entry = {
            id: item.node._id,
            name: item.node.name,
            encapsulationType: item.node.encapsulationType,
            mode: item.node.mode,
            mtu: item.node.mtu,
            netmask: item.node.netmask,
            networkId: item.node.networkId,
            number: item.node.number,
            purpose: item.node.purpose,
            status: item.node.status,
            translation: item.node.translation,
            type: item.node.type,
            vcid: item.node.vcid,
            createdAt: utils.formatDate(item.node.createdAt),
            createdBy: item.node.createdBy + (item.node.source ? ' (' + item.node.source + ')' : ''),
            manufacturer: item.manufacturerOpt ? item.manufacturerOpt.name : null,
            serviceType: item.servicetypeOpt ? item.servicetypeOpt.name : null,
            links: {
              view: {url: '/device/' + deviceId + '/vlan/' + item.node._id, method: 'GET'}
            }
          }

          data.push(entry)
        })

        if (data.length === 0) {
          clientResponse.send(result)
          return
        }

        clientResponse.send(result)
      },
      function(err) {
        const result = {
          draw: queries.draw,
          data: [],
          recordsFiltered: 0,
          recordsTotal: 0
        }

        if (err.response && err.response.status !== 404) {
          result.error = err.data
        }

        clientResponse.status(200).send(result)
      }
    )
  })
})

// UPDATE AN VLAN
router.put('/:deviceId/vlan/:vlanId', function(clientRequest, clientResponse, next) {
  _updateVLAN(clientRequest.params.deviceId, clientRequest.params.vlanId, clientRequest, clientResponse, next)
})

router.put('/:deviceId/vlan/:vlanId/configuration', function(req, res, next) {
  const deviceId = req.params.deviceId
  const vlanId = req.params.vlanId
  const serviceTypeRelId = req.query.serviceTypeRelId
  const manufacturerRelId = req.query.manufacturerRelId
  const serviceTypeId = req.body.serviceTypeId
  const manufacturerId = req.body.manufacturerId
  security.protectWithFetchDevice(deviceId, 'DEVICE', req, res, next, function() {
    let startNodeId = vlanId
    let endNodeId = 0
    if (serviceTypeId) {
      endNodeId = serviceTypeId
      if (serviceTypeRelId) {
        internalService.moveRelationship(
          {id: serviceTypeRelId, startNodeId: startNodeId, endNodeId: endNodeId},
          res,
          next,
          function() {
            res.status(204).send()
          }
        )
      } else {
        internalService.createRelationship(
          {type: 'CONFIGURED_FOR', startNodeId: startNodeId, endNodeId: endNodeId},
          res,
          next,
          function() {
            res.status(204).send()
          }
        )
      }
    } else if (manufacturerId) {
      endNodeId = manufacturerId
      if (manufacturerRelId) {
        internalService.moveRelationship(
          {id: manufacturerRelId, startNodeId: startNodeId, endNodeId: endNodeId},
          res,
          next,
          function() {
            res.status(204).send()
          }
        )
      } else {
        internalService.createRelationship(
          {type: 'CONFIGURED_FOR', startNodeId: startNodeId, endNodeId: endNodeId},
          res,
          next,
          function() {
            res.status(204).send()
          }
        )
      }
    }
  })
})

// ADD new AN VLAN
router.post('/:deviceId/vlan', function(clientRequest, clientResponse, next) {
  const deviceId = clientRequest.body.deviceId
  const deviceIp = clientRequest.body.deviceIp

  const manufactureId = clientRequest.body.manufactureId
  const serviceTypeId = clientRequest.body.serviceTypeId

  const vlanName = clientRequest.body.name
  const netmask = parseInt(clientRequest.body.netmask)
  const mtu = parseInt(clientRequest.body.mtu)
  const number = parseInt(clientRequest.body.number)
  const translation = parseInt(clientRequest.body.translation)
  const purpose = clientRequest.body.purpose
  const status = clientRequest.body.status
  const encapsulationType = clientRequest.body.encapsulationType
  const mode = clientRequest.body.mode
  const networkId = clientRequest.body.networkId
  const type = clientRequest.body.type

  security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    const requestData = {
      label: 'VLAN',
      properties: {
        key: vlanName + '@' + deviceIp,
        name: vlanName,
        purpose: purpose,
        status: status,
        encapsulationType: encapsulationType,
        mode: mode,
        mtu: mtu,
        netmask: netmask,
        networkId: networkId,
        number: number,
        translation: translation,
        type: type
      }
    }

    internalService.createNode(requestData, clientResponse, next, function(responseData) {
      const vlanId = responseData.node._id
      const requestData = {
        startNodeId: deviceId,
        endNodeId: vlanId,
        type: 'HAS_VLAN'
      }

      internalService.createRelationship(requestData, clientResponse, next, function(responseData) {
        const requestData = {
          startNodeId: vlanId,
          endNodeId: manufactureId,
          type: 'CONFIGURED_FOR'
        }

        internalService.createRelationship(requestData, clientResponse, next, function(responseData) {
          const requestData = {
            startNodeId: vlanId,
            endNodeId: serviceTypeId,
            type: 'CONFIGURED_FOR'
          }

          internalService.createRelationship(requestData, clientResponse, next, function(responseData) {
            clientResponse.status(200).send({
              links: {
                view: {url: '/device/' + deviceId + '/vlan/' + vlanId, method: 'GET'}
              }
            })
          })
        })
      })
    })
  })
})

// GET AN VLAN details
router.get('/:deviceId/vlan/:vlanId', function(clientRequest, clientResponse, next) {
  const deviceId = clientRequest.params.deviceId
  const moduleId = clientRequest.params.moduleId
  const vlanId = clientRequest.params.vlanId

  security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    internalService.getNode(vlanId, true, clientResponse, next, function(responseData) {
      const vlan = responseData.node
      let manr = {}
      let sert = {}
      responseData.relationships.forEach(n => {
        if (n.node._labels.includes('Manufacturer')) manr = n.node

        if (n.node._labels.includes('ServiceType')) sert = n.node
      })

      const data = {
        id: vlan._id,
        name: vlan.name,
        encapsulationType: vlan.encapsulationType,
        intervlanName: vlan.intervlanName,
        mode: vlan.mode,
        mtu: vlan.mtu,
        netmask: vlan.netmask,
        networkId: vlan.networkId,
        number: vlan.number,
        purpose: vlan.purpose,
        status: vlan.status,
        translation: vlan.translation,
        type: vlan.type,
        createdAt: utils.formatDate(vlan.createdAt),
        createdBy: vlan.createdBy + (vlan.source ? ' (' + vlan.source + ')' : ''),
        linkedNte: [],
        linkedVlan: [],
        others: [],
        links: {
          delete: {url: `/device/${deviceId}/vlan/${vlanId}?manrId=${manr._id}&sertId=${sert._id}`, method: 'DELETE'},
          edit: {url: `/device/${deviceId}/vlan/${vlanId}`, method: 'PUT'},
          editConfiguration: {url: `/device/${deviceId}/vlan/${vlanId}/configuration?_=1`, method: 'PUT'},
          addLinkWISMVLAN: {url: `/device/${deviceId}/vlan/${vlanId}/wismLink`, method: 'POST'},
          linkWISMVLAN: {url: `/device/${deviceId}/vlan/${vlanId}/wismLink`, method: 'GET'}
        }
      }

      responseData.relationships.forEach(function(item) {
        if (!item.relationship) {
          return
        }

        if (item.relationship._type === 'LINKED_TO') {
          data.linkedVlan.push({
            name: item.node.name,
            links: {
              view: {url: '/device/moduleVLAN/' + item.node._id, method: 'GET'}
            }
          })
        } else if (item.relationship._type === 'HAS_VLAN') {
          data.device = {
            id: item.node._id,
            name: item.node.name,
            ipAddress: item.node.ipAddress,
            links: {
              view: {url: `/device/${item.node._id}`, method: 'GET'}
            }
          }
        } else if (item.relationship._type === 'SERVING_TO' && item.node._labels.includes('Resource')) {
          data.linkedNte.push({
            name: item.node.name,
            ipAddress: item.node.ipAddress,
            links: {
              view: {url: '/device/${item.node._id}', method: 'GET'}
            }
          })
        } else if (item.relationship._type === 'CONFIGURED_FOR' && item.node._labels.includes('ServiceType')) {
          data.purpose = item.relationship.purpose
          data.serviceType = item.node.name
          data.links.viewServiceType = {
            url: '/serviceType/' + item.node._id,
            method: 'GET'
          }
          data.links.editConfiguration.url += `&serviceTypeRelId=${item.relationship._id}`
        } else if (item.relationship._type === 'CONFIGURED_FOR' && item.node._labels.includes('Manufacturer')) {
          data.manufacturer = item.node.name
          data.links.editConfiguration.url += `&manufacturerRelId=${item.relationship._id}`
        } else {
          data.others.push({
            relationshipPayload: item.relationship,
            nodePayload: item.node
          })
        }
      })

      clientResponse.render('vlan/gponVLANDetails', {
        user: clientResponse.locals.user,
        data: data
      })
    })
  })
})

// DELETE AN VLAN
router.delete('/:deviceId/vlan/:vlanId', function(clientRequest, clientResponse, next) {
  const vlanId = clientRequest.params.vlanId
  const deviceId = clientRequest.params.deviceId
  const manrId = clientRequest.query.manrId
  const sertId = clientRequest.query.sertId

  security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    const data = {
      nodeId: vlanId,
      otherNodeIds: [deviceId, manrId, sertId]
    }

    internalService.deleteNodeWithEdges(data, clientResponse, next, function(responseData) {
      clientResponse.status(200).send({
        id: responseData._id,
        links: {
          index: {
            url: `/device/${deviceId}`,
            method: 'GET'
          }
        }
      })
    })
  })
})

// GET WISM VLAN list
router.get('/:deviceId/vlan/:vlanId/wismLink', function(clientRequest, clientResponse, next) {
  const deviceId = clientRequest.params.deviceId
  const vlanId = clientRequest.params.vlanId

  security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    internalService.getNodesByLabelPath(vlanId, 'VLAN,Module,Resource', clientResponse, next, function(responseData) {
      let result = {
        data: []
      }
      if (responseData.length > 0) {
        responseData.forEach(path => {
          let wismLInk = path['001']
          let wismVLAN = path['002']
          let wism = path['004']
          let wac = path['006']

          if (
            wismVLAN._labels.includes('VLAN') &&
            wism._labels.includes('Module') &&
            wac._labels.includes('Resource')
          ) {
            let entry = {
              name: wismVLAN.name,
              module: wism.name,
              device: wac.name,
              links: {
                view: {url: `/device/${wac._id}/module/${wism._id}/vlan/${wismVLAN._id}`, method: 'GET'},
                moduleView: {url: `/device/${wac._id}/module/${wism._id}`, method: 'GET'},
                deviceView: {url: `/device/${wac._id}`, method: 'GET'},
                delete: {url: `/device/${deviceId}/vlan/${vlanId}/wismLink/${wismLInk._id}`, method: 'DELETE'}
              }
            }
            result.data.push(entry)
          }
        })
      }

      clientResponse.send(result)
    })
  })
})

// ADD new AN-WISM VLAN connection
router.post('/:deviceId/vlan/:vlanId/wismLink', function(clientRequest, clientResponse, next) {
  const deviceId = clientRequest.body.deviceId
  const vlanId = clientRequest.body.vlanId
  const wismVLANId = clientRequest.body.wismVLANId

  security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    const relationData = {
      startNodeId: vlanId,
      endNodeId: wismVLANId,
      type: 'LINKED_TO'
    }

    internalService.createRelationship(relationData, clientResponse, next, function() {
      clientResponse.status(200).send({
        links: {
          view: {url: `/device/${deviceId}/vlan/${vlanId}`, method: 'GET'}
        }
      })
    })
  })
})

// DELETE WISM VLAN link
router.delete('/:deviceId/vlan/:vlanId/wismLink/:relId', function(clientRequest, clientResponse, next) {
  const deviceId = clientRequest.params.deviceId
  const vlanId = clientRequest.params.vlanId
  const relId = clientRequest.params.relId

  security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    internalService.deleteRelationship(relId, clientResponse, next, function(r) {
      clientResponse.status(202).send({
        links: {
          index: {url: `/device/${deviceId}/vlan/${vlanId}`, method: 'GET'}
        }
      })
    })
  })
})

//
// WAG VLAN (via Port)
//

// GET list of WAG VLAN
router.get('/:deviceId/module/:moduleId/port/:portId/vlan', function(clientRequest, clientResponse, next) {
  const deviceId = clientRequest.params.deviceId
  const moduleId = clientRequest.params.moduleId
  const portId = clientRequest.params.portId

  security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    const queries = clientRequest.query
    const requestData = {
      limit: 0,
      label: 'VLAN',
      relatedNodes: [
        {
          includeRelationship: false,
          label: 'Port',
          propertyFilter: {
            _id: portId
          }
        }
      ],
      propertySort: {
        name: 'asc'
      }
    }

    internalService.listNodes(
      requestData,
      clientResponse,
      next,
      function(responseData) {
        const data = []
        const result = {
          data: data
        }

        responseData.nodes.forEach(function(item) {
          const entry = {
            id: item.node._id,
            name: item.node.name,
            encapsulationType: item.node.encapsulationType,
            intervlanName: item.node.intervlanName,
            mode: item.node.mode,
            mtu: item.node.mtu,
            netmask: item.node.netmask,
            networkId: item.node.networkId,
            number: item.node.number,
            purpose: item.node.purpose,
            status: item.node.status,
            translation: item.node.translation,
            type: item.node.type,
            vcid: item.node.vcid,
            createdAt: item.node.createdAt ? new Date(item.node.createdAt).toUTCString() : 'N/A',
            createdBy: item.node.createdBy + (item.node.source ? ' (' + item.node.source + ')' : ''),
            links: {
              view: {
                url: `/device/${deviceId}/module/${moduleId}/port/${portId}/vlan/${item.node._id}`,
                method: 'GET'
              }
            }
          }

          data.push(entry)
        })

        clientResponse.send(result)
      },
      function(err) {
        const result = {
          draw: queries.draw,
          data: [],
          recordsFiltered: 0,
          recordsTotal: 0
        }

        if (err.response && err.response.status !== 404) {
          result.error = err.data
        }

        clientResponse.status(200).send(result)
      }
    )
  })
})

// GET list of WAG VLAN data (id + name map)
router.get('/:deviceId/module/:moduleId/port/:portId/vlan/data', function(clientRequest, clientResponse, next) {
  const deviceId = clientRequest.params.deviceId
  const moduleId = clientRequest.params.moduleId
  const portId = clientRequest.params.portId

  security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    const queries = clientRequest.query
    const requestData = {
      limit: 0,
      label: 'Port',
      propertyFilter: {
        _id: portId
      },
      relatedNodes: [
        {
          includeRelationship: false,
          label: 'Resource',
          propertyFilter: {
            _id: deviceId
          }
        }
      ],
      optionalNodes: [
        {
          includeRelationship: false,
          label: 'VLAN'
        }
      ]
    }

    internalService.listNodes(requestData, clientResponse, next, function(responseData) {
      const data = []

      responseData.nodes.forEach(function(item) {
        data.push({
          id: item.vlanOpt._id,
          name: item.vlanOpt.name
        })
      })
      data.sort(function(a, b) {
        return parseInt(a.name) > parseInt(b.name)
      })

      clientResponse.send(data)
    })
  })
})

// ADD new WAG VLAN
router.post('/:deviceId/module/:moduleId/port/:portId/vlan', function(clientRequest, clientResponse, next) {
  const deviceId = clientRequest.params.deviceId
  const moduleId = clientRequest.params.moduleId
  const portId = clientRequest.params.portId

  const vlanName = clientRequest.body.name
  const netmask = parseInt(clientRequest.body.netmask)
  const mtu = parseInt(clientRequest.body.mtu)
  const number = parseInt(clientRequest.body.number)
  const translation = parseInt(clientRequest.body.translation)
  const purpose = clientRequest.body.purpose
  const intervlanName = clientRequest.body.intervlanName
  const status = clientRequest.body.status
  const encapsulationType = clientRequest.body.encapsulationType
  const mode = clientRequest.body.mode
  const networkId = clientRequest.body.networkId
  const type = clientRequest.body.type

  security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    const requestData = {
      label: 'VLAN:InterfaceVLAN',
      properties: {
        key: vlanName + '@' + portId + '@' + moduleId + '@' + deviceId,
        name: vlanName,
        purpose: purpose,
        intervlanName: intervlanName,
        status: status,
        encapsulationType: encapsulationType,
        mode: mode,
        mtu: mtu,
        netmask: netmask,
        networkId: networkId,
        number: number,
        translation: translation,
        type: type
      }
    }

    internalService.createNode(requestData, clientResponse, next, function(responseData) {
      const relationData = {
        startNodeId: portId,
        endNodeId: responseData.node._id,
        type: 'HAS_VLAN'
      }

      internalService.createRelationship(relationData, clientResponse, next, function(responseData2) {
        clientResponse.status(200).send({
          links: {
            view: {url: `/device/${deviceId}/module/${moduleId}/port/${portId}`, method: 'GET'}
          }
        })
      })
    })
  })
})

// GET WAG VLAN details
router.get('/:deviceId/module/:moduleId/port/:portId/vlan/:vlanId', function(clientRequest, clientResponse, next) {
  const deviceId = clientRequest.params.deviceId
  const moduleId = clientRequest.params.moduleId
  const portId = clientRequest.params.portId
  const vlanId = clientRequest.params.vlanId

  security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    internalService.getNodesByLabelPath(vlanId, 'Port,Module,Resource', clientResponse, next, function(responseData) {
      const vlan = responseData[0]['000']
      const port = responseData[0]['002']
      port.links = {
        view: {url: `/device/${deviceId}/module/${moduleId}/port/${portId}`, method: 'GET'}
      }
      const module = responseData[0]['004']
      module.links = {
        view: {url: `/device/${deviceId}/module/${moduleId}`, method: 'GET'}
      }
      const device = responseData[0]['006']
      device.links = {
        view: {url: `/device/${deviceId}`, method: 'GET'}
      }

      internalService.getNode(vlanId, true, clientResponse, next, function(responseData) {
        const vlan = responseData.node
        const manr = responseData.manufacturerOpt
        const sert = responseData.servicetypeOpt

        const data = {
          id: vlan._id,
          name: vlan.name,
          encapsulationType: vlan.encapsulationType,
          intervlanName: vlan.intervlanName,
          mode: vlan.mode,
          mtu: vlan.mtu,
          netmask: vlan.netmask,
          networkId: vlan.networkId,
          number: vlan.number,
          purpose: vlan.purpose,
          status: vlan.status,
          translation: vlan.translation,
          type: vlan.type,
          createdAt: utils.formatDate(vlan.createdAt),
          createdBy: vlan.createdBy + (vlan.source ? ' (' + vlan.source + ')' : ''),
          manufacture: manr ? manr.name : null,
          services: [],
          others: [],
          links: {
            delete: {url: `/device/${deviceId}/module/${moduleId}/port/${portId}/vlan/${vlanId}`, method: 'DELETE'},
            edit: {url: `/device/${deviceId}/module/${moduleId}/port/${portId}/vlan/${vlanId}`, method: 'PUT'},
            device: {url: `/device/${deviceId}`, method: 'GET'},
            module: {url: `/device/${deviceId}/module/${moduleId}`, method: 'GET'},
            port: {url: `/device/${deviceId}/module/${moduleId}/port/${portId}`, method: 'GET'},
            linkWISMVLAN: {
              url: `/device/${deviceId}/module/${moduleId}/port/${portId}/vlan/${vlanId}/wismLink`,
              method: 'GET'
            },
            addLinkWISMVLAN: {
              url: `/device/${deviceId}/module/${moduleId}/port/${portId}/vlan/${vlanId}/wismLink`,
              method: 'POST'
            }
          },
          device: device,
          module: module,
          port: port
        }

        responseData.relationships.forEach(function(item) {
          if (!item.relationship) {
            return
          }

          if (item.relationship._type === '_SERVING') {
            data.services.push({
              sid: item.node.sid,
              reservationId: item.node.reservationId,
              ssid: item.node.ssid,
              links: {
                view: {url: '/serviceOrder/' + item.node._id, method: 'GET'}
              }
            })
          } else if (item.relationship._type === 'LINKED_TO') {
          } else if (item.relationship._type === 'LINKED_FROM') {
          } else if (item.relationship._type === '_CONFIGURED_FOR') {
          } else if (item.relationship._type === 'HAS_VLAN') {
          } else {
            data.others.push({
              relationshipPayload: item.relationship,
              nodePayload: item.node
            })
          }
        })

        clientResponse.render('vlan/wagVLANDetails', {
          user: clientResponse.locals.user,
          data: data
        })
      })
    })
  })
})

// DELETE WAG VLAN
router.delete('/:deviceId/module/:moduleId/port/:portId/vlan/:vlanId', function(clientRequest, clientResponse, next) {
  const vlanId = clientRequest.params.vlanId
  const deviceId = clientRequest.params.deviceId
  const moduleId = clientRequest.params.moduleId
  const portId = clientRequest.params.portId

  security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    const data = {
      nodeId: vlanId,
      otherNodeIds: [portId]
    }

    internalService.deleteNodeWithEdges(data, clientResponse, next, function(responseData) {
      clientResponse.status(200).send({
        id: responseData._id,
        links: {
          index: {
            url: `/device/${deviceId}/module/${moduleId}/port/${portId}`,
            method: 'GET'
          }
        }
      })
    })
  })
})

// UPDATE WAG VLAN
router.put('/:deviceId/module/:moduleId/port/:portId/vlan/:vlanId', function(clientRequest, clientResponse, next) {
  _updateVLAN(clientRequest.params.deviceId, clientRequest.params.vlanId, clientRequest, clientResponse, next)
})

// GET WAG-WISM VLAN list
router.get('/:deviceId/module/:moduleId/port/:portId/vlan/:vlanId/wismLink', function(
  clientRequest,
  clientResponse,
  next
) {
  const deviceId = clientRequest.params.deviceId
  const moduleId = clientRequest.params.moduleId
  const portId = clientRequest.params.portId
  const vlanId = clientRequest.params.vlanId

  security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    internalService.getNodesByLabelPath(vlanId, 'VLAN,Module,Resource', clientResponse, next, function(responseData) {
      let result = {
        data: []
      }
      if (responseData.length > 0) {
        responseData.forEach(path => {
          let wismLInk = path['001']
          let wismVLAN = path['002']
          let wism = path['004']
          let wac = path['006']

          if (
            wismVLAN._labels.includes('VLAN') &&
            wism._labels.includes('Module') &&
            wac._labels.includes('Resource')
          ) {
            let entry = {
              name: wismVLAN.name,
              module: wism.name,
              device: wac.name,
              links: {
                view: {url: `/device/${wac._id}/module/${wism._id}/vlan/${wismVLAN._id}`, method: 'GET'},
                moduleView: {url: `/device/${wac._id}/module/${wism._id}`, method: 'GET'},
                deviceView: {url: `/device/${wac._id}`, method: 'GET'},
                delete: {
                  url: `/device/${deviceId}/module/${moduleId}/port/${portId}/vlan/${vlanId}/wismLink/${wismLInk._id}`,
                  method: 'DELETE'
                }
              }
            }
            result.data.push(entry)
          }
        })
      }

      clientResponse.send(result)
    })
  })
})

// ADD new WAG-WISM VLAN connection
router.post('/:deviceId/module/:moduleId/port/:portId/vlan/:vlanId/wismLink', function(
  clientRequest,
  clientResponse,
  next
) {
  const deviceId = clientRequest.params.deviceId
  const moduleId = clientRequest.params.moduleId
  const portId = clientRequest.params.portId
  const vlanId = clientRequest.params.vlanId
  const wismVLANId = clientRequest.body.wismVLANId

  security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    const relationData = {
      startNodeId: wismVLANId,
      endNodeId: vlanId,
      type: 'LINKED_FROM'
    }

    internalService.createRelationship(relationData, clientResponse, next, function() {
      clientResponse.status(200).send({
        links: {
          view: {url: `/device/${deviceId}/module/${moduleId}/port/${portId}/vlan/${vlanId}`, method: 'GET'}
        }
      })
    })
  })
})

// DELETE WAG-WISM VLAN link
router.delete('/:deviceId/module/:moduleId/port/:portId/vlan/:vlanId/wismLink/:relId', function(
  clientRequest,
  clientResponse,
  next
) {
  const deviceId = clientRequest.params.deviceId
  const moduleId = clientRequest.params.moduleId
  const portId = clientRequest.params.portId
  const vlanId = clientRequest.params.vlanId
  const relId = clientRequest.params.relId

  security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    internalService.deleteRelationship(relId, clientResponse, next, function(r) {
      clientResponse.status(202).send({
        links: {
          index: {url: `/device/${deviceId}/module/${moduleId}/port/${portId}/vlan/${vlanId}`, method: 'GET'}
        }
      })
    })
  })
})

module.exports = router
