const express = require('express')
const router = express.Router()

const security = require('../lib/Security')
const internalService = require('../lib/ISPService')

const dataStructure = [{name: 'name', label: 'Name', link: 'view', filter: {type: 'text'}}]

router.get('/data', function(clientRequest, clientResponse, next) {
  // security.protect('SERVICE_TYPE', clientRequest, clientResponse, next, function() {
    const queries = clientRequest.query
    const requestData = {
      limit: queries.length ? queries.length : 10,
      label: 'ServiceType',
      skip: queries.start,
      propertyFilter: {},
      propertySort: {}
    }

    if (queries.order) {
      queries.order.forEach(function(item) {
        const field = dataStructure[item.column]
        if (field) {
          requestData.propertySort[field.name] = item.dir
        }
      })
    }

    if (queries.columns) {
      queries.columns.forEach(function(item) {
        if (item.search.value !== '' && item.name) {
          const query = item.search.value.replace(/ /g, '*')
          requestData.propertyFilter[item.name] = '(?i)' + query + '*'
        }
      })
    }

    internalService.listNodes(
      requestData,
      clientResponse,
      next,
      function(responseData) {
        const data = []

        responseData.nodes.forEach(function(item) {
          data.push({
            id: item.node._id ? item.node._id : null,
            name: item.node.name ? item.node.name : null,
            links: {
              view: {url: '/serviceType/' + item.node._id, method: 'GET'}
            }
          })
        })

        const result = {
          draw: queries.draw,
          data: data,
          recordsFiltered: responseData.pager.total,
          recordsTotal: responseData.pager.total
        }

        if (queries.json === 'true') clientResponse.status(200).send(result.data)
        else clientResponse.send(result)
      },
      function(err) {
        const result = {
          draw: queries.draw,
          data: [],
          recordsFiltered: 0,
          recordsTotal: 0
        }

        if (err.status !== 404) {
          result.error = err.message
        }

        clientResponse.status(200).send(result)
      }
    )
  // })
})

router.get('/', function(clientRequest, clientResponse, next) {
  // security.protect('SERVICE_TYPE', clientRequest, clientResponse, next, function() {
    clientResponse.render('serviceType/list', {
      user: clientResponse.locals.user,
      dataStructure: dataStructure,
      links: {
        data: {url: '/serviceType/data', method: 'GET'}
      }
    })
  // })
})

router.post('/', function(clientRequest, clientResponse, next) {
  // security.protect('SERVICE_TYPE', clientRequest, clientResponse, next, function() {
    const requestData = {
      label: 'ServiceType',
      properties: {
        name: clientRequest.body.name
      }
    }

    internalService.createNode(requestData, clientResponse, next, function(responseData) {
      clientResponse.status(200).send({
        id: responseData.node._id,
        links: {
          view: {url: '/serviceType/' + responseData.node._id, method: 'GET'}
        }
      })
    })
  // })
})

router.get('/:serviceId', function(clientRequest, clientResponse, next) {
  // security.protect('SERVICE_TYPE', clientRequest, clientResponse, next, function() {
    const serviceId = clientRequest.params.serviceId

    internalService.getNode(serviceId, true, clientResponse, next, function(responseData) {
      const data = {
        id: serviceId,
        name: responseData.node.name,
        resources: [],
        ports: [],
        vlan: [],
        vlanGroup: [],
        pop: [],
        product: [],
        servicePackage: [],
        others: [],
        links: {
          view: {url: '/serviceType/' + serviceId, method: 'GET'},
          edit: {url: '/serviceType/' + serviceId, method: 'PUT'},
          delete: {url: '/serviceType/' + serviceId, method: 'DELETE'},
          addVlanGroup: {url: '/vlanGroup/service', method: 'POST'},
          addServicePackage: {url: '/serviceType/servicePackage', method: 'POST'}
        }
      }

      responseData.relationships.forEach(function(item) {
        if (!item.relationship) {
          return
        }

        if (item.relationship._type === 'VLAN_GROUP') {
          data.vlanGroup.push({
            id: item.node._id,
            name: item.node.name,
            category: item.node.category,
            relationship: item.relationship,
            links: {
              view: {url: '/vlanGroup/' + item.node._id, method: 'GET'},
              deleteRelationship: {
                url: '/serviceType/' + serviceId + '/relation/' + item.relationship._id,
                method: 'DELETE'
              }
            }
          })
        } else if (item.relationship._type === 'PROVISION_IN') {
          data.resources.push({
            id: item.node._id,
            name: item.node.name,
            type: item.node.type,
            ipAddress: item.node.ipAddress,
            networkRole: item.node.networkRole,
            links: {
              view: {url: '/device/' + item.node._id, method: 'GET'}
            }
          })
        } else if (item.relationship._type === 'SERVED_BY') {
          data.ports.push({
            id: item.node._id,
            name: item.node.name,
            deviceName: item.node.resource,
            deviceIpAddress: item.node.resourceIpAddress,
            links: {
              view: {url: '/device/xxx/port/' + item.node._id, method: 'GET'}
            }
          })
        } else if (item.relationship._type === 'WITH_PRODUCT') {
          data.product.push({
            id: item.node._id,
            name: item.node.name
          })
        } else if (item.relationship._type === 'SERVING') {
          data.pop.push({
            id: item.node._id,
            name: item.node.name,
            type: item.node.type,
            links: {
              view: {url: '/area/pop/' + item.node._id, method: 'GET'}
            }
          })
        } else if (item.relationship._type === 'CONFIGURED_FOR' && item.node._labels.includes('VLAN')) {
          data.vlan.push({
            name: item.node.name,
            links: {
              view: {url: '/area/pop/' + item.node._id, method: 'GET'}
            }
          })
        } else if (item.relationship._type === 'HAS_PACKAGE') {
          data.servicePackage.push({
            name: item.node.name,
            alias: item.node.alias,
            relationship: item.relationship,
            links: {
              view: {url: `/servicePackage/${item.node._id}`, method: 'GET'},
              deleteServicePackage: {
                url: '/serviceType/' + serviceId + '/servicePackage/' + item.relationship._id,
                method: 'DELETE'
              }
            }
          })
        } else if (item.relationship._type !== 'COUNTER' && item.relationship._type !== 'WITH_TYPE') {
          data.others.push({
            relationshipPayload: item.relationship,
            nodePayload: item.node
          })
        }
      })

      clientResponse.render('serviceType/details', {
        user: clientResponse.locals.user,
        data: data
      })
    })
  // })
})

router.put('/:serviceId', function(clientRequest, clientResponse, next) {
  // security.protect('SERVICE_TYPE', clientRequest, clientResponse, next, function() {
    const serviceId = clientRequest.params.serviceId

    const requestData = {
      label: 'ServiceType',
      id: serviceId,
      properties: {
        name: clientRequest.body.name
      }
    }

    internalService.updateNode(requestData, clientResponse, next, function() {
      clientResponse.status(200).send({
        id: serviceId,
        links: {
          view: {
            url: '/serviceType/' + serviceId,
            method: 'GET'
          }
        }
      })
    })
  // })
})

router.delete('/:serviceId', function(clientRequest, clientResponse, next) {
  // security.protect('SERVICE_TYPE', clientRequest, clientResponse, next, function() {
    const serviceId = clientRequest.params.serviceId

    internalService.deleteNode(serviceId, clientResponse, next, function() {
      clientResponse.status(200).send({
        id: serviceId,
        links: {
          index: {url: '/serviceType', method: 'GET'}
        }
      })
    })
  // })
})

router.post('/pop', function(clientRequest, clientResponse, next) {
  // security.protect('SERVICE_TYPE', clientRequest, clientResponse, next, function() {
    const relationData = {
      startNodeId: clientRequest.body.popId,
      endNodeId: clientRequest.body.serviceId,
      type: 'SERVING'
    }

    internalService.createRelationship(relationData, clientResponse, next, function() {
      clientResponse.status(200).send({
        links: {
          view: {url: '/serviceType/' + clientRequest.body.serviceId, method: 'GET'}
        }
      })
    })
  // })
})

router.post('/servicePackage', function(req, res, next) {
  // security.protect('SERVICE_TYPE', req, res, next, function() {
    const relationData = {
      startNodeId: req.body.id,
      endNodeId: req.body.servicePackageId,
      type: 'HAS_PACKAGE'
    }

    internalService.createRelationship(relationData, res, next, function() {
      res.status(200).send({
        links: {
          view: {url: '/serviceType/' + req.body.id, method: 'GET'}
        }
      })
    })
  // })
})

router.delete('/:serviceId/relation/:relationshipId', function(clientRequest, clientResponse, next) {
  // security.protect('SERVICE_TYPE', clientRequest, clientResponse, next, function() {
    const relationshipId = clientRequest.params.relationshipId
    const serviceId = clientRequest.params.serviceId

    internalService.deleteRelationship(relationshipId, clientResponse, next, function() {
      clientResponse.status(200).send({
        id: serviceId,
        links: {
          index: {url: '/serviceType/' + serviceId, method: 'GET'}
        }
      })
    })
  // })
})

router.delete('/:id/servicePackage/:relationshipId', function(req, res, next) {
  // security.protect('SERVICE_TYPE', req, res, next, function() {
    const relationshipId = req.params.relationshipId
    const id = req.params.id

    internalService.deleteRelationship(relationshipId, res, next, function() {
      res.status(200).send({
        id: id,
        links: {
          index: {url: '/serviceType/' + id, method: 'GET'}
        }
      })
    })
  // })
})

module.exports = router
