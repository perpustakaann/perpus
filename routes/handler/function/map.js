const internalService = require('../../../lib/InternalService')
const convert = require('./convert')
const geoJSONPolyline = require('geojson-polyline')

exports.spatialIntersect = function (objectType, req, res, next) {
    var paramIntersect = {
        wkt: req.query.bboxWKT,
        objectType: objectType,
        geojson: true,
        encodeGeom: true
      }
    internalService.spatialIntersect(paramIntersect, res, next, function(dataIntersect){
        let features = []
        dataIntersect.forEach(function(i){
            features.push(convert.toGeoJson(geoJSONPolyline.decode(i.geometry), i))
        })
        res.send(convert.combineFeatureCollection(features))
    })
}

exports.deleteSpatial = function(objectType, req ,res ,next){
    const paramDeleteSpatial = {
        objectType : objectType,
        key : req.body.properties.uid
    }
    internalService.spatialDelete(paramDeleteSpatial, res, next, function(dataDelete){
       res.send()
    })
}

exports.getLocationIntersect = function (param, res ,next ,callback) {
    var paramIntersect = {
        wkt: param.wkt,
        objectType: param.objectType
    }
    internalService.spatialIntersect(paramIntersect, res, next, function(dataIntersect){
        callback(dataIntersect[0])
    })
}