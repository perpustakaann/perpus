const wkt = require('wkt')

exports.combineFeatureCollection = function (data) {
    data = {
        type: 'FeatureCollection',
        features: data
    }
    return data
}

exports.toGeoJson = function (data, properties) {
    // properties.styles = {
    //     strokeColor: random_color(),
    //     fillColor: random_color(),
    //     editable: true
    // }

    data = wkt.parse(data)
    let id = generate_hex()
    data = {
        type: 'Feature',
        id: id,
        properties: properties,
        geometry: data
    }
    return data
}

function generate_hex() {
    return Math.random().toString(16).substring(5, 10).toUpperCase() + Math.random().toString(16).substring(5, 10).toUpperCase();
}

function random_color() {
    console.log('hit')
    return 'hsl(' + 360 * Math.random() + ',' + (80 + 70 * Math.random()) + '%,' + (80 + 10 * Math.random()) + '%)';
}