const router = require('express').Router()
const internalService = require('../lib/InternalService')

const dataStructure = [{
  name: 'name',
  label: 'Name',
  link: 'view',
  filter: {
    type: 'text'
  },
  defaultSort: 'asc'
},
{
  name: 'type',
  label: 'Type',
  filter: {
    type: 'text'
  }
},
{
  name: 'uid',
  label: 'Site ID',
  filter: {
    type: 'text'
  }
},
{
  name: 'address',
  label: 'Address',
  filter: {
    type: 'text'
  }
},
{
  name: 'building',
  label: 'Building',
  filter: {
    type: 'text'
  }
},
{
  name: 'createdAt',
  label: 'Created At',
  type: 'date'
},
{
  name: 'createdBy',
  label: 'Created By',
  filter: {
    type: 'text'
  }
}
]

router.get('/data', function (req, res, next) {
  const queries = req.query
  const reqData = {
    limit: 0,
    label: "Location",
    skip: queries.start,
    propertyFilter: {},
    propertySort: {}
  }

  if (queries.order) {
    queries.order.forEach(function (item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.name] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function (item) {
      if (item.search.value !== '' && item.name) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.name] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function (resData) {
      const data = []

      resData.nodes.forEach(function (item) {
        data.push({
          _id: item.node._id ? item.node._id : null,
          name: item.node.name ? item.node.name : null,
          type: item.node.type ? item.node.type : null,
          uid: item.node.uid ? item.node.uid : null,
          address: item.node.address ? item.node.address : null,
          status: item.node.status ? item.node.status : null,
          createdAt: item.node.createdAt ? item.node.createdAt : null,
          createdBy: item.node.createdBy ? item.node.createdBy : null,
          updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
          updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
          links: {
            view: {
              url: '/location/' + item.node._id,
              method: 'GET'
            }
          }
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }

      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function (err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }

      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})

router.get('/', function (req, res, next) {
  res.render('location/list', {
    title: `${serverConfig.server.name.toUpperCase()}`,
    user: res.locals.user,
    dataStructure: dataStructure,
    links: {
      data: {
        url: '/location/data',
        method: 'GET'
      }
    }
  })
})

router.post('/', function (req, res, next) {
  const reqData = {
    label: 'Location',
    properties: {
      name: req.body.name,
      type: req.body.type,
      uid: req.body.uid,
      address: req.body.address,
      status: 'ACTIVE'
    }
  }

  internalService.createNode(reqData, res, next, function (resData) {
    res.status(200).send({
      links: {
        view: {
          url: '/location/' + resData.node._id,
          method: 'GET'
        }
      }
    })
  })
})

router.put('/:_id', function (req, res, next) {
  var _id = req.params._id
  var data = {
    label: 'Location',
    id: parseInt(_id),
    properties: req.body
  }

  internalService.updateNode(data, res, next, function () {
    res.status(200).send({
      links: {
        index: {
          url: '/location/' + _id,
          method: 'GET'
        }
      }
    })
  })
})

router.get('/:_id', function (req, res, next) {
  var id = req.params._id

  internalService.getNodesByLabelPath(id, 'Location', '', 'Building,Floor,Room,Rack', res, next, function (resData) {
    let result = {
      location: {},
      building: [],
      floor: [],
      room: [],
      rack: [],
      space: []
    }
    const records = resData
    if (records.length > 0) {
      records.forEach((i) => {
        result.location = i['000']
        result.location['links'] = {
          delete: {
            url: '/location/' + i['000']._id,
            method: 'DELETE'
          },
          edit: {
            url: '/location/' + i['000']._id,
            method: 'PUT'
          }
        }
        const height = i['002'].height ? i['002'].height : null
        const width = i['002'].width ? i['002'].width : null
        const length = i['002'].length ? i['002'].length : null
        const squareMeter = i['002'].squareMeter ? i['002'].squareMeter : null

        // Building
        if (i['002']) {
          i['002']['size'] = `${height}/${width}/${length}/${squareMeter}`
          i['002']['links'] = {
            view: {
              url: '/building/' + i['002']._id + '?parentId=' + id,
              method: 'GET'
            },
            delete: {
              url: `/location/building/${i['002']._id}/${i['001']._id}/${id}`,
              method: 'DELETE'
            },
          }
          if (result.building.length > 0) {
            result.building.forEach((b) => {
              if (b._id !== i['002']._id) {

                result.building.push(i['002'])
              }
            })
          } else {
            result.building.push(i['002'])
          }
        }
        if (i['004']) {
          i['004']['links'] = {
            view: {
              url: '/floor/' + i['004']._id + '?parentId=' + id,
              method: 'GET'
            },
            delete: {
              url: '/floor/' + i['004']._id + '?parentId=' + id,
              method: 'DELETE'
            },
          }
          i['004']['size'] = `${i['004'].height}/${i['004'].width}`
          // Floor
          if (result.floor.length > 0) {
            result.floor.forEach((b) => {
              if (b._id !== i['004']._id) {
                result.floor.push(i['004'])
              }
            })
          } else {
            result.floor.push(i['004'])
          }
        }

        // Room
        if (i['006']) {
          i['006']['links'] = {
            view: {
              url: '/room/' + i['006']._id + '?parentId=' + id,
              method: 'GET'
            },
            delete: {
              url: '/room/' + i['006']._id + '?parentId=' + id,
              method: 'DELETE'
            },
          }
          i['006']['size'] = `${i['006'].height}/${i['006'].width}/${i['006'].length}`
          if (result.room.length > 0) {
            result.room.forEach((b) => {
              if (b._id !== i['006']._id) {
                result.room.push(i['006'])
              }
            })
          } else {
            result.room.push(i['006'])
          }
        }
        // Rack
        i['008']['links'] = {
          view: { url: '/rack/' + i['008']._id + '?parentId=' + id, method: 'GET' },
          delete: { url: '/rack/' + i['008']._id + '?parentId=' + id, method: 'DELETE' },
        }
        i['008']['size'] = `${i['008'].height}/${i['008'].width}/${i['008'].length}`
        // if(result.rack.length > 0){
        //   result.rack.forEach((b) => {
        //     if(b._id !== i['008']._id){
        //       result.rack.push(i['008'])
        //     }
        //   })
        // }else{
        result.rack.push(i['008'])
        // }
      })
      res.render('location/details', {
        title: `${serverConfig.server.name.toUpperCase()}`,
        user: res.locals.user,
        data: result
      })
    }
  })



})

router.delete('/:_id', function (req, res, next) {
  const _id = req.params._id

  internalService.deleteNode(_id, res, next, function () {
    res.status(200).send({
      links: {
        index: {
          url: '/location/',
          method: 'GET'
        }
      }
    })
  })
})
router.delete('/building/:buildingId/:relationshipId/:locationId', function (req, res, next) {
  const _id = req.params.buildingId
  const locationId = req.params.locationId
  const relationshipId = req.params.relationshipId
  internalService.deleteRelationship(relationshipId, res, next, function () {
    internalService.deleteNode(_id, res, next, function () {
      res.status(200).send({
        links: {
          index: {
            url: `/location/${locationId}`,
            method: 'GET'
          }
        }
      })
    })
  })
})
router.get('/:name/location', function (req, res, next) {
  const name = req.params.locationName
  const queries = req.query
  const reqData = {
    limit: 0,
    label: 'Location',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {},
    relatedNodes: [{
      label: 'Building',
      relationLabel: "LOCATED_AT",
      includeRelationship: true,
      collection: true,
      propertyFilter: {
        name: name
      }
    },]
  }

  if (queries.order) {
    queries.order.forEach(function (item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.name] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function (item) {
      if (item.search.value !== '' && item.name) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.name] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function (resData) {
      const data = []

      resData.nodes.forEach(function (item) {
        data.push({
          _id: item.node._id ? item.node._id : null,
          name: item.node.name ? item.node.name : null
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }
      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function (err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }
      if (err.status !== 404) {
        result.error = err.message
      }
      res.status(200).send(result)
    }
  )
})
router.post('/building', function (req, res, next) {
  const reqData = {
    label: 'Building',
    properties: {
      name: req.body.name,
      type: req.body.type,
      height: req.body.height,
      width: req.body.width,
      length: req.body.lengthBuilding,
      squareMeter: req.body.squareMeter,
      labelingCode: req.body.labelingCode,
      floor: req.body.floor,
      builtDate: req.body.builtDate,
      status: 'ACTIVE'
    }
  }
  internalService.createNode(reqData, res, next, function (resData) {
    const paramsData = {
      startNodeId: resData.node._id,
      endNodeId: req.body.locationId,
      type: "LOCATED_AT"
    }
    internalService.createRelationship(paramsData, res, next, function (resData2) {
      let request = {
        collection: 'IDC-FE',
        type: 'QUERY',
        name: 'GENERATE_FLOOR',
        body: {
          params: {
            buildingId: parseInt(resData.node._id),
            floorcount: parseInt(req.body.floor),
            createdBy: res.locals.user
          }
        }
      }
      internalService.predefinedQuery(request, res, next, function (resData) {
        res.status(200).send({
          links: {
            view: {
              url: '/location/' + req.body.locationId,
              method: 'GET'
            }
          }
        })
      })
    })
  })
})

router.get('/:name/building', function (req, res, next) {
  const name = req.params.name
  const queries = req.query
  const reqData = {
    limit: 0,
    label: 'Building',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {},
    relatedNodes: [{
      label: "Location",
      relationLabel: "LOCATED_AT",
      // directionSensitive: true,
      includeRelationship: true,
      collection: true,
      propertyFilter: {
        name: name
      }
    },]
  }

  if (queries.order) {
    queries.order.forEach(function (item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.name] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function (item) {
      if (item.search.value !== '' && item.name) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.name] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function (resData) {
      const data = []

      resData.nodes.forEach(function (item) {
        data.push({
          _id: item.node._id ? item.node._id : null,
          name: item.node.name ? item.node.name : null
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }

      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function (err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }

      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})




// map handler




module.exports = router