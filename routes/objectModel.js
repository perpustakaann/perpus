const router = require('express').Router()
const internalService = require('../lib/InternalService')

const dataStructure = [
  {name: 'name', label: 'Name', link: 'view', filter: {type: 'text'}, defaultSort: 'asc'},
  {name: 'nameAlias', label: 'Name Alias', filter: {type: 'text'}},
  {name: 'objectName', label: 'Object', link: 'viewObject', filter: {type: 'text'}},
  {name: 'description', label: 'Description', filter: {type: 'text'}},
  {name: 'status', label: 'Status', type: 'boolean'},
  {name: 'createdAt', label: 'Created At', type: 'date'},
  {name: 'createdBy', label: 'Created By', filter: {type: 'text'}}
]

router.get('/data', function(req, res, next) {
  const queries = req.query
  const reqData = {
    limit: 0,
    label: 'ObjectModel',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {},
    optionalNodes: [
      {
        label: "Object",
        relationLabel: "HAS_PARENT",
        // directionSensitive: true,
        includeRelationship: true,
        collection: true
      },
    ]
  }

  if (queries.order) {
    queries.order.forEach(function(item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.name] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function(item) {
      if (item.search.value !== '' && item.name) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.name] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function(resData) {
      const data = []

      resData.nodes.forEach(function(item) {
        data.push({
          _id: item.node._id ? item.node._id : null,
          objectName: item.objectOpt[0].name,
          name: item.node.name ? item.node.name : null,
          nameAlias: item.node.nameAlias ? item.node.nameAlias : null,
          description: item.node.description ? item.node.description : null,
          status: item.node.status ? item.node.status: null,
          createdAt: item.node.createdAt ? item.node.createdAt : null,
          createdBy: item.node.createdBy ? item.node.createdBy : null,
          updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
          updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
          links: {
            view: {url: '/objectModel/' + item.node._id, method: 'GET'},
            viewObject: {url: '/object/' + item.objectOpt[0]._id, method: 'GET'},
          }
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }

      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function(err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }

      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})

router.get('/', function(req, res, next) {
  res.render('objectModel/list', {
    title: `${serverConfig.server.name.toUpperCase()}`,
    user: res.locals.user,
    dataStructure: dataStructure,
    links: {
      data: {url: '/objectModel/data', method: 'GET'}
    }
  })
})

router.post('/', function(req, res, next) {
  const reqData = {
    label: 'ObjectModel',
    properties: {
      name: req.body.name,
      nameAlias: req.body.nameAlias,
      description: req.body.description,
      status: 'ACTIVE'
    }
  }

  internalService.createNode(reqData, res, next, function(resData) {

    if(req.body.objectId){
      const relationData = {
        startNodeId: resData.node._id,
        endNodeId: req.body.objectId,
        type: 'HAS_PARENT',
      }
  
      internalService.createRelationship(relationData, res, next, function(){
        res.status(200).send({
          links: {
            view: {url: '/objectModel/' + resData.node._id, method: 'GET'}
          }
        })
      })
    }else{
      res.status(200).send({
        links: {
          view: {url: '/objectModel/' + resData.node._id, method: 'GET'}
        }
      })
    }
  })
})

router.put('/:_id', function(req, res, next) {
  var _id = req.params._id
  var data = {
    label: 'ObjectModel',
    id: parseInt(_id),
    properties: req.body
  }

  internalService.updateNode(data, res, next, function(){
    res.status(200).send({
      links: {
        index: {url: '/objectModel/' + _id, method: 'GET'}
      }
    })
  })
})

router.get('/:_id', function(req, res, next) {
  var id = req.params._id
  internalService.getNode(id, 'TRUE', res, next, function(resData) {

    const data = []
    var item = resData

    data.push({
      name: item.node.name ? item.node.name : null,
      nameAlias: item.node.nameAlias ? item.node.nameAlias : null,
      description: item.node.description ? item.node.description : null,
      status: item.node.status ? item.node.status : null,
      createdAt: item.node.createdAt ? item.node.createdAt : null,
      createdBy: item.node.createdBy ? item.node.createdBy : null,
      updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
      updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
      links: {
        delete: {url: '/objectModel/' + item.node._id, method: 'DELETE'},
        edit: {url: '/objectModel/' + item.node._id, method: 'PUT'}
      }
    })
      
    res.render('objectModel/details', {
      title: `${serverConfig.server.name.toUpperCase()}`,
      user: res.locals.user,
      data: data[0]
    })    
  }
  )
})

router.delete('/:_id', function(req, res, next) {
  const _id = req.params._id

  internalService.deleteNode(_id, res, next, function() {
    res.status(200).send({
      links: {
        index: {
          url: '/objectModel/',
          method: 'GET'
        }
      }
    })
  })
})

module.exports = router
