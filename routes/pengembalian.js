const router = require('express').Router()
const internalService = require('../lib/InternalService')

const dataStructure = [
  {name: 'code_pengembalian', label: 'Code Pengembalian', link: 'view', filter: {type: 'text'}, defaultSort: 'asc'},
  {name: 'tanggal_pengembalian', label: 'Tanggal Pengembalian', filter: {type: 'text'}},
  {name: 'denda', label: 'Denda', filter: {type: 'text'}},
  {name: 'bukuId', label: 'Id Buku', link: 'view', filter: {type: 'text'}, defaultSort: 'asc'},
  {name: 'anggotaId', label: 'Id Anggota', link: 'view', filter: {type: 'text'}, defaultSort: 'asc'},
  {name: 'petugasId', label: 'Id Petugas', link: 'view', filter: {type: 'text'}, defaultSort: 'asc'},
  {name: 'createdAt', label: 'Created At', type: 'date'},
  {name: 'createdBy', label: 'Created By', filter: {type: 'text'}}
]

router.get('/data', function(req, res, next) {
  const queries = req.query
  const reqData = {
    limit: 0,
    label: 'Pengembalian',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {}
  }

  if (queries.order) {
    queries.order.forEach(function(item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.code_pengembalian] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function(item) {
      if (item.search.value !== '' && item.code_pengembalian) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.code_pengembalian] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function(resData) {
      const data = []

      resData.nodes.forEach(function(item) {
        data.push({
          _id: item.node._id ? item.node._id : null,
          code_pengembalian: item.node.code_pengembalian ? item.node.code_pengembalian : null,
          tanggal_pengembalian: item.node.tanggal_pengembalian ? item.node.tanggal_pengembalian : null,
          denda: item.node.denda ? item.node.denda : null,
          bukuId: item.node.bukuId ? item.node.bukuId : null,
          anggotaId: item.node.anggotaId ? item.node.anggotaId : null,
          petugasId: item.node.petugasId ? item.node.petugasId : null,
          createdAt: item.node.createdAt ? item.node.createdAt : null,
          createdBy: item.node.createdBy ? item.node.createdBy : null,
          updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
          updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
          links: {
            view: {url: '/pengembalian/' + item.node._id, method: 'GET'}
          }
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }

      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function(err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }

      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})

router.get('/', function(req, res, next) {
  res.render('pengembalian/list', {
    title: `${serverConfig.server.name.toUpperCase()}`,
    user: res.locals.user,
    dataStructure: dataStructure,
    links: {
      data: {url: '/pengembalian/data', method: 'GET'}
    }
  })
})

router.post('/', function(req, res, next) {
  const reqData = {
    label: 'Pengembalian',
    properties: {
      code_pengembalian: req.body.code_pengembalian,
      tanggal_pengembalian: req.body.tanggal_pengembalian,
      denda: req.body.denda,
      bukuId: req.body.bukuId,
      anggotaId: req.body.anggotaId,
      petugasId: req.body.petugasId
    }
  }

  internalService.createNode(reqData, res, next, function (resData) {

    if (req.body.bukuId) {
      const relationData = {
        startNodeId: resData.node._id,
        endNodeId: req.body.bukuId,
        type: 'HAS_PARENT',
      }

      internalService.createRelationship(relationData, res, next, function () {
        res.status(200).send({
          links: {
            view: { url: '/pengembalian/' + resData.node._id, method: 'GET' }
          }
        })
      })
      if(req.body.anggotaId){
        const relationData = {
          startNodeId: resData.node._id,
          endNodeId: req.body.anggotaId,
          type: 'HAS_PARENT',
        }
  
        internalService.createRelationship(relationData, res, next, function () {
          res.status(200).send({
            links: {
              view: { url: '/pengembalian/' + resData.node._id, method: 'GET' }
            }
          })
        })
      }

      if(req.body.petugasId){
        const relationData = {
          startNodeId: resData.node._id,
          endNodeId: req.body.petugasId,
          type: 'HAS_PARENT',
        }
  
        internalService.createRelationship(relationData, res, next, function () {
          res.status(200).send({
            links: {
              view: { url: '/pengembalian/' + resData.node._id, method: 'GET' }
            }
          })
        })
      }

    }else {
      res.status(200).send({
        links: {
          view: { url: '/pengembalian/' + resData.node._id, method: 'GET' }
        }
      })
    }
  })
})

router.put('/:_id', function(req, res, next) {
  var _id = req.params._id
  var data = {
    label: 'Pengembalian',
    id: parseInt(_id),
    properties: req.body
  }

  internalService.updateNode(data, res, next, function(){
    res.status(200).send({
      links: {
        index: {url: '/pengembalian/' + _id, method: 'GET'}
      }
    })
  })
})

router.get('/:_id', function(req, res, next) {
  var id = req.params._id
  internalService.getNode(id, 'TRUE', res, next, function(resData) {

    const data = []
    
    data.push({
      _id: resData.node._id ? resData.node._id : null,
      code_pengembalian: resData.node.code_pengembalian ? resData.node.code_pengembalian : null,
      tanggal_pengembalian: resData.node.tanggal_pengembalian ? resData.node.tanggal_pengembalian : null,
      denda: resData.node.denda ? resData.node.denda : null,
      bukuId: resData.node.bukuId ? resData.node.bukuId : null,
      anggotaId: resData.node.anggotaId ? resData.node.anggotaId : null,
      petugasId: resData.node.petugasId ? resData.node.petugasId : null,
      createdAt: resData.node.createdAt ? resData.node.createdAt : null,
      createdBy: resData.node.createdBy ? resData.node.createdBy : null,
      updatedAt: resData.node.updatedAt ? resData.node.updatedAt : null,
      updatedBy: resData.node.updatedBy ? resData.node.updatedBy : null,
      links: {
        delete: {url: '/pengembalian/' + resData.node._id, method: 'DELETE'},
        edit: {url: '/pengembalian/' + resData.node._id, method: 'PUT'},
        addBuku: {url: '/pengembalian/addBuku/', method: 'POST'},
        addAnggota: {url: '/pengembalian/addAnggota/', method: 'POST'},
        addPetugas: {url: '/pengembalian/addPetugas/', method: 'POST'},
      },
      buku: [],
      anggota: [],
      petugas: [],
    })

    resData.relationships.forEach(function(itemRelationship){
      if(itemRelationship.relationship != null){
        if(itemRelationship.relationship._type == "HAS_PARENT"){
          if(itemRelationship.node._labels.includes("Buku")){
            data[0].buku.push({
              _id: itemRelationship.node._id ? itemRelationship.node._id : null,
              id_buku: itemRelationship.node.id_buku ? itemRelationship.node.id_buku : null,
              book_code: itemRelationship.node.book_code ? itemRelationship.node.book_code : null,
              judul: itemRelationship.node.judul ? itemRelationship.node.judul : null,
              penulis: itemRelationship.node.penulis ? itemRelationship.node.penulis : null,
              penerbit: itemRelationship.node.penerbit ? itemRelationship.node.penerbit : null,
              terbit: itemRelationship.node.terbit ? itemRelationship.node.terbit : null,
              status: itemRelationship.node.status ? (itemRelationship.node.status == 'ACTIVE') : null,
              createdAt: itemRelationship.node.createdAt ? itemRelationship.node.createdAt : null,
              createdBy: itemRelationship.node.createdBy ? itemRelationship.node.createdBy : null,
              updatedAt: itemRelationship.node.updatedAt ? itemRelationship.node.updatedAt : null,
              updatedBy: itemRelationship.node.updatedBy ? itemRelationship.node.updatedBy : null,
              links: {
                view: {url: '/buku/' + itemRelationship.node._id, method: 'DELETE'},
                delete: {url: '/pengembalian/buku/' + resData.node._id + '/' + itemRelationship.node._id, method: 'DELETE'},
              }
            })
          }
        }
      }
    })
    resData.relationships.forEach(function(itemRelationship){
      if(itemRelationship.relationship != null){
        if(itemRelationship.relationship._type == "HAS_PARENT"){
          if(itemRelationship.node._labels.includes("Anggota")){
            data[0].anggota.push({
              _id: itemRelationship.node._id ? itemRelationship.node._id : null,
              kd_anggota: itemRelationship.node.kd_anggota ? itemRelationship.node.kd_anggota : null,
              name: itemRelationship.node.name ? itemRelationship.node.name : null,
              gender: itemRelationship.node.gender ? itemRelationship.node.gender : null,
              bod: itemRelationship.node.bod ? itemRelationship.node.bod : null,
              address: itemRelationship.node.address ? itemRelationship.node.address : null,
              email: itemRelationship.node.email ? itemRelationship.node.email : null,
              telp: itemRelationship.node.telp ? itemRelationship.node.telp : null,
              status: itemRelationship.node.status ? (itemRelationship.node.status == 'ACTIVE') : null,
              createdAt: itemRelationship.node.createdAt ? itemRelationship.node.createdAt : null,
              createdBy: itemRelationship.node.createdBy ? itemRelationship.node.createdBy : null,
              updatedAt: itemRelationship.node.updatedAt ? itemRelationship.node.updatedAt : null,
              updatedBy: itemRelationship.node.updatedBy ? itemRelationship.node.updatedBy : null,
              links: {
                view: {url: '/anggota/' + itemRelationship.node._id, method: 'DELETE'},
                delete: {url: '/pengembalian/anggota/' + resData.node._id + '/' + itemRelationship.node._id, method: 'DELETE'},
              }
            })
          }
        }
      }
    })
    resData.relationships.forEach(function(itemRelationship){
      if(itemRelationship.relationship != null){
        if(itemRelationship.relationship._type == "HAS_PARENT"){
          if(itemRelationship.node._labels.includes("Petugas")){
            data[0].petugas.push({
              _id: itemRelationship.node._id ? itemRelationship.node._id : null,
              kd_petugas: itemRelationship.node.kd_petugas ? itemRelationship.node.kd_petugas : null,
              nama_petugas: itemRelationship.node.nama_petugas ? itemRelationship.node.nama_petugas : null,
              jabatan_petugas: itemRelationship.node.jabatan_petugas ? itemRelationship.node.jabatan_petugas : null,
              no_telp_petugas: itemRelationship.node.no_telp_petugas ? itemRelationship.node.no_telp_petugas : null,
              alamat_petugas: itemRelationship.node.alamat_petugas ? itemRelationship.node.alamat_petugas : null,
              status: itemRelationship.node.status ? (itemRelationship.node.status == 'ACTIVE') : null,
              createdAt: itemRelationship.node.createdAt ? itemRelationship.node.createdAt : null,
              createdBy: itemRelationship.node.createdBy ? itemRelationship.node.createdBy : null,
              updatedAt: itemRelationship.node.updatedAt ? itemRelationship.node.updatedAt : null,
              updatedBy: itemRelationship.node.updatedBy ? itemRelationship.node.updatedBy : null,
              links: {
                view: {url: '/petugas/' + itemRelationship.node._id, method: 'DELETE'},
                delete: {url: '/pengembalian/petugas/' + resData.node._id + '/' + itemRelationship.node._id, method: 'DELETE'},
              }
            })
          }
        }
      }
    })

    res.render('pengembalian/details', {
      title: `${serverConfig.server.name.toUpperCase()}`,
      user: res.locals.user,
      data: data[0]
    })    
  }
  )
})

router.delete('/:_id', function(req, res, next) {
  const _id = req.params._id

  internalService.deleteNode(_id, res, next, function() {
    res.status(200).send({
      links: {
        index: {
          url: '/pengembalian/',
          method: 'GET'
        }
      }
    })
  })
})

module.exports = router
