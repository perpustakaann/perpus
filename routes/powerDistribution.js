const router = require('express').Router()
const internalService = require('../lib/InternalService')

const dataStructure = [
  {name: 'name', label: 'Name', link: 'view', filter: {type: 'text'}, defaultSort: 'asc'},
  {name: 'type', label: 'Type', filter: {type: 'text'}},
  {name: 'locatedAt', label: 'Located At', filter: {type: 'text'}},
  {name: 'purpose', label: 'Purpose', filter: {type: 'text'}},
  {name: 'createdAt', label: 'Created At', filter:{type: 'text'}},
  {name: 'createdBy', label: 'Created By', filter: {type: 'text'}}
]

router.get('/data', function(req, res, next) {
  const queries = req.query
  const reqData = {
    limit: 0,
    label: 'PowerDistribution',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {}
  }

  if (queries.order) {
    queries.order.forEach(function(item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.name] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function(item) {
      if (item.search.value !== '' && item.name) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.name] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function(resData) {
      const data = []

      resData.nodes.forEach(function(item) {
        data.push({
          _id: item.node._id ? item.node._id : null,
          name: item.node.name ? item.node.name : null,
          type: item.node.type ? item.node.type : null,
          uid: item.node.uid ? item.node.uid : null,
          height: item.node.height ? item.node.height : null,
          width: item.node.width ? item.node.width : null,
          length: item.node.length ? item.node.length : null,
          serialNumber: item.node.serialNumber ? item.node.serialNumber : null,
          manufacturer: item.node.manufacturer ? item.node.manufacturer : null,
          model: item.node.model ? item.node.model : null,
          currentType: item.node.currentType ? item.node.currentType: null,
          voltage: item.node.voltage ? item.node.voltage: null,
          ampere: item.node.ampere ? item.node.ampere: null,
          status: item.node.status ? item.node.status: null,
          createdAt: item.node.createdAt ? item.node.createdAt : null,
          createdBy: item.node.createdBy ? item.node.createdBy : null,
          updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
          updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
          links: {
            view: {url: '/powerDistribution/' + item.node._id, method: 'GET'}
          }
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }

      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function(err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }

      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})

router.get('/', function(req, res, next) {
  res.render('powerDistribution/list', { title: `${serverConfig.server.name.toUpperCase()}`,
    user: res.locals.user,
    dataStructure: dataStructure,
    links: {
      data: {url: '/powerDistribution/data', method: 'GET'}
    }
  })
})

router.post('/', function(req, res, next) {
  let params = req.body
  params['createdBy'] = res.locals.user
  params['spaceId'] = parseInt(req.body.spaceId)
  params['ipOn'] = req.get('host')
  let request = {
    collection : 'IDC-FE' ,
    type : 'QUERY',
    name : 'CREATE_POWER_DISTRIBUTION_UNIT',
    body : {
      params : params
    }
  }
  internalService.predefinedQuery(request, res, next, function(resData) {
    res.status(200).send({
      links: {
        view: {url: '/powerDistribution/' + resData[0]._id, method: 'GET'}
      }
    })
  })
})

router.put('/:_id', function(req, res, next) {
  var _id = req.params._id
  var data = {
    label: 'PowerDistribution',
    id: parseInt(_id),
    properties: req.body
  }

  internalService.updateNode(data, res, next, function(){
    res.status(200).send({
      links: {
        index: {url: '/powerDistribution/' + _id, method: 'GET'}
      }
    })
  })
})

router.get('/:_id', function(req, res, next) {
  var id = req.params._id
  internalService.getNode(id, 'TRUE', res, next, function(resData) {
    const data = []
    data.push({
      _id: resData.node._id ? resData.node._id : null,
      name: resData.node.name ? resData.node.name : null,
      type: resData.node.type ? resData.node.type : null,
      ipAddress: resData.node.ipAddress ? resData.node.ipAddress : null,
      label: resData.node.label ? resData.node.label : null,
      uid: resData.node.uid ? resData.node.uid : null,
      height: resData.node.height ? resData.node.height : null,
      width: resData.node.width ? resData.node.width : null,
      length: resData.node.length ? resData.node.length : null,
      serialNumber: resData.node.serialNumber ? resData.node.serialNumber : null,
      manufacturer: resData.node.manufacturer ? resData.node.manufacturer : null,
      model: resData.node.model ? resData.node.model : null,
      currentType: resData.node.currentType ? resData.node.currentType: null,
      voltage: resData.node.voltage ? resData.node.voltage: null,
      ampere: resData.node.ampere ? resData.node.ampere: null,
      status: resData.node.status ? resData.node.status: null,
      purpose: resData.node.purpose ? resData.node.purpose: null,
      createdAt: resData.node.createdAt ? resData.node.createdAt : null,
      createdBy: resData.node.createdBy ? resData.node.createdBy : null,
      updatedAt: resData.node.updatedAt ? resData.node.updatedAt : null,
      updatedBy: resData.node.updatedBy ? resData.node.updatedBy : null,
      links: {
        delete: {url: '/powerDistribution/' + resData.node._id, method: 'DELETE'},
        edit: {url: '/powerDistribution/' + resData.node._id, method: 'PUT'}
      },
      powerUnit: []
    })
    
    if(resData.relationships[0].relationship !== null){
      resData.relationships.forEach(function(item){
        if(item.node._labels.includes('PowerUnit')){
          data[0].powerUnit.push({
            _id: item.node._id ? item.node._id : null,
            name: item.node.name ? item.node.name : null,
            number: item.node.number ? item.node.number : null,
            label: item.node.label ? item.node.label : null,
            sequence: item.node.sequence ? item.node.sequence : null,
            voltage: item.node.voltage ? item.node.voltage : null,
            ampere: item.node.ampere ? item.node.ampere : null,
            status: item.node.status ? item.node.status : null,
            type: item.node.type ? item.node.status : null,
            currentType: item.node.currentType ? item.node.currentType : null,
            purpose: item.node.purpose ? item.node.purpose : null,
            createdAt: item.node.createdAt ? item.node.createdAt : null,
            createdBy: item.node.createdBy ? item.node.createdBy : null,
            createdOn: item.node.createdOn ? item.node.createdOn : null,
            links: {
              view: {url: '/powerUnit/' + item.node._id + '?parentId=' + resData.node._id, method : 'GET'},
              delete: {url: '/powerUnit/' + item.node._id + '?parentId=' + resData.node._id, method: 'DELETE'},
            }
          })
        }
      })
    }

    res.render('powerDistribution/details', {
      title: `${serverConfig.server.name.toUpperCase()}`,
      user: res.locals.user,
      data: data[0]
    })    
  }
  )
})

router.delete('/:_id', function(req, res, next) {
  const _id = req.params._id

  internalService.deleteNode(_id, res, next, function() {
    res.status(200).send({
      links: {
        index: {
          url: '/powerDistribution/',
          method: 'GET'
        }
      }
    })
  })
})

module.exports = router
