/* eslint-disable no-unused-vars */
function addSuccessNotification(title, message) {
  iziToast.success({
    title: title,
    message: message,
    timeout: 1500,
    titleSize: '9px',
    messageSize: '9px',
    position: 'bottomRight',
    close: false,
    closeOnClick: true
  })
}

function addFailNotification(title, message) {
  iziToast.error({
    title: title,
    message: message,
    timeout: 1500,
    titleSize: '9px',
    messageSize: '9px',
    position: 'bottomRight',
    close: false,
    closeOnClick: true
  })
}

function showLoading() {
  document.getElementById('loader').style.display = 'block'
}

function hideLoading() {
  document.getElementById('loader').style.display = 'none'
}

function objectifyForm(formArray) {
  var returnArray = {}
  for (var i = 0; i < formArray.length; i++) {
    returnArray[formArray[i]['name']] = formArray[i]['value']
  }
  return returnArray
}

function send(method, options) {
  var resp = $.ajax({
    type: method,
    url: options.url,
    data: options.data
  })

  const defaultDone = function(statusCode, data, text) {
    if (statusCode === 303) {
      window.location = text
    } else {
      if (method === 'POST') {
        window.location = data.links.view.url
      } else if (method === 'PUT') {
        window.location.reload()
      } else if (method === 'DELETE') {
        window.location = data.links.index.url
      } else {
        addSuccessNotification(data.status, data.message)
      }
    }
  }

  const defaultFail = function(statusCode, data, text) {
    addFailNotification(statusCode, text)
  }

  resp.always(function(x, xx, xxx) {
    const context = xxx.status !== undefined ? xxx : x
    if (context.status >= 200 && context.status < 400) {
      if (options.done) {
        options.done(
          '[' + context.status + '] ' + context.statusText,
          context.responseJSON,
          context.responseText,
          defaultDone
        )
      } else {
        defaultDone('[' + context.status + '] ' + context.statusText, context.responseJSON, context.responseText)
      }
    } else {
      hideLoading()
      if (options.fail) {
        options.fail(
          '[' + context.status + '] ' + context.statusText,
          context.responseJSON,
          context.responseText,
          defaultFail
        )
      } else {
        defaultFail('[' + context.status + '] ' + context.statusText, context.responseJSON, context.responseText)
      }
    }
  })
}

function sendGet(options) {
  send('GET', options)
}

function sendPut(options) {
  showLoading()
  send('PUT', options)
}

function sendDelete(options) {
  showLoading()
  send('DELETE', options)
}

function sendPost(options) {
  showLoading()
  send('POST', options)
}

$(document).ready(function() {
  $('#notification').on('click', '.message .close', function() {
    document.getElementById('notification').removeChild(this.parentElement)
  })
})
